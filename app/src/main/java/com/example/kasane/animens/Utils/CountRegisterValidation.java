package com.example.kasane.animens.Utils;

/**
 * Clase modelo de utilidad
 * @author Marina Espinosa
 */
public class CountRegisterValidation {
    private String countNick;
    private String countEmail;
    private String countPhone;

    public String getCountNick() {
        return countNick;
    }

    public void setCountNick(String countNick) {
        this.countNick = countNick;
    }

    public String getCountEmail() {
        return countEmail;
    }

    public void setCountEmail(String countEmail) {
        this.countEmail = countEmail;
    }

    public String getCountPhone() {
        return countPhone;
    }

    public void setCountPhone(String countPhone) {
        this.countPhone = countPhone;
    }
}
