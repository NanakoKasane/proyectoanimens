package com.example.kasane.animens.ui.User;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Fragment que gestiona el listao de usuarios de la aplicación
 * @author Marina Espinosa
 */
public class UserListFragment extends BaseFragment implements UserListContract.View{
    public static String TAG = "UserListFragment";

    RecyclerView rvUserList;
    SwipeRefreshLayout swipeRefreshLayout;

    UserListAdapter userListAdapter;
    UserListPresenter presenter;
    GoToUser goToUser;
    boolean noAmigos = false;
    String nick;

    public interface GoToUser{
        void goToUser(Profile profile); // Al user pulsado
    }

    public static UserListFragment newInstance(Bundle bundle){
        UserListFragment userListFragment = new UserListFragment();
        if (bundle!= null){
            userListFragment.setArguments(bundle);
        }
        return userListFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            goToUser = (GoToUser) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements GoToUser");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        if (getView() != null) {
            rvUserList = getView().findViewById(R.id.rvUserList);
            swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);

            rvUserList.setLayoutManager(new GridLayoutManager(getContext(), 2));
            userListAdapter = new UserListAdapter(getContext(), new ArrayList<User>());
            userListAdapter.setListenerUser(new ListenerPerfilUser());
            rvUserList.setAdapter(userListAdapter);


            presenter = new UserListPresenter(this);

            // TODO. Obtener del extra ya la lista de users. Si no hay nada en el extra, hacer lo siguiente (obtenerla del presenter e interactor):
            if (getArguments() == null) {
                if (!CommonUtils.isNetworkOnline()) {
                    if (getActivity() != null)
                        CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                } else {
                    showProgressBar(null, getString(R.string.msg_loading_users), false, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
                    presenter.getUserList();
                }
            }
            // TODO. O si no -> CONSULTA A LA BD -> select amigos de ese user (nick)
            else {
                nick = getArguments().getString("Nick");
                if (!CommonUtils.isNetworkOnline()) {
                    if (getActivity() != null)
                        CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                } else {
                    showProgressBar(null, getString(R.string.msg_loading_friends), false, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
                    presenter.getFriendList(nick);
                }
            }

            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (!CommonUtils.isNetworkOnline()) {
                        if (getActivity() != null)
                            CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                    } else {
                        showProgressBar(null, getString(R.string.msg_updating_users), false, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                        if (getArguments() == null) {
                            presenter.getUserList();
//                    showProgressBar();
                        } else {
                            nick = getArguments().getString("Nick");
                            presenter.getFriendList(nick);
//                    showProgressBar();
                        }
                    }
                }
            });
        }


    }


    // View
    @Override
    public void showUserList(ArrayList<User> users) {
        hideProgressBar();
        swipeRefreshLayout.setRefreshing(false);

        if (users != null && users.size() == 0){
            showInformationDialog(getString(R.string.msg_nousers_title), getString(R.string.msg_nousers), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
        else if (users != null) {
//            Collections.sort(users, new User.OrderByNick());
            userListAdapter.setUsers(users);
            userListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onServerError() {
        swipeRefreshLayout.setRefreshing(false);
        hideProgressBar();

        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_server);
    }

    @Override
    public void onUserProfile(Profile profile) {
        goToUser.goToUser(profile);
    }

    @Override
    public void onNoFriends() {
        swipeRefreshLayout.setRefreshing(false);
        hideProgressBar();
        // Toast.makeText(getContext(), "Aún no tiene amigos añadidos", Toast.LENGTH_SHORT).show();
        if (userListAdapter.getItemCount() == 0)
        {
            DialogInterface.OnClickListener listenerOk =  new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (getFragmentManager() != null && getFragmentManager().getBackStackEntryCount() == 0){
                        Intent intent = new Intent(getActivity(), ProfileActivity.class);
                        startActivity(intent);
                    }
                    else{
                        if (getFragmentManager() != null){
                            getFragmentManager().popBackStack();
                            // nombre del user en la barra
                            try {
                                if (nick != null)
                                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(nick);
                                else
                                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.nav_profile);
                            } catch (Exception e) {
                                Log.d("Marina", e.getMessage());
                            }

                        }
                    }
                }
            };

            showInformationDialog(getString(R.string.msg_information_title), getString(R.string.msg_information_friend), listenerOk);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        // TODO. Obtener del extra ya la lista de users. Si no hay nada en el extra, hacer lo siguiente (obtenerla del presenter e interactor):
//        if (getArguments() == null){
//            presenter.getUserList();
//        }
//        // TODO. O si no -> CONSULTA A LA BD -> select amigos de ese user (nick)
//        else{
//            String nick = getArguments().getString("Nick");
//            presenter.getFriendList(nick);
//        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        setHasOptionsMenu(true);
    }

    private class ListenerPerfilUser implements View.OnClickListener{
        @Override
        public void onClick(View v) {

            // TODO. (cargar fragment de profile con el extra del profile de ese user, que se saca con una CONSULTA a la BD)

//            Toast.makeText(AnimensApplication.getContext(), "Perfil del usuario " + userListAdapter.getItem(rvUserList.getChildAdapterPosition(v)).getNick(), Toast.LENGTH_SHORT).show();

            if (getActivity() != null && getView() != null)
                CommonUtils.hideKeyboard(getActivity(), getView());
            presenter.getUserProfile(userListAdapter.getItem(rvUserList.getChildAdapterPosition(v)).getNick());



//            Intent intent = new Intent(getContext(), ProfileActivity.class);
//            intent.putExtra("User", ""); // TODO. Deberá pasarse solo el id o nick de las preferencias y consultar a la BBDD
//            startActivity(intent);

            // TODO. IR AL PERFIL DE ESE USER PULSADO
            //intent.putExtra("User", userListAdapter.getItem(rcvUserList.getChildAdapterPosition(v)));  // El Nick

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                userListAdapter.filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                userListAdapter.filter(newText);
                return false;
            }
        });
    }





}
