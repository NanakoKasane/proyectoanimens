package com.example.kasane.animens.ui.Notifications;

import android.content.Context;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.data.network.repository.FriendRequestsRepository;
import com.example.kasane.animens.data.network.repository.ProfileRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class NotificationsInteractor {

    public void getNotifications(Context context, NotificationsInteractor.Notifications listener) {
        // TODO. Obtener las peticiones de ESTE USER, el nick siguiente:
        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        try {
            String url = AnimensApplication.BASE_URL+"animens/friendRequestsList.php?userWhoReceivedRequest_nick=" + nick;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<FriendRequests> friendRequests = gson.fromJson(response.getJSONArray("friendrequests").toString(), new TypeToken<ArrayList<FriendRequests>>(){}.getType());
                        CommonUtils.saveCountOfFriendRequests(friendRequests.size());
                        listener.onNotifications(friendRequests);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();

                    listener.onNoNotifications();// Lista vacía
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" +throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" +throwable.getMessage());
//                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
//            listener.onServerError();
        }


//        listener.onNotifications(FriendRequestsRepository.getInstance().getFriendRequests());
    }

    public void getUserProfile(String nick, NotificationsInteractor.GetUserProfile listener) {

        // TODO. Consulta a la BD de obtener el perfil de ese user


        listener.onProfileObtained(ProfileRepository.getProfileRepository().getProfile());


    }

    public void cancelFriendRequest(String friendrequest_id, NotificationsInteractor.Notifications listener) {

        // TODO. Delete en la BD de la notificación con id = friendrequest_id

        try {
            String url = AnimensApplication.BASE_URL+"animens/denyFriendRequest.php?friendrequest_id=" + friendrequest_id ;
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    // TODO. listener.onSuccess();
                    Log.d("AnimensPHP", "ONSUCCESS (insertado en addFriendRequest)" + responseString );
                    listener.onFriendRequestCancelled();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }


//        ArrayList<FriendRequests> friendRequests = FriendRequestsRepository.getInstance().getFriendRequests();
//        int indice = friendRequests.indexOf(new FriendRequests(friendrequest_id, "alguien", "otroalguien", ""));
//        if (indice != -1)
//            friendRequests.remove(indice);
//        listener.onFriendRequestCancelled();

    }

    public void acceptFriendRequest(String friendrequest_id, NotificationsInteractor.Notifications listener) {

        // TODO. Cambio accepted a true

        try {
            String url = AnimensApplication.BASE_URL+"animens/acceptFriendRequest.php?friendrequest_id=" + friendrequest_id ;
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    // TODO. listener.onSuccess();
                    Log.d("AnimensPHP", "ONSUCCESS (insertado en addFriendRequest)" + responseString );
                    listener.onFriendRequestAccepted();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }




        // TODO. INSERT en la BD en la tabla FRIENDS los usuarios que devuelve la CONSULTA -> select userWhoSendsRequest_nick, userWhoReceivedRequest_nick from friendrequests where friendrequests_id = 'friendrequest_id';


        // TODO. DELETE en la BD de la notificación con id = friendrequest_id



//        ArrayList<FriendRequests> friendRequests = FriendRequestsRepository.getInstance().getFriendRequests();
//        int indice = friendRequests.indexOf(new FriendRequests(friendrequest_id, "alguien", "otroalguien", ""));
//        if (indice != -1)
//            friendRequests.remove(indice);

    }

    interface Notifications{
        void onNotifications(ArrayList<FriendRequests> friendRequests);
        void onFriendRequestCancelled();
        void onFriendRequestAccepted();

        void onNoNotifications();
        void onServerError();
    }

    interface GetUserProfile{
        void onProfileObtained(Profile profile);
    }


}
