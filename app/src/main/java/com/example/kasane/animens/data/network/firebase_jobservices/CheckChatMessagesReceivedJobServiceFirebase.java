package com.example.kasane.animens.data.network.firebase_jobservices;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
//import android.app.job.JobParameters;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.ui.Chats.ChatsActivity;
import com.example.kasane.animens.ui.Chats.ConversationFirebaseActivity;
import com.example.kasane.animens.ui.Notifications.NotificationsActivity;
import com.example.kasane.animens.ui.Pofile.ProfileFragment;
//import com.firebase.jobdispatcher.JobParameters;
//import com.firebase.jobdispatcher.JobService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * JobService (de Firebase) que comprueba si hay mensajes sin leer de un chatroom.
 * Este JobService se inicializa en AnimensApplication y se programa para que se ejecute cada X tiempo
 * Al funcionar con Firebase aunque la aplicación haya finalizado se siguen ejecutando
 * @author Marina Espinosa
 */

public class CheckChatMessagesReceivedJobServiceFirebase {

}
/*
public class CheckChatMessagesReceivedJobServiceFirebase extends JobService {

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public boolean onStartJob(@NonNull JobParameters job) {
        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        String url = "https://marina.edufdezsoy.es/animens/getUnreadedMessages.php?nick=" + nick;
        RestClient.get(url, new JsonHttpResponseHandler() {

            // TODO. On success -> obtengo la lista de los chats no leídos
            // Mediante Broadcast notifico -> Le paso en el bundle serializable las notificaciones
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Log.d("AnimensPHP", "success "+ response.toString());
                    Gson gson = new Gson();
                    ArrayList<Chatroom> chatrooms = gson.fromJson(response.getJSONArray("chatroom").toString(), new TypeToken<ArrayList<Chatroom>>(){}.getType());

                    if (chatrooms.size() == 1){
                        Intent intentChats = new Intent(AnimensApplication.getContext(), ConversationFirebaseActivity.class);
                        intentChats.putExtra("chatroom", chatrooms.get(0));
                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentChats, PendingIntent.FLAG_UPDATE_CURRENT);

                        Log.d("JobService", "Notificará que hay 1 mensaje sin leer");
                        String otherUser;
                        if (nick.equals(chatrooms.get(0).getNick1()))
                            otherUser = chatrooms.get(0).getNick2();
                        else
                            otherUser = chatrooms.get(0).getNick1();
                        createNotification("Hay mensajes sin leer del usuario " + otherUser, pendingIntent, "Mensajes", chatrooms.get(0)); // TODO. el usuario
                    }
                    else if (chatrooms.size() > 1){
                        Intent intentChats = new Intent(AnimensApplication.getContext(), ChatsActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 1, intentChats, PendingIntent.FLAG_UPDATE_CURRENT);

                        Log.d("JobService", "Notificará que hay más de 1 mensaje sin leer");
                        createNotification("Tienes varios mensajes sin leer", pendingIntent, "Mensajes", null);
                    }
                    else{
                        Log.d("JobService", "No hay mensajes sin leer");
                    }

            // if / else
//                        createNotification("Te han mandado " + numRecibidas + " peticiones de amistad", pendingIntent, "Peticiones de amistad");

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("JobService", e.getMessage());
                    Log.d("JobService", response.toString());
//                        listener.onServerError();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("JobService", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("JobService", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("JobService", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
        });

        jobFinished(job, true);
        return true;
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        return false;
    }


    private void createNotification(String text, PendingIntent pendingIntent, String title, Chatroom chatroom){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AnimensApplication.getContext(), AnimensApplication.notificationChannel);
        notificationBuilder.setSmallIcon(R.mipmap.animenslogo);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (chatroom != null) {
            notificationBuilder.setContentTitle(chatroom.getSenderLastMessage());
            notificationBuilder.setContentText(chatroom.getLastMessage());

            String imageOtherUser;
            if (chatroom.getNick1().equals(CommonUtils.getCurrentUserNick(AnimensApplication.getContext())))
                imageOtherUser = chatroom.getPicture_nick2();
            else
                imageOtherUser = chatroom.getPicture_nick1();

            Picasso.get()
                    .load(imageOtherUser)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                            notificationBuilder.setStyle(new NotificationCompat.BigPictureStyle()
//                                    .bigPicture(bitmap));

                            notificationBuilder.setLargeIcon(bitmap);

                            if (notificationManager != null)
                                notificationManager.notify(0, notificationBuilder.build());
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            if (notificationManager != null)
                                notificationManager.notify(0, notificationBuilder.build());
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            if (notificationManager != null)
                                notificationManager.notify(0, notificationBuilder.build());
                        }
                    });
        }
        else {
            if (notificationManager != null)
                notificationManager.notify(0, notificationBuilder.build());
        }

//        startForeground(0, notificationBuilder.build());
    }


}
*/
