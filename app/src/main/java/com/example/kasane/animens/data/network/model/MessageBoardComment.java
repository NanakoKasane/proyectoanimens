package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class MessageBoardComment implements Comparable<MessageBoardComment> {
    private String messageBoardComment_id; // 100 max
    private String messageBoard_id; // 100 max
    private String userWhoComments;
    private String comment; // max 200

    public MessageBoardComment(String messageBoardComment_id, String messageBoard_id, String userWhoComments, String comment) {
        this.messageBoardComment_id = messageBoardComment_id;
        this.messageBoard_id = messageBoard_id;
        this.userWhoComments = userWhoComments;
        this.comment = comment;
    }

    public String getMessageBoardComment_id() {
        return messageBoardComment_id;
    }

    public void setMessageBoardComment_id(String messageBoardComment_id) {
        this.messageBoardComment_id = messageBoardComment_id;
    }

    public String getMessageBoard_id() {
        return messageBoard_id;
    }

    public void setMessageBoard_id(String messageBoard_id) {
        this.messageBoard_id = messageBoard_id;
    }

    public String getUserWhoComments() {
        return userWhoComments;
    }

    public void setUserWhoComments(String userWhoComments) {
        this.userWhoComments = userWhoComments;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    // Comparando por id
    @Override
    public int compareTo(@NonNull MessageBoardComment o) {
        return this.getMessageBoardComment_id().compareTo(o.getMessageBoardComment_id());
    }

    // Comparando por id del mensaje
    public static class OrderByMessageId implements Comparator<MessageBoardComment> {
        @Override
        public int compare(MessageBoardComment o1, MessageBoardComment o2) {
            return o1.getMessageBoard_id().compareTo(o2.getMessageBoard_id());
        }
    }
}
