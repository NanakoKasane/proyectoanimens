package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.data.network.model.Genres;

import java.util.ArrayList;

public class GenresListRepository {
    private static GenresListRepository genresListRepository;
    private ArrayList<Genres> generos;

    static {
        genresListRepository = new GenresListRepository();
    }

    private GenresListRepository(){
        generos = new ArrayList<>();
        anadirGeneros();
    }
    private void anadirGeneros(){
        generos.add(new Genres("Accion"));
        generos.add(new Genres("Aventura"));
        generos.add(new Genres("Psicológico"));
        generos.add(new Genres("Hentai"));
        generos.add(new Genres("Ecchi"));
        generos.add(new Genres("Comedia"));
        generos.add(new Genres("Romance"));
        generos.add(new Genres("Comedia"));
        generos.add(new Genres("Comedia"));
        generos.add(new Genres("Comedia"));
        generos.add(new Genres("Comedia"));
        generos.add(new Genres("Comedia"));
    }


    public ArrayList<Genres> getGeneros() {
        return generos;
    }

    public static GenresListRepository getGenresListRepository() {
        return genresListRepository;
    }
}
