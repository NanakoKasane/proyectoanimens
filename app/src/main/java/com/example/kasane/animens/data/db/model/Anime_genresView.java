package com.example.kasane.animens.data.db.model;


import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Clase modelo para sqlite
 * @author Marina Espinosa
 */
public class Anime_genresView implements Comparable<Anime_genresView> { // Extends Anime_genres
    private String genre; // Nombre del género
    private int count; // count(*) de animes de ese género

    public Anime_genresView(String genre) {
        this.genre = genre;
    }

    public Anime_genresView(String genre, int count) {
        this.genre = genre;
        this.count = count;
    }


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    /**
     * Comparación por género (ordenado alfabéticamente)
     */
    @Override
    public int compareTo(@NonNull Anime_genresView o) {
        return this.genre.compareTo(o.getGenre());
    }

    /**
     * Otros criterios de comparación
     */
    // Ordenación de más animes de ese género a menos (por count)
    public static class OrderByCount implements Comparator<Anime_genresView> {
        @Override
        public int compare(Anime_genresView o1, Anime_genresView o2) {
            return o2.getCount()- o1.getCount();
        }
    }

}

