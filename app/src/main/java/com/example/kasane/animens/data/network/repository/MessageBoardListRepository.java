package com.example.kasane.animens.data.network.repository;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.kasane.animens.data.network.model.MessageBoard;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class MessageBoardListRepository {
    static private MessageBoardListRepository messageBoardListRepository;
    private ArrayList<MessageBoard> messageBoards;
    static {
        messageBoardListRepository = new MessageBoardListRepository();
    }

    private MessageBoardListRepository(){
        messageBoards = new ArrayList<>();
        add();
    }
    private void add(){
        MessageBoard mess = new MessageBoard("1", "lourdes", "Búsqueda de amigos", "BUSCO AMIGOS", null, "Buenas! Soy Lourdes y me gusta el anime " +
                " y querría conocer gente que también le guste " +
                "Muchas gracias! ");
                mess.setTime(System.currentTimeMillis());
                mess.setPicture("https://myanimelist.cdn-dena.com/images/anime/12/74683l.jpg");
        messageBoards.add(mess);

        messageBoards.add(new MessageBoard("2", "@NickProvisional", "Quedadas", "VER PELI", "https://marina.edufdezsoy.es/animens/imagenes/image-2019-02-121343.jpeg", "olaa! Soy Alfred y he visto que estrenan " +
                "en el cine la película Anime Carnival " +
                "Phantasm. ¿Alguien se apunta"));
        messageBoards.add(new MessageBoard("3", "@NickProvisional", "Búsqueda de amigos", "BUSCO FANS DE KIZUNA", "https://marina.edufdezsoy.es/animens/imagenes/image-2019-02-122218.jpeg", "Buenas! Soy María y me encanta el anime " +
                "de Kizuna Ichigeki y busco amigos"));

        Collections.sort(messageBoards);

    }

    public ArrayList<MessageBoard> getMessageBoards() {
        return messageBoards;
    }

    public static MessageBoardListRepository getMessageBoardListRepository() {
        return messageBoardListRepository;
    }



    // Todo. Este método a Utils.
    private static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Log.d("Marina", "Error al convertir a bitmap la imagen: " + src);
            return null;
        }
    }

    public ArrayList<Bitmap> getImagesBitMap(){
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        for(MessageBoard messageBoard : messageBoards){
            Bitmap bitmapTmp = getBitmapFromURL(messageBoard.getPicture());
            bitmaps.add(bitmapTmp);
        }
        return bitmaps;
    }
}
