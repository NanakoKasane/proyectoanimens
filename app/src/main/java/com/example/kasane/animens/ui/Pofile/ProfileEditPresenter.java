package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.Profile;

public class ProfileEditPresenter implements ProfileEditContact.Presenter, ProfileEditInteractor.EditProfile{
    ProfileEditContact.View view;
    ProfileEditInteractor interactor;

    public ProfileEditPresenter(ProfileEditContact.View view){
        this.view = view;
        interactor = new ProfileEditInteractor();
    }


    @Override
    public void editProfile(String nombre, String genero, String fechaNac, String descripcion, String image, Context context, Bitmap bitmap) {
        interactor.editProfile(nombre, genero, fechaNac, descripcion, image, this, context, bitmap);
    }

    // Interactor
    @Override
    public void onEdited() {
        view.onEdited();
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onPhotoTooBig() {
        view.onPhotoTooBig();
    }
}
