package com.example.kasane.animens.ui.Chats;

import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Chats;

import java.util.ArrayList;

public class ChatsListPresenter implements ChatsListContract.Presenter, ChatsListInteractor.LoadChats{
    private ChatsListContract.View view;
    private ChatsListInteractor interactor;

    public ChatsListPresenter(ChatsListContract.View view){
        this.view = view;
        interactor = new ChatsListInteractor(this);
    }

    // Presenter
    @Override
    public void loadChats() {
        interactor.getChats();
    }

    // Interactor
    @Override
    public void loadChats(ArrayList<Chatroom> chats) {
        view.showChats(chats);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }
}
