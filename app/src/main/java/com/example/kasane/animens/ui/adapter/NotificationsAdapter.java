package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Adapter para mostrar la lista de notificaciones (peticiones de amistad)
 * @author Marina Espinosa
 */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsHolder> {
    private ArrayList<FriendRequests> friendRequests = new ArrayList<>();
    private Context context;
    private View.OnClickListener clickGoToUser;
    private View.OnClickListener clickAccept;
    private View.OnClickListener clickCancel;

    public NotificationsAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)(parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))).inflate(R.layout.notifications_item, parent, false);
        return new NotificationsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NotificationsHolder holder, int position) {
        FriendRequests friendRequest = friendRequests.get(position);

        holder.tvTextoSolicitud.setText(friendRequest.getUserWhoSendsRequest_nick() + " " + context.getString(R.string.tvTextoSolicitud));

        Picasso.get()
                .load(friendRequest.getPicture())
                .fit()
                .centerInside()
                .into(holder.img_user, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onError(Exception e) {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }

                });

        holder.img_user.setTag(friendRequest.getUserWhoSendsRequest_nick());
        holder.img_user.setOnClickListener(clickGoToUser);

        holder.imgAccept.setTag(friendRequest.getFriendrequests_id());
        holder.imgAccept.setOnClickListener(clickAccept);
        holder.imgCancel.setTag(friendRequest.getFriendrequests_id());
        holder.imgCancel.setOnClickListener(clickCancel);

    }

    @Override
    public int getItemCount() {
        return friendRequests.size();
    }

    public static class NotificationsHolder extends RecyclerView.ViewHolder{
        CircleImageView img_user;
        TextView tvTextoSolicitud;
        ImageButton imgAccept;
        ImageButton imgCancel;
        ProgressBar progressBar;

        public NotificationsHolder(View itemView) {
            super(itemView);
            img_user = itemView.findViewById(R.id.img_user);
            tvTextoSolicitud = itemView.findViewById(R.id.tvTextoSolicitud);
            imgAccept = itemView.findViewById(R.id.imgAccept);
            imgCancel = itemView.findViewById(R.id.imgCancel);
            progressBar = itemView.findViewById(R.id.progressBar);

        }
    }

    public void setFriendRequests(ArrayList<FriendRequests> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public void setClickGoToUser(View.OnClickListener clickGoToUser) {
        this.clickGoToUser = clickGoToUser;
    }
    public void setClickAccept(View.OnClickListener clickAccept) {
        this.clickAccept = clickAccept;
    }
    public void setClickCancel(View.OnClickListener clickCancel) {
        this.clickCancel = clickCancel;
    }

}
