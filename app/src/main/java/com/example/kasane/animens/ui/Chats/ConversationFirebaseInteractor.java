package com.example.kasane.animens.ui.Chats;

import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.data.network.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
/**
 * Clase Interactor que se comunica con el servidor
 */
public class ConversationFirebaseInteractor {

    public void setLastMessageReaded(String chat_id, UpdateLastMessage listener) {
        String url = AnimensApplication.BASE_URL + "animens/setLastMessageReaded.php";
        RequestParams requestParams = new RequestParams();
        requestParams.put("chat_id", chat_id);

        try {
            RestClient.post(url, requestParams, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("Chat", "Chat readed - OnFailure: "+responseString);
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseBytes, throwable);
                    Log.d("Chat", "Chat readed - OnFailure: "+throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    listener.onUpdatedReaded();
                }
            });
        }
        catch (Exception e) {
            Log.d("Chat", "Chat readed - Excepcion: "+ e.getMessage());
            listener.onServerError();
        }

    }

    public void updateLastChatMessage(String lastMessage, String timeLastMessage, String dateLastMessage, String senderLastMessage, String chat_id, UpdateLastMessage listener) {
        String url = AnimensApplication.BASE_URL+"animens/updateLastChatMessage.php";
        RequestParams requestParams = new RequestParams();
        requestParams.put("lastMessage", lastMessage);
        requestParams.put("timeLastMessage", timeLastMessage);
        requestParams.put("dateLastMessage", dateLastMessage);
        requestParams.put("senderLastMessage", senderLastMessage);
        requestParams.put("chat_id", chat_id);

        try {
            RestClient.post(url, requestParams, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("Chat", "UpdateLastMessage - OnFailure: "+responseString);
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseBytes, throwable);
                    Log.d("Chat", "UpdateLastMessage - OnFailure: "+throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    listener.onUpdatedLastChatMessage();
                }
            });
        }
        catch (Exception e) {
            Log.d("Chat", "Chat UpdateLastMessage - Excepcion: "+ e.getMessage());
            listener.onServerError();
        }
    }


    public interface UpdateLastMessage {
        void onUpdatedReaded();
        void onUpdatedLastChatMessage();

        void onServerError();
    }
}
