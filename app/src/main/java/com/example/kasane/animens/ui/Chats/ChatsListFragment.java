package com.example.kasane.animens.ui.Chats;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Chats;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.adapter.ChatroomAdapter;
import com.example.kasane.animens.ui.adapter.ChatsListAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Fragment que gestiona el listado de los chats
 * @author Marina Espinosa
 */
public class ChatsListFragment extends BaseFragment implements ChatsListContract.View {
    static public String TAG = "ChatsListFragment";
    RecyclerView rvConversations;
    SwipeRefreshLayout swipeRefreshLayout;

//    ChatsListAdapter chatsListAdapter;
    ChatsListContract.Presenter presenter;
    ChatroomAdapter chatroomAdapter;
    GoChatRoom listener;
    boolean primeraVez = true;

    public interface GoChatRoom{
        void loadChatRoom(Chatroom chatroom);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (GoChatRoom) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException( context + " must implements GoChatRoom");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        if (getView() != null) {
            rvConversations = getView().findViewById(R.id.rvConversations);
            swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);

            rvConversations.setLayoutManager(new LinearLayoutManager(getContext()));
            chatroomAdapter = new ChatroomAdapter(getContext());

            chatroomAdapter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO. Va al chat (detalle del chat con ese user)
                    Chatroom chatroom = chatroomAdapter.getItem(rvConversations.getChildAdapterPosition(v));

//                if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(chatroom.getNick1()))
//                    Toast.makeText(getContext(), "Yendo al chat con el usuario " + chatroom.getNick2(), Toast.LENGTH_SHORT).show();
//                else
//                    Toast.makeText(getContext(), "Yendo al chat con el usuario " + chatroom.getNick1(), Toast.LENGTH_SHORT).show();

                    listener.loadChatRoom(chatroom);
                }
            });
            rvConversations.setAdapter(chatroomAdapter);

            presenter = new ChatsListPresenter(this);
            if (!CommonUtils.isNetworkOnline()) {
                if (getActivity() != null)
                    CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
            } else {
                showProgressBar(null, getString(R.string.msg_loading_chats), false, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                presenter.loadChats();
            }

            swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (!CommonUtils.isNetworkOnline()) {
                        if (getActivity() != null)
                            CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                    } else {
                        showProgressBar(null, getString(R.string.msg_reloading_chats), false, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                        presenter.loadChats();
                    }
                }
            });
        }
    }


    @Override
    public void showChats(ArrayList<Chatroom> chats) {
        hideProgressBar();

        if (chats.size() == 0 && primeraVez){
            primeraVez = false;
            showInformationDialog("No chats", "No hay chats. Para iniciar un chat con otro usuario visite su perfil", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        else {
            Collections.sort(chats);
            chatroomAdapter.setChats(chats);
            chatroomAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onServerError() {
        swipeRefreshLayout.setRefreshing(false);

        hideProgressBar();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_general_register);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
        }
        else {
            showProgressBar(null, getString(R.string.msg_reloading_chats), false, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                }
            });
            presenter.loadChats();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                chatroomAdapter.filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                chatroomAdapter.filter(newText);
                return false;
            }
        });
    }
}
