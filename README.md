<p align="center">
  <img width="150" src="http://89.39.159.65/animens/animens.png">
</p>

<p><b>Animens es una aplicación social destinada a personas que les gusten los Animes (series de dibujos Animados japoneses)</b> </p>
<br/>
<ul>
    <li><p>El <b>manual del usuario</b> con las opciones detalladas de la aplicación se encuentra en el siguiente enlace: <i>http://89.39.159.65/animens/Animens_manualDelUsuario.pdf</i></p></li>
    <li><p>Para <b>descargar el apk</b> y probar la aplicación se puede entrar en el siguiente enlace: <i>http://89.39.159.65/animens/Animens.apk</i> </p></li>
    <li><p>Esta aplicación es para <i>Android</i> y está programada en<b> <i>Java</i></b>. </p></li>
    <li><p>La parte del <b>servidor</b> está en su mayoría en <b><i>PHP</i></b> y se puede encontrar en el siguiente repositorio: <i>https://gitlab.com/NanakoKasane/animensserverphp/tree/master/</i> </p></li>
    <li><p>Los chats y la autenticación están implementados mediante <i><b>Firebase</b></i></p></li>
</ul>
<br/>
