package com.example.kasane.animens.ui.Notifications;

import android.content.Context;

import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;

public class NotificationsPresenter implements NotificationsContract.Presenter, NotificationsInteractor.Notifications, NotificationsInteractor.GetUserProfile {
    private NotificationsContract.View view;
    private NotificationsInteractor interactor;

    public NotificationsPresenter(NotificationsContract.View view){
        this.view = view;
        interactor = new NotificationsInteractor();
    }

    // Presenter
    @Override
    public void getNotifications(Context context) {
        interactor.getNotifications(context, this);
    }

    @Override
    public void getUserProfile(String nick) {
        interactor.getUserProfile(nick, this);
    }

    @Override
    public void cancelFriendRequest(String friendrequest_id) {
        interactor.cancelFriendRequest(friendrequest_id, this);
    }

    @Override
    public void acceptFriendRequest(String friendrequest_id) {
        interactor.acceptFriendRequest(friendrequest_id, this);
    }


    // Interactor
    @Override
    public void onNotifications(ArrayList<FriendRequests> friendRequests) {
        view.showNotifications(friendRequests);
    }

    @Override
    public void onFriendRequestCancelled() {
        view.onFriendRequestCancelled();
    }

    @Override
    public void onFriendRequestAccepted() {
        view.onFriendRequestAccepted();
    }

    @Override
    public void onNoNotifications() {
        view.onNoNotifications();
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onProfileObtained(Profile profile) {
        view.onProfileObtained(profile);
    }
}
