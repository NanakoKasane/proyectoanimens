package com.example.kasane.animens;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.kasane.animens.data.network.jobservices.CheckChatMessagesJobService;
//import com.example.kasane.animens.data.network.firebase_jobservices.CheckChatMessagesReceivedJobServiceFirebase;
//import com.example.kasane.animens.data.network.firebase_jobservices.CheckFriendRequestsAcceptedFirebaseJobService;
import com.example.kasane.animens.data.network.jobservices.CheckFriendRequestsAcceptedJobService;
import com.example.kasane.animens.data.network.services.CheckFriendRequestsAcceptedService;
import com.example.kasane.animens.data.network.jobservices.CheckFriendRequestsReceivedJobService;
import com.example.kasane.animens.data.network.services.CheckFriendRequestsReceivedService;
//import com.example.kasane.animens.data.network.firebase_jobservices.CheckFriendRequestsReceivedJobServiceFirebase;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.ui.Notifications.NotificationsActivity;
import com.example.kasane.animens.ui.User.UserListActivity;
//import com.firebase.jobdispatcher.Constraint;
//import com.firebase.jobdispatcher.FirebaseJobDispatcher;
//import com.firebase.jobdispatcher.GooglePlayDriver;
//import com.firebase.jobdispatcher.Job;
//import com.firebase.jobdispatcher.Lifetime;
//import com.firebase.jobdispatcher.RetryStrategy;
//import com.firebase.jobdispatcher.Trigger;

import org.acra.ACRA;
import org.acra.annotation.AcraMailSender;
import org.acra.annotation.AcraNotification;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.NotificationConfigurationBuilder;
import org.acra.data.StringFormat;

import java.util.ArrayList;


// ACRA

@AcraNotification(resText = R.string.crash_notification_text,
        resTitle = R.string.crash_notification_title,
        resChannelName = R.string.notification_channel)

@AcraMailSender(mailTo = "marinaespinosagalvez@gmail.com")

// Así será con un toast, sin tener que esperar que el usuario diga que sí para enviar error:
//@AcraToast(resText=R.string.crash_toast_text,
//        length = Toast.LENGTH_LONG)


/**
 * Clase Application en la que se crea el canal para las notificaciones, se obtiene el contexto
 * y se iniciar los JobServices que se ejecutarán cada X tiempo
 * @author Marina Espinosa
 */
public class AnimensApplication extends Application {
    static private Context context;
    static public String notificationChannel = "canal_1";
//    public int contador = 4637;
//    static FirebaseJobDispatcher dispatcher;
    static public String BASE_URL = "http://89.39.159.65/";

    @Override
    public void onCreate() {
        super.onCreate();

        CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this);
        builder.setBuildConfigClass(BuildConfig.class).setReportFormat(StringFormat.JSON);
        builder.getPluginConfigurationBuilder(NotificationConfigurationBuilder.class).setResText(R.string.crash_notification_text);
        ACRA.init(this);

        AnimensApplication.context = getApplicationContext();
        createNotificationChannel();

        try {
            scheduleCheckFriendRequestsReceivedJobService(getApplicationContext());
            scheduleCheckChatMessagesJobService(getApplicationContext());
            scheduleCheckFriendRequestsAcceptedJobService(getApplicationContext());
        }
        catch (Exception e){
            Log.d("JobServices", "No se han podido iniciar los servicios " + e.getMessage());
        }
//        setJobServices();

        // Registro Broadcast
//        IntentFilter intentFilter = new IntentFilter();
//        intentFilter.addAction(CheckFriendRequestsAcceptedService.ACTION_SUCCESS);
//        intentFilter.addAction(CheckFriendRequestsAcceptedService.ACTION_FAILURE);
//        intentFilter.addAction(CheckFriendRequestsReceivedService.ACTION_SUCCESS);
//        LocalBroadcastManager.getInstance(AnimensApplication.getContext()).registerReceiver(broadcastReceiverFriendRequests, intentFilter);

        // . Inicio los servicios que se ejecutarán cada 1 minuto
//        setServices();
    }

    public static Context getContext() {
        return AnimensApplication.context;
    }


    static public void createNotificationChannel(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(AnimensApplication.notificationChannel, AnimensApplication.context.getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription(AnimensApplication.context.getString(R.string.channel_description));

            NotificationManager notificationManager = (NotificationManager) AnimensApplication.context.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

        }

    }

    // TODO. broadcast
    BroadcastReceiver broadcastReceiverFriendRequests = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case CheckFriendRequestsAcceptedService.ACTION_FAILURE:
                    // Simplemente no han aceptado ninguna petición, se ignora
                    break;

                case CheckFriendRequestsAcceptedService.ACTION_SUCCESS:
                    // Se ha aceptado una o varias peticiones de amistad, muestro nofiticacion

                    int aceptadas = intent.getIntExtra("peticioneaceptadas", 0);
                    ArrayList<FriendRequests> friendRequests = new ArrayList<>();
//                    Toast.makeText(context, "Num aceptadas: " + friendRequests, Toast.LENGTH_SHORT).show();
                    for(int i = 0; i < aceptadas; i++){
                        FriendRequests peticion = (FriendRequests) intent.getExtras().getSerializable("friendrequest" + i);
                        friendRequests.add(peticion);

                        Toast.makeText(context, "El usuario " + peticion.getUserWhoReceivedRequest_nick() + " te ha aceptado la petición de amistad", Toast.LENGTH_SHORT).show();


                        if (aceptadas > 1){
                            Intent intentPerfil = new Intent(AnimensApplication.getContext(), UserListActivity.class); // TODO. Esto luego irá a TUS AMIGOS
                            PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentPerfil, PendingIntent.FLAG_UPDATE_CURRENT);
                            createNotification("Varios usuarios han aceptado sus peticiones de amistad", pendingIntent, "Petición aceptada");
                        }
                        else{
                            Intent intentPerfil = new Intent(AnimensApplication.getContext(), UserListActivity.class); // TODO. Esto luego irá a TUS AMIGOS
                            PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentPerfil, PendingIntent.FLAG_UPDATE_CURRENT);
                            createNotification("El usuario " + peticion.getUserWhoReceivedRequest_nick() + " te ha aceptado la petición de amistad", pendingIntent, "Petición aceptada");

                        }
                    }

                    break;


                // TODO. Caso del Service que comprueba si te han llegado peticiones de amistad
                case CheckFriendRequestsReceivedService.ACTION_SUCCESS:
                    int numRecibidas = intent.getIntExtra("peticiones", 0);
                    Intent intentNotificaciones = new Intent(AnimensApplication.getContext(), NotificationsActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentNotificaciones, PendingIntent.FLAG_UPDATE_CURRENT);

                    if (numRecibidas > 1)
                        createNotification("Te han mandado " + numRecibidas + " peticiones de amistad", pendingIntent, "Peticiones de amistad");
                    else
                        createNotification("Te han mandado una petición de amistad", pendingIntent, "Peticiones de amistad");

                    break;
            }
        }
    };

    @Override
    public void onTerminate() {

        // TODO. Desregistro broadcast
        LocalBroadcastManager.getInstance(AnimensApplication.getContext()).unregisterReceiver(broadcastReceiverFriendRequests);

        super.onTerminate();

    }

    private void setServices(){
        // TODO. Servicio que comprueba si te han aceptado peticiones
        Intent intentAccepted = new Intent(this, CheckFriendRequestsAcceptedService.class);
        PendingIntent pendingIntentAccepted = PendingIntent.getService(this, 0, intentAccepted, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntentAccepted); // Cancelo el anterior
        // Inicio el servicio (que está en el pending intent), que se ejecuta cada minuto (ya que el minimo es un minuto aunque pongamos 5 segundos):
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000 , pendingIntentAccepted);

        // TODO. Servicio que comprueba si te han enviado peticiones
        Intent intentSend = new Intent(this, CheckFriendRequestsReceivedService.class);
        PendingIntent pendingIntentSend = PendingIntent.getService(this, 0, intentSend, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntentSend); // Cancelo el anterior
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000 , pendingIntentSend);
    }

    /**
     * Método que cra una notificación
     */
    private void createNotification(String text, PendingIntent pendingIntent, String title){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AnimensApplication.getContext(), AnimensApplication.notificationChannel);
        notificationBuilder.setSmallIcon(R.mipmap.animenslogo);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


    // TODO. Job services
    public static void scheduleCheckChatMessagesJobService(Context context) {
        ComponentName serviceComponent = new ComponentName(context, CheckChatMessagesJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(5 * 1000);
        builder.setOverrideDeadline(10 * 1000);
        builder.setRequiresCharging(false); // we don't care if the device is charging or not
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }
    public static void scheduleCheckFriendRequestsAcceptedJobService(Context context) {
        ComponentName serviceComponent = new ComponentName(context, CheckFriendRequestsAcceptedJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(1, serviceComponent);
        builder.setMinimumLatency(30 * 1000); // min
        builder.setOverrideDeadline(60 * 1000); // max
        builder.setRequiresCharging(false);
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }
    public static void scheduleCheckFriendRequestsReceivedJobService(Context context) {
        ComponentName serviceComponent = new ComponentName(context, CheckFriendRequestsReceivedJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(2, serviceComponent);
        builder.setMinimumLatency(30 * 1000);
        builder.setOverrideDeadline(60 * 1000);
        builder.setRequiresCharging(false);
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }


    // TODO. Job services de Firebase
//    private void setJobServicesFirebase(){
//         dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//
//        scheduleJobCheckFriendRequests(this);
//        scheduleJobAcceptedFriendRequests(this);
//        scheduleJobCheckChatMessages(this);
//    }
//
//
//    public static void scheduleJobCheckFriendRequests(Context context) {
//        //creating new firebase job dispatcher
//
////        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//
//        //creating new job and adding it with dispatcher
//        Job job = createJobCheckFriendRequests(dispatcher);
//        dispatcher.mustSchedule(job);
//    }
//
//    public static Job createJobCheckFriendRequests(FirebaseJobDispatcher dispatcher){
//        Job job = dispatcher.newJobBuilder()
//                //persist the task across boots
//                .setLifetime(Lifetime.FOREVER)
//                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
//                //call this service when the criteria are met.
//                .setService(CheckFriendRequestsReceivedJobServiceFirebase.class)
//                //unique id of the task
//                .setTag("CheckFriendRequestsReceivedService")
//                //don't overwrite an existing job with the same tag
//                .setReplaceCurrent(true)
//                // We are mentioning that the job is periodic.
//                .setRecurring(true)
//                // Run between 3 - 6 minutes from now.
//                .setTrigger(Trigger.executionWindow(10, 60 ))
//                // retry with exponential backoff
//                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
//                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                //Run this job only when the network is available.
//                .setConstraints(Constraint.ON_ANY_NETWORK, Constraint.DEVICE_CHARGING)
//                .build();
//        return job;
//    }
//
//
//    public static void scheduleJobAcceptedFriendRequests(Context context) {
//        //creating new firebase job dispatcher
//
////        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//
//        //creating new job and adding it with dispatcher
//        Job job = createJobAcceptedFriendRequests(dispatcher);
//        dispatcher.mustSchedule(job);
//
//    }
//
//    public static Job createJobAcceptedFriendRequests(FirebaseJobDispatcher dispatcher){
//        Job job = dispatcher.newJobBuilder()
//                //persist the task across boots
//                .setLifetime(Lifetime.FOREVER)
//                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
//                //call this service when the criteria are met.
//                .setService(CheckFriendRequestsAcceptedFirebaseJobService.class)
//                //unique id of the task
//                .setTag("CheckFriendRequestsAcceptedService")
//                //don't overwrite an existing job with the same tag
//                .setReplaceCurrent(true)
//                // We are mentioning that the job is periodic.
//                .setRecurring(true)
//                // Run between 10 - 60 seconds from now.
//                .setTrigger(Trigger.executionWindow(10, 60))
//                // retry with exponential backoff
//                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
//                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                //Run this job only when the network is available.
//                .setConstraints(Constraint.ON_ANY_NETWORK, Constraint.DEVICE_CHARGING)
//                .build();
//        return job;
//    }
//
//    public static void scheduleJobCheckChatMessages(Context context) {
//        //creating new firebase job dispatcher
//
////        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
//
//        //creating new job and adding it with dispatcher
//        Job job = createJobCheckChatMessages(dispatcher);
//        dispatcher.mustSchedule(job);
//
//    }
//
//    public static Job createJobCheckChatMessages(FirebaseJobDispatcher dispatcher){
//        Job job = dispatcher.newJobBuilder()
//                //persist the task across boots
//                .setLifetime(Lifetime.FOREVER)
//                //.setLifetime(Lifetime.UNTIL_NEXT_BOOT)
//                //call this service when the criteria are met.
//                .setService(CheckChatMessagesReceivedJobServiceFirebase.class)
//                //unique id of the task
//                .setTag("CheckChatMessagesReceivedJobServiceFirebase")
//                //don't overwrite an existing job with the same tag
//                .setReplaceCurrent(true)
//                // We are mentioning that the job is periodic.
//                .setRecurring(true)
//                // Run between 10 - 60 seconds from now.
//                .setTrigger(Trigger.executionWindow(10, 60))
//                // retry with exponential backoff
//                .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
//                //.setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                //Run this job only when the network is available.
//                .setConstraints(Constraint.ON_ANY_NETWORK, Constraint.DEVICE_CHARGING)
//                .build();
//        return job;
//    }



}
