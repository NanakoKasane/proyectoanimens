package com.example.kasane.animens.ui.Register;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.User.UserListInteractor;

import java.util.ArrayList;

public class RegisterPresenterImpl implements RegisterPresenter, RegisterInteractorImpl.RegisterInteractor, RegisterInteractorImpl.InsertUser{
    private RegisterView view;
    private RegisterInteractorImpl interactor;

    public RegisterPresenterImpl(RegisterView view){
        this.view = view;
        interactor = new RegisterInteractorImpl(this);
    }

    // De presenter
    @Override
    public void validateCredentials(String nick, String email, String phone, String password, String passwordConfirm) {
        view.showProgressBar(null, AnimensApplication.getContext().getString(R.string.msg_validatingregister));
        interactor.validation(nick, email, phone, password, passwordConfirm);
    }

    @Override
    public void insertUser(String nick, String email, String phone, String password) {
        interactor.insertUser(nick, email, phone, password, this);
    }

    @Override
    public void deleteUser(String email) {
        interactor.deleteUser(email);
    }

    // De interactor
    @Override
    public void onSuccess() {
        view.hideProgressBar();
        view.successValidation();
    }

    @Override
    public void onPasswordError() {
        view.hideProgressBar();
        view.showPasswordError();
    }

    @Override
    public void onPasswordConfirmError() {
        view.hideProgressBar();
        view.showPasswordConfirmError();
    }

    @Override
    public void onNickEmptyError() {
        view.hideProgressBar();
        view.showNickEmptyError();
    }

    @Override
    public void onInvalidEmailOrPhoneError() {
        view.hideProgressBar();
        view.showInvalidEmailOrPhoneError();
    }

    @Override
    public void onEmailExists() {
        view.onEmailExists();
    }

    @Override
    public void onNickExists() {
        view.onNickExists();
    }

    @Override
    public void onPhoneExists() {
        view.onPhoneExists();
    }

    @Override
    public void onNickError() {
        view.onNickError();
    }

    @Override
    public void onError() {
        view.onError();
    }

    @Override
    public void onInserted(String email) {
        view.onRegistered(email);
    }

    @Override
    public void onErrorInserting() {
        view.onError();
    }


}
