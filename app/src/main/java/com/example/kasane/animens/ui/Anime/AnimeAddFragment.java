package com.example.kasane.animens.ui.Anime;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.Utils.MultiselectionSpinner;
import com.example.kasane.animens.ui.Base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

// todo. Diferencias entre Add y Edit:
// en edit: hay que cargar los datos del anime a editar. A la BD un update
// en add: no se cargan datos. A la BD un add


public class AnimeAddFragment extends BaseFragment implements AnimeAddContract.View {
    List<String> generosString;
    MultiselectionSpinner spinner;
    FloatingActionButton btGuardar;

    TextInputEditText edTitulo;
    TextInputEditText edUrl;
    TextInputEditText edTipo;
    TextInputEditText edCapitulos;

    AnimeAddPresenter presenter;
    Anime anime;

    public static AnimeAddFragment newInstance(Bundle bundle) {
        AnimeAddFragment animeAddFragment = new AnimeAddFragment();
        if (bundle != null)
            animeAddFragment.setArguments(bundle);
        return animeAddFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_anime_add, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new AnimeAddPresenter(AnimeAddFragment.this);
        inicializar();

        generosString = new ArrayList<String>();
        presenter.getGenresStringList();


        // Todo. Hay que cargar los datos a editar.
        if (getArguments() != null) {
            anime = (Anime) getArguments().getSerializable("Anime");
            if (anime != null) {
                rellenarDatosAnime(anime);
            }
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void showGenresStringList(ArrayList<String> genres) {
        generosString = genres;

        // Añado los géneros al Spinner
        ArrayAdapter<String> adapter = new ArrayAdapter <String>(AnimensApplication.getContext(), android.R.layout.simple_list_item_multiple_choice, generosString);
        spinner.setItems(generosString);
    }

    @Override
    public void onValidated() {
        double rating;
        String anime_id;

        if (anime == null){
            rating = 0;
            anime_id = null; // Se calcula luego. select max(anime_id) from anime;
        }
        else{
            rating = anime.getRating();
            anime_id = anime.getAnime_id();
        }


        presenter.editAnime(anime, new Anime(anime_id, 0, edTitulo.getText().toString(), spinner.getSelectedItemsAsString(),
                edTipo.getText().toString(), edCapitulos.getText().toString(), (float) rating, edUrl.getText().toString() ));

    }

    @Override
    public void showEmptyNameError() {
//        Toast.makeText(getContext(), R.string.err_emptyNameError, Toast.LENGTH_SHORT).show();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_emptyNameError);
    }

    @Override
    public void showEmptyEpisodesError() {
//        Toast.makeText(getContext(), R.string.err_emptyEpisodesError, Toast.LENGTH_SHORT).show();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_emptyEpisodesError);
    }

    @Override
    public void showEmptyPictureError() {
//        Toast.makeText(getContext(), R.string.err_emptyImageError, Toast.LENGTH_SHORT).show();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_emptyImageError);

    }

    @Override
    public void showInvalidEpisodesNumber() {
//        Toast.makeText(getContext(), R.string.err_invalidEpisodesNumber, Toast.LENGTH_SHORT).show();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_invalidEpisodesNumber);
    }


    @Override
    public void onEdited(Anime anime) {
//        Toast.makeText(getContext(), "Datos actualizados correctamente: " + anime.getName(), Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(AnimeAddFragment.this, AnimeDetailFragment.class);
//        intent.putExtra("Anime", anime);
//        startActivity(intent);

        Intent intent = new Intent(getActivity(), AnimeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intent, 0);
        buildNotification(getString(R.string.app_name), "Anime \""+ anime.getName() + "\" añadido con éxito", pendingIntent, getContext());
        Toast.makeText(AnimensApplication.getContext(), "Anime \""+ anime.getName() + "\" añadido con éxito", Toast.LENGTH_SHORT).show();

        if (getActivity() != null)
            CommonUtils.hideKeyboard(getActivity(), getView());

        if (getFragmentManager() != null)
            getFragmentManager().popBackStack();

    }



    void inicializar(){
        if (getView() == null)
            return;
        spinner = getView().findViewById(R.id.miMultipleSelection);
        edTitulo = getView().findViewById(R.id.edTitulo);
        edUrl = getView().findViewById(R.id.edUrl);
        edTipo = getView().findViewById(R.id.edTipo);
        edCapitulos = getView().findViewById(R.id.edCapitulos);
        btGuardar = getView().findViewById(R.id.btGuardar);

        btGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO. Método validarCampos. Y actualizar en la BD
                // Todo. No puede estar vacío ni el título ni los capítulos ni la foto (y la foto debe ser válida, verse en un navegador). name maximo 100, genre 125, type 7, episodes 7, picture (60)

                presenter.validateAnime(edTitulo.getText().toString(), edUrl.getText().toString(),
                        edTipo.getText().toString(), edCapitulos.getText().toString() );


//        Toast.makeText(this, "Datos actualizados correctamente", Toast.LENGTH_SHORT).show();
//        this.finish();
            }
        });
    }

    void rellenarDatosAnime(Anime anime){
        edTitulo.setText(anime.getName());
        edTitulo.setFocusable(false); // El nombre no se podrá cambiar, si no no será editar ese anime
        edTitulo.setClickable(false);

        edUrl.setText(anime.getPicture());
        edTipo.setText(anime.getType());
        edCapitulos.setText(anime.getEpisodes());


        spinner.setSelection(anime.getGenresList());
    }




}
