package com.example.kasane.animens.ui.Genres;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Anime_genres;
import com.example.kasane.animens.ui.Anime.AnimeActivity;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.adapter.GenreListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity que gestiona los géneros. Carga los fragment
 * @author Marina Espinosa
 */
public class GenresListActivity extends BaseActivity implements GenresListFragment.GetAnimes {
    Fragment genresListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO
       // setContentView(R.layout.activity_genres_list);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.nav_genres);

        genresListFragment = getSupportFragmentManager().findFragmentByTag(GenresListFragment.TAG);
        if (genresListFragment == null){
            genresListFragment = new GenresListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mcontent, genresListFragment, GenresListFragment.TAG);
            fragmentTransaction.commit();
        }

    }

    @Override
    public void getAnimesOfThisGenre(String genre) {
        // TODO. Luego consulta de obtener los animes de ese género


//        Toast.makeText(AnimensApplication.getContext(), "Animes del género: " + genre, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(GenresListActivity.this, AnimeActivity.class);
        intent.putExtra("Genre", genre);
        startActivity(intent);


    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                super.onBackPressed();
        }
    }
}
