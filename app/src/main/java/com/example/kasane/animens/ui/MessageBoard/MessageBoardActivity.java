package com.example.kasane.animens.ui.MessageBoard;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;

public class MessageBoardActivity extends BaseActivity implements MessageBoardListFragment.Listener{

    Fragment messageBoardListFragment;
    Fragment messageBoardAddFragment;
    Fragment messageBoardDetailFragment;
//    Fragment profileFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setContentView(R.layout.activity_messageboard);

        getSupportActionBar().setTitle(R.string.nav_messageboard);

        messageBoardListFragment = getSupportFragmentManager().findFragmentByTag(MessageBoardListFragment.TAG);
        if (messageBoardListFragment == null){
            messageBoardListFragment = new MessageBoardListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mcontent, messageBoardListFragment, MessageBoardListFragment.TAG);
            fragmentTransaction.commit();
        }




    }


    @Override
    public void onDetail(MessageBoard messageBoard) {
        // Cargo el fragment del detalle
        messageBoardDetailFragment = getSupportFragmentManager().findFragmentByTag(MessageBoardDetailFragment.TAG);
        if (messageBoardDetailFragment == null){
            // argumentos con el bundle ("MessageBoard")
            Bundle bundle = new Bundle();
            bundle.putSerializable("MessageBoard", messageBoard);
            messageBoardDetailFragment = MessageBoardDetailFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mcontent, messageBoardDetailFragment, MessageBoardDetailFragment.TAG);
            // añadir a la pila
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onEdit(MessageBoard messageBoard) {
        // cargo el fragment de Add (si es null lo que le pasas es add, si no, es editar)
        messageBoardAddFragment = getSupportFragmentManager().findFragmentByTag(MessageBoardAddFragment.TAG);
        if (messageBoardAddFragment == null){
            // argumentos con el bundle ("MessageBoard")
            Bundle bundle = new Bundle();
            bundle.putSerializable("MessageBoard", messageBoard);
            messageBoardAddFragment = MessageBoardAddFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mcontent, messageBoardAddFragment, MessageBoardAddFragment.TAG);
            // añadir a la pila
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }


    }


    @Override
    public void onUserProfile(Profile profile) {
        // cargo el fragment de profile
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("Profile", profile);
//        profileFragment = ProfileFragment.newInstance(bundle);
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.replace(android.R.id.content, profileFragment);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();

        Intent intent = new Intent(MessageBoardActivity.this, ProfileActivity.class);
        intent.putExtra("Profile", profile);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            CommonUtils.showDialogLogOut(this);
        }
        else {
            super.onBackPressed();
        }
    }
}
