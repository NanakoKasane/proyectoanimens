package com.example.kasane.animens.ui.Chats;

import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ChatsListInteractor {
    ChatsListInteractor.LoadChats listener;

    public ChatsListInteractor(ChatsListInteractor.LoadChats listener){
        this.listener = listener;
    }

    public void getChats(){
//        listener.loadChats(ChatsListRepository.getChatsListRepository().getChats());

        // TODO
        // https://marina.edufdezsoy.es/animens/getChatRooms.php?nick=claudia
        RestClient.get(AnimensApplication.BASE_URL+"animens/getChatRooms.php?nick=" + CommonUtils.getCurrentUserNick(AnimensApplication.getContext()), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<Chatroom> chatrooms = gson.fromJson(response.getJSONArray("chatroom").toString(), new TypeToken<ArrayList<Chatroom>>(){}.getType());
                    listener.loadChats(chatrooms);

                    // TODO. listener.onChatObtained(chatrooms);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensDB", e.getMessage());
                    listener.onServerError(); //e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }
        });


    }

    interface LoadChats{
        void loadChats(ArrayList<Chatroom> chats);
        void onServerError();
    }

}
