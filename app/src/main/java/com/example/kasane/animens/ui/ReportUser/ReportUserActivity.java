package com.example.kasane.animens.ui.ReportUser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Activity que gestiona los mensajes de una conversación de Firebase
 * @author Marina Espinosa
 */
public class ReportUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        String userNick = getIntent().getStringExtra("nick");

        if (userNick != null) {

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);

                getSupportActionBar().setTitle(getString(R.string.nav_report_user) + " " + userNick);
            }


            Bundle bundle = new Bundle();
            bundle.putString("nick", userNick);
            getSupportFragmentManager().beginTransaction().replace(R.id.preferencecontent, ReportUserFragment.newInstance(bundle)).commit();

        }

    }

    //     Volver Atras con back y con flechita de arriba
    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        // TODO.

        this.finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
