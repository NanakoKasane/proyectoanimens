package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Anime;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Adapter para mostrar el listado de Animes
 * @author Marina Espinosa
 */
public class AnimeListAdapter extends RecyclerView.Adapter<AnimeListAdapter.AnimeHolder> {
    private Context context;
    private ArrayList<Anime> animeList;
    private ArrayList<Anime> animeListCopy = new ArrayList<>();
    private ArrayList<Bitmap> bitmaps;
    private MaterialRatingBar.OnRatingBarChangeListener onRatingBarChangeListener;
    private View.OnClickListener onClickListener;
    private View.OnLongClickListener onLongClickListener;

    public AnimeListAdapter(Context context, ArrayList<Anime> animeList, ArrayList<Bitmap> bitmaps) {
        this.context = context;
        this.animeList = animeList;
        this.animeListCopy = new ArrayList<>();
        this.animeListCopy.addAll(animeList);

        this.bitmaps = bitmaps;
    }

    public void setOnRatingBarChangeListener(MaterialRatingBar.OnRatingBarChangeListener onTouchListener) {
        this.onRatingBarChangeListener = onTouchListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.onLongClickListener = onLongClickListener;
    }

    @NonNull
    @Override
    public AnimeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.animelist_item, parent, false);
        // view.setOnLongClickListener(onLongClickListener);
        AnimeHolder animeHolder = new AnimeHolder(view);
        return animeHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull final AnimeHolder holder, final int position) {
        Anime anime = animeList.get(position);
        // anime.setBitmapImage(bitmaps.get(position)); //  No se puede.
//        anime.setImage(CommonUtils.bitmapAByteArray(bitmaps.get(position)));

        holder.tvNombreAnime.setText(anime.getName());
        holder.tvNombreAnime.setOnClickListener(onClickListener);
        holder.tvNombreAnime.setOnLongClickListener(onLongClickListener);
        holder.tvNombreAnime.setTag(anime);

//        holder.imgAnime.setImageBitmap(bitmaps.get(position));
        holder.imgAnime.setOnClickListener(onClickListener);
        holder.imgAnime.setOnLongClickListener(onLongClickListener);
        holder.imgAnime.setTag(anime);

        if (holder.progressBar != null)
            holder.progressBar.setVisibility(View.VISIBLE);
        Picasso.get()
                .load(anime.getPicture())
                .fit()
                .centerInside()
                .error(R.drawable.imagenotfound)
                .into(holder.imgAnime, new com.squareup.picasso.Callback()
                {
                @Override
                public void onSuccess() {
                    if (holder.progressBar != null) {
                        holder.progressBar.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onError(Exception e) {
                    if (holder.progressBar != null) {
                        holder.progressBar.setVisibility(View.GONE);
                    }
                }

        });

        holder.ratingBarAnime.setRating(animeList.get(position).getRatingPer5());
        holder.ratingBarAnime.setTag(animeList.get(position));
        holder.ratingBarAnime.setOnRatingBarChangeListener(onRatingBarChangeListener);
    }

    @Override
    public int getItemCount() {
        return animeList.size();
    }


    public class AnimeHolder extends RecyclerView.ViewHolder{
        private TextView tvNombreAnime;
        private ImageView imgAnime;
        private MaterialRatingBar ratingBarAnime;
        private ProgressBar progressBar;

        public AnimeHolder(View itemView) {
            super(itemView);
            tvNombreAnime = itemView.findViewById(R.id.tvNombreAnime);
            imgAnime = itemView.findViewById(R.id.imgAnime);
            ratingBarAnime = itemView.findViewById(R.id.ratingBarAnime);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public Anime getItem(int position) {
        return animeList.get(position);
    }


    // Todo. Luego quitaré la lista del constructor y lo añadiré desde aquí

    public void setAnimeList(ArrayList<Anime> animeList) {
        this.animeList = animeList;
        animeListCopy = new ArrayList<>();
        animeListCopy.addAll(animeList);
    }

    public void setBitmaps(ArrayList<Bitmap> bitmaps) {
        this.bitmaps = bitmaps;
    }

    public ArrayList<Anime> getAnimeList() {
        return animeList;
    }


    public void filter(String text) {
        animeList.clear();
        if(text.isEmpty()){
            animeList.addAll(animeListCopy);
        } else{
            text = text.toLowerCase();
            for(Anime anime : animeListCopy){
                if(anime.getName().toLowerCase().contains(text) || anime.getName().toUpperCase().contains(text)){
                    animeList.add(anime);
                }
            }
        }
        notifyDataSetChanged();
    }
}
