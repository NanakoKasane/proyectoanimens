package com.example.kasane.animens.ui.ReportUser;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.Utils.MultiselectionSpinner;
import com.example.kasane.animens.ui.Base.BaseFragment;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;

public class ReportUserFragment extends BaseFragment implements ReportUserContract.View{
    private static final int SELECT_FILE = 1;
    Bitmap bitmap;
    ReportUserPresenter presenter;
    String userNick;

    MultiselectionSpinner multiselectionSpinner;
    EditText edDescripcion;
    FloatingActionButton btReportUser;
    ImageView imgCamera;
    ImageView imgPerfil;
    TextView tvImagenTitle;

    public static ReportUserFragment newInstance(Bundle bundle){
        ReportUserFragment reportUserFragment = new ReportUserFragment();
        if (bundle != null){
            reportUserFragment.setArguments(bundle);
        }
        return reportUserFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.report_user, container, false);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null)
            userNick = getArguments().getString("nick");

        inicializar(view);
    }

    private void inicializar(View view){
        presenter = new ReportUserPresenter(this);
        multiselectionSpinner = view.findViewById(R.id.miMultipleSelection);
        edDescripcion = view.findViewById(R.id.edDescripcion);
        btReportUser = view.findViewById(R.id.btReportUser);
        imgCamera = view.findViewById(R.id.imgCamera);
        imgPerfil = view.findViewById(R.id.imgPerfil);
        tvImagenTitle = view.findViewById(R.id.tvImagenTitle);

        if (getContext() != null){
            multiselectionSpinner.setItems(getContext().getResources().getStringArray(R.array.motivos_reporte));
            multiselectionSpinner.setSelection(0);
        }
        else
            multiselectionSpinner.setItems(new ArrayList<>());

        // subir imagen
        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });
        tvImagenTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });

        imgPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });

        // enviar el reporte
        btReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!CommonUtils.isNetworkOnline()){
                    if (getActivity() != null)
                        CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                }
                else {
                    if (getActivity() != null && getView() != null)
                        CommonUtils.hideKeyboard(getActivity(), getView());
                    if (edDescripcion.getText().toString().isEmpty() || TextUtils.isEmpty(multiselectionSpinner.getSelectedItemsAsString())){
                        if (getActivity() != null){
                            CommonUtils.showMessage(getActivity(), R.string.err_notmotivo);
                        }
                    }
                    else {
                        presenter.sendReport(userNick, bitmap, edDescripcion.getText().toString(), multiselectionSpinner.getSelectedItemsAsString());
                        showProgressBar(null, getString(R.string.msg_sendingreport), false, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                            }
                        });
                    }
                }
            }
        });

    }



    /**
     * Subir foto
     */
    public void abrirGaleria(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), SELECT_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Uri selectedImage;

        switch (requestCode) {
            case SELECT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();

                    try {
                        String selectedPath = selectedImage.getPath();
                        if (requestCode == SELECT_FILE) {

                            if (selectedPath != null) {
                                InputStream imageStream = null;
                                try {
                                    imageStream = getContext().getContentResolver().openInputStream(
                                            selectedImage);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }

                                // Transformamos la URI de la imagen a inputStream y este a un Bitmap
                                Bitmap bmp = BitmapFactory.decodeStream(imageStream);
                                bitmap = bmp;

                                // Ponemos nuestro bitmap en un ImageView que tengamos en la vista
                                imgPerfil.setImageBitmap(bmp);

                            }
                        }
                    }
                    catch (Exception e){

                    }
                }
                break;
        }
    }



    @Override
    public void onServerError() {
        hideProgressBar();

        if (getActivity() != null){
            CommonUtils.showMessage(getActivity(), "Error en el servidor");
        }

    }

    @Override
    public void onPhotoTooBig() {
        hideProgressBar();

        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), "El tamaño máximo permitido para la imagen son 400kB. Por favor, suba otra imagen.");
    }

    @Override
    public void onReported() {
        hideProgressBar();

        Toast.makeText(AnimensApplication.getContext(), "Reporte enviado", Toast.LENGTH_SHORT).show();

        try {
            ((AppCompatActivity) getActivity()).onBackPressed();
        } catch (Exception e) {
            Log.d("Marina", e.getMessage());
        }

//        if (getFragmentManager() != null)
//            getFragmentManager().popBackStack();
    }


}
