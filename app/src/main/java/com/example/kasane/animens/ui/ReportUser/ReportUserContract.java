package com.example.kasane.animens.ui.ReportUser;

import android.graphics.Bitmap;

public interface ReportUserContract {
    interface View{
        void onServerError();
        void onReported();
        void onPhotoTooBig();
    }

    interface Presenter{
        void sendReport(String nick, Bitmap imagen, String descripcion, String motivos);
    }
}
