package com.example.kasane.animens.data.network.model;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * Clase modelo que define la entidad Profile
 * @author Marina Espinosa
 */
public class Profile implements Serializable, Comparable<Profile> {
    private String user_id;
    private String name; // varchar(30)
    private String email;  // 254
    private String phoneNumber; // 17
    private String address; // 300
    private String birthdate;
    private String gender; // Posible enum
    private String description; // 80
    private String nick; // 15
    private String registerDate;

    private String picture;
    private Bitmap image;
//    private String picture64;

    public Profile(String user_id, String name, String birthdate, String gender, String description, String picture, String nick) {
        this.user_id = user_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.description = description;
        this.picture = picture;
        this.nick = nick;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }


    // Comparando por id
    @Override
    public int compareTo(@NonNull Profile o) {
        return this.getUser_id().compareTo(o.getUser_id());
    }



    // Comparando por nick
    public static class OrderByNick implements Comparator<Profile>{
        @Override
        public int compare(Profile o1, Profile o2) {
            return o1.getNick().compareTo(o2.getNick());
        }
    }
}
