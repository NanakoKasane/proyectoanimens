package com.example.kasane.animens.ui.prefs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.TwoStatePreference;
import android.util.Log;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;

import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;

/**
 * Fragment que gestiona las preferencias
 * @author Marina Espinosa
 */
public class MyPreferenceFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
//    FirebaseAuth mAuth;
//    FirebaseAuth.AuthStateListener mAuthListener;
//    boolean sesionIniciadaConGoogle = true;
    final String KEY_GUARDARUSUARIO = "guardarUsuario";

    // Añado el XML de preferencias
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override public void onStart() {
        super.onStart();
//        if (FirebaseAuth.getInstance().getCurrentUser() == null) // TODO. Porque ahí aún no cerramos la sesión, si el user es null es que no se ha iniciado con Google
//            sesionIniciadaConGoogle = false;
//        mAuth = FirebaseAuth.getInstance();
//        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Click de Cerrar Sesión. Cierra sesión con Google si se ha iniciado con Google y si no, la cierra normal
         */
        Preference pref_logout = getPreferenceScreen().findPreference("logout");
        pref_logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                CommonUtils.showDialogLogOut(getContext());
                return true;
            }
        });


        /**
         * Click de ver las condiciones de uso
         */
        Preference pref_terms = getPreferenceScreen().findPreference("termsofuse");
        pref_terms.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final String url = "https://marina.edufdezsoy.es/terminos-de-uso/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            }
        });

        /**
         * Click en abour us
         */
        Preference pref_aboutus = getPreferenceScreen().findPreference("aboutus");
        pref_aboutus.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                final String url = "https://marina.edufdezsoy.es/about-me/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;
            }
        });


        // Obtengo el valor de si se guarda (si se recuerda) o no el usuario y lo checkeo
        if (getContext() != null) {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
            String pass = pref.getString("password", null);
            SharedPreferences.Editor editor = pref.edit();
            TwoStatePreference twoStatePreference = (TwoStatePreference) findPreference(KEY_GUARDARUSUARIO);
            if (pass == null || pass.equals("")) {
                editor.putBoolean(KEY_GUARDARUSUARIO, false);
                twoStatePreference.setChecked(false);
            } else {
                editor.putBoolean(KEY_GUARDARUSUARIO, true);
                Log.d("Preferences", "Cambianda preferencia de guardar usuario a true (porque el usuario se ha recordado)");
                twoStatePreference.setChecked(true);
            }
            editor.commit();
        }



        /**
         *  Listener que inidica que se ha cerrado sesión con Google
         */
//        mAuthListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                // Cuando cerramos la sesión, vamos al login de nuevo:
//                if (sesionIniciadaConGoogle && firebaseAuth.getCurrentUser() == null){
//                    Intent intent = new Intent(getContext(), LoginActivity.class);
//                    startActivity(intent);
//                }
//            }
//        };



    }

    /**
     * Evento cada vez que se cambia una opción de las preferencias
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //  TODO. Switch con las claves de las preferencias


        // TODO. aqui lo de guardar usuario y leo el valor de la key guardarUsuario
        switch (key){
            case KEY_GUARDARUSUARIO:
                if (getContext() != null) {
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                    boolean seGuarda = pref.getBoolean(KEY_GUARDARUSUARIO, true);
                    SharedPreferences.Editor editor = pref.edit();

                    if (!seGuarda) {
                        // Quito la contraseña para que no se recuerde el usuario
                        editor.putString("password", "");
                    }
                    if (seGuarda) {
                        String pass = pref.getString("password", null);
                        if (pass == null) {
                            TwoStatePreference twoStatePreference = (TwoStatePreference) findPreference(KEY_GUARDARUSUARIO);
                            editor.putBoolean(KEY_GUARDARUSUARIO, false);
                            twoStatePreference.setChecked(false);
                        }
                    }
                    editor.commit();
                }
                break;
        }

        /*
        switch (key){
            case "tema":
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());
                String temaElegido = pref.getString("tema", "Azul");
                if (temaElegido.equals("Azul")){
                    getContext().setTheme(R.style.AppThemeNoActionBar);
                    getActivity().getWindow().setNavigationBarColor(getResources().getColor(R.color.primary));
                    getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
                }
                else if (temaElegido.equals("Rojo")){
                    getContext().setTheme(R.style.AppThemeNoActionBarRed);
                    getActivity().getWindow().setNavigationBarColor(getResources().getColor(R.color.colorRedPrimary));
                    getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorRedDark));
                    getContext().getTheme().applyStyle(R.style.AppThemeNoActionBarRed, true);
                }
                break;

        }
        */

    }


    /**
     * Registro el listener de cambiar opciones de las preferencias
     */
    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }


}
