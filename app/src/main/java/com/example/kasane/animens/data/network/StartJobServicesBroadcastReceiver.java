package com.example.kasane.animens.data.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;

public class StartJobServicesBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            AnimensApplication.scheduleCheckChatMessagesJobService(context);
            AnimensApplication.scheduleCheckFriendRequestsAcceptedJobService(context);
            AnimensApplication.scheduleCheckFriendRequestsReceivedJobService(context);
        }
        catch (Exception e){
            Log.d("JobServices", "No se han podido iniciar los servicios " + e.getMessage());
        }
    }
}