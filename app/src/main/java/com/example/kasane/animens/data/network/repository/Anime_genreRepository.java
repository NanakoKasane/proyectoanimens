package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.data.network.model.Anime_genres;

import java.util.ArrayList;

public class Anime_genreRepository {
    static private Anime_genreRepository anime_genreRepository;
    private ArrayList<Anime_genres> anime_genres;

    static {
        anime_genreRepository = new Anime_genreRepository();
    }

    private Anime_genreRepository(){
        anime_genres = new ArrayList<>();
        add();
    }

    private void add(){
        anime_genres.add(new Anime_genres("1", "Action", 300001));
        anime_genres.add(new Anime_genres("1", "Slice of Life", 56501));
        anime_genres.add(new Anime_genres("2", "Supernatural", 301));
        anime_genres.add(new Anime_genres("2", "Super Power", 90));
        anime_genres.add(new Anime_genres("5", "Romance", 30222));
        anime_genres.add(new Anime_genres("5", "Action", 300001));
        anime_genres.add(new Anime_genres("9", "Action", 300001));
        anime_genres.add(new Anime_genres("8", "Action", 300001));
        anime_genres.add(new Anime_genres("7", "Action", 300001));
        anime_genres.add(new Anime_genres("5", "Action", 300001));
        anime_genres.add(new Anime_genres("10", "Action", 300001));
        anime_genres.add(new Anime_genres("11", "Action", 300001));
        anime_genres.add(new Anime_genres("23", "Action", 300001));
        anime_genres.add(new Anime_genres("24", "Action", 300001));
        anime_genres.add(new Anime_genres("44", "Action", 300001));
        anime_genres.add(new Anime_genres("22", "Action", 300001));
        anime_genres.add(new Anime_genres("47", "Action", 300001));
        anime_genres.add(new Anime_genres("225", "Action", 300001));
        anime_genres.add(new Anime_genres("212", "Action", 300001));
        anime_genres.add(new Anime_genres("1", "Slice of Life", 56501));
        anime_genres.add(new Anime_genres("2", "Supernatural", 301));
        anime_genres.add(new Anime_genres("2", "Super Power", 90));
        anime_genres.add(new Anime_genres("5", "Romance", 30222));

    }


    public static Anime_genreRepository getAnime_genreRepository() {
        return anime_genreRepository;
    }

    public ArrayList<Anime_genres> getAnime_genres() {
        return anime_genres;
    }
}
