package com.example.kasane.animens.data.db.Repository;

import com.example.kasane.animens.data.db.DAO.AnimeDAO;
import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;
/**
 * Repository para sqlite
 * @author Marina Espinosa
 */
public class AnimeRepository {
    private static AnimeRepository animeRepository;
    private AnimeDAO dao;

    private AnimeRepository(){
        dao = new AnimeDAO();
    }

    public static AnimeRepository getInstance() {
        if (animeRepository == null)
            animeRepository = new AnimeRepository();
        return animeRepository;
    }

    public boolean insertAnimes(ArrayList<Anime> animes){
        return dao.insertAnimes(animes);
    }

    public int getAnimeCount(){
        return dao.getAnimeCount();
    }

    public ArrayList<Anime> getAnimeList(){
        return dao.getAnimeList();
    }

    public ArrayList<Anime> getAnimesOfThisGenre(String genre){
        return dao.getAnimesOfThisGenre(genre);
    }

    public void insertAnime(Anime anime){
        dao.insertAnime(anime);
    }

    public void updateAnime(Anime anime){
        dao.updateAnime(anime);
    }

//    public ArrayList<String> getGenresStringList(){
//        return dao.getGenresStringList();
//    }

    public void delete(String anime_id){
        dao.delete(anime_id);
    }


}
