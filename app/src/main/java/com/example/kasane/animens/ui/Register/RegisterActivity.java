package com.example.kasane.animens.ui.Register;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.example.kasane.animens.ui.Login.LoginsFirebaseActivity;
import com.example.kasane.animens.ui.PasswordReset.PasswordResetActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activity que gestiona el registro del usuario por la opción de email y contraseña
 * También manda un email de confirmación de la cuenta mediante Firebase
 * @author Marina Espinosa
 */
public class RegisterActivity extends AppCompatActivity implements RegisterView {
    @BindView(R.id.edPassword) EditText edPassword;
    @BindView(R.id.edEmail) EditText edEmail;
    @BindView(R.id.edPasswordConfirm) EditText edPasswordConfirm;
    @BindView(R.id.edNick) EditText edNick;
//    @BindView(R.id.edPhoneNumber) EditText edPhoneNumber;
    RegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);
        presenter = new RegisterPresenterImpl(this);

    }

    /**
     * onClick del botón SignIn, que lleva ocultarTeclado la Activity LoginActivity
     */
    @OnClick(R.id.tvGoSignIn) void irIniciarSesion(){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    /**
     * onClick del layout. Que oculta el teclado si se pulsa en él
     */
    @OnClick(R.id.constraintLayout) void ocultarTeclado(){
        try {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edPassword.getWindowToken(), 0);
        }
        catch (Exception e){}
    }

    /**
     * onClick del botón registar, que valida los datos introducidos
     */
    @OnClick(R.id.btRegister) void register(){
//        showProgressBar(null, getString(R.string.msg_validatingregister));

        if (!CommonUtils.isNetworkOnline()){
            CommonUtils.showMessage(RegisterActivity.this, R.string.msg_nointernet);
        }
        else {
            CommonUtils.hideKeyboard(RegisterActivity.this, getWindow().getDecorView());
            presenter.validateCredentials(edNick.getText().toString(), edEmail.getText().toString(),
                    null, edPassword.getText().toString(), // dPhoneNumber.getText().toString()
                    edPasswordConfirm.getText().toString());
        }
    }



    /**
     * Método que solicita el foco y abre el teclado (para escribir en el edittext) en la vista que se le pasa por parámetros.
     * @param view
     */
    private void solicitarFoco(View view){
        if (view.requestFocus()) {
            try {
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
            catch (Exception e){}
        }
    }

//    public void showMessage(int id){
//        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getString(id), Snackbar.LENGTH_LONG);
//        snackbar.show();
//    }


    @Override
    public void showPasswordError() {
        hideProgressBar();
        edPassword.setError(getString(R.string.err_passwordInvalid));
        solicitarFoco(edPassword);
    }

    @Override
    public void showPasswordConfirmError() {
        hideProgressBar();
        edPasswordConfirm.setError(getString(R.string.err_edPasswordConfirm_notTheSame));
        solicitarFoco(edPasswordConfirm);
    }

    @Override
    public void showNickEmptyError() {
        hideProgressBar();
        edNick.setError(getString(R.string.err_edNick_empty));
        solicitarFoco(edNick);
    }

    @Override
    public void showInvalidEmailOrPhoneError() {
        hideProgressBar();
        edEmail.setError(getString(R.string.err_edEmail_emailOrPhoneNumberInvalid));
        solicitarFoco(edEmail);
    }

    @Override
    public void successValidation() {
        hideProgressBar();
//        Toast.makeText(this, "Registro correcto!", Toast.LENGTH_SHORT).show();
//        this.finish();


        registerWithFirebase();
    }

    /**
     * Registro en firebase con email y contraseña
     */
    private void registerWithFirebase(){
        showProgressBar(null, getString(R.string.msg_registering));
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(edEmail.getText().toString(), edPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    hideProgressBar();
//                    Toast.makeText(RegisterActivity.this, "Registro correcto!", Toast.LENGTH_SHORT).show();


                    // TODO. mando email de verificación
                    if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                        FirebaseAuth.getInstance().getCurrentUser().sendEmailVerification()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(AnimensApplication.getContext(),
                                                    getString(R.string.sendedemail) + " " + edEmail.getText().toString(),
                                                    Toast.LENGTH_LONG).show();


                                            // TODO. Aquí es cuando inserto en la base de datos porque se ha registrado bien con firebase y mandado el email
                                            showProgressBar(null, getString(R.string.msg_registering));
                                            presenter.insertUser(edNick.getText().toString().toLowerCase(), edEmail.getText().toString(),
                                                    null, edPassword.getText().toString()); // edPhoneNumber.getText().toString()
//                    RegisterActivity.this.finish();
                                        } else {
                                            Log.d("Firebase", "sendEmailVerification", task.getException());
                                            Toast.makeText(AnimensApplication.getContext(), R.string.err_cannotregister, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }

                }
                else{
                    hideProgressBar();
                    Log.d("Firebase", "register", task.getException());

                    if(task.getException() instanceof FirebaseAuthUserCollisionException) { // TODO. FirebaseAuthUserCollisionException. An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address
                        CommonUtils.showMessage(RegisterActivity.this, getString(R.string.err_logincollision));
//                        Toast.makeText(AnimensApplication.getContext(), getString(R.string.err_logincollision), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        CommonUtils.showMessage(RegisterActivity.this, getString(R.string.err_cantregister));
//                        Toast.makeText(AnimensApplication.getContext(), R.string.err_cantregister, Toast.LENGTH_SHORT).show();
                        //  delete from en la BBD de ese email en PROFILE y USER porque no se ha registrado realmente
//                        presenter.deleteUser(edEmail.getText().toString());
                    }

                    // TODO.

                }
            }
        });
    }



    @Override
    public void onRegistered(String email) {
        // TODO.
        hideProgressBar();

        RegisterActivity.this.finish();
    }

    @Override
    public void onEmailExists() {
        hideProgressBar();
        CommonUtils.showMessage(RegisterActivity.this, R.string.err_email_exists);
    }

    @Override
    public void onNickExists() {
        hideProgressBar();
        CommonUtils.showMessage(RegisterActivity.this, R.string.err_nick_exists);
    }

    @Override
    public void onPhoneExists() {
        hideProgressBar();
        CommonUtils.showMessage(RegisterActivity.this, R.string.err_phone_exists);
    }

    @Override
    public void onError() {
        hideProgressBar();
        CommonUtils.showMessage(RegisterActivity.this, R.string.err_general_register);
    }

    @Override
    public void onNickError() {
        hideProgressBar();
        CommonUtils.showMessage(RegisterActivity.this, R.string.err_invalidNick);
    }


    private ProgressDialog progressDialog;

    public void hideProgressBar() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar(String titulo, String mensaje) {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(mensaje); // R.string.msg_loading
        if (titulo != null)
            progressDialog.setTitle(titulo); // R.string.app_name
        progressDialog.show();
    }


//    /**
//     * Método que valida los datos necesarios para registrarse
//     * @return
//     */
//    private boolean validar(){
//        return validarContrasena() & validoCorreoOTelefono() & validarNick();
//    }
//
//    /**
//     * Método que valida que el número de teléfono introducido sea válido
//     * @return
//     */
//    private boolean validarTelefono() {
//        String patron = "(\\+34|0034|34)?[ -]*(6|7)[ -]*([0-9][ -]*){8}";
//        if (isEmpty(edPhoneNumber))
//            return false;
//        if (edPhoneNumber.getText().toString().matches(patron))
//            return true;
//        // edPhoneNumber.setError("Número de teléfono no válido");
//        return false;
//    }
//
//    /**
//     * Método que valida si es teléfono o el correo son válidos (ya que solo es necesario indicar uno de ellos)
//     * @return
//     */
//    private boolean validoCorreoOTelefono() {
//        boolean isValid = false;
//
//        // Si ni el correo ni el teléfono es válido, no será válido
//        if (!validarCorreo() & !validarTelefono()) {
//            edEmail.setError(getString(R.string.err_edEmail_emailOrPhoneNumberInvalid));
//            isValid = false;
//            solicitarFoco(edEmail);
//        }
//        else {
//            // Si el correo no es válido y el teléfono sí, es válido
//            if (!validarCorreo() && validarTelefono())
//                isValid = true;
//            // Si el teléfono no es válido y el correo sí, es válido.
//            if (!validarTelefono() && validarCorreo())
//                isValid = true;
//
//            // Si no ha entrado por ninguno de los 2 casos anteriores, informaremos que no es válido
//            else if (!isValid){
//                edEmail.setError(getString(R.string.err_edEmail_emailInvalid));
//                solicitarFoco(edEmail);
//            }
//        }
//
//        return isValid;
//    }
//
//    /**
//     * Método que valida que el correo electrónico sea válido
//     * @return
//     */
//    private boolean validarCorreo(){
//        boolean isValid = true;
//        String patron = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
//        if (edEmail.getText().toString().isEmpty())
//            isValid = false;
//        else if (!edEmail.getText().toString().matches(patron))
//            isValid = false;
//
//        return isValid;
//    }
//
//    /**
//     * Método que valida que el nick sea válido
//     * @return
//     */
//    private boolean validarNick() { // TODO comprobar en la base de datos que no exista ya ese nick
//        if (!edNick.getText().toString().isEmpty())
//            return true; // que el nick no esté vacío, no exista
//        edNick.setError(getString(R.string.err_edNick_empty));
//        solicitarFoco(edNick);
//        return false;
//    }
//
//    /**
//     * Método que valida las contraseñas
//     * @return
//     */
//    private boolean validarContrasena() {
//        if (isEmpty(edPasswordConfirm)) {
//            edPasswordConfirm.setError(getString(R.string.err_edPasswordConfirm_empty));
//            solicitarFoco(edPasswordConfirm);
//        }
//        if (isEmpty(edPassword)) {
//            edPassword.setError(getString(R.string.err_edPassword_empty));
//            solicitarFoco(edPassword);
//        }
//        // TODO.
//        // TODO. if (edPassword.getText().toString().length() < 4 && edPassword.getText().toString().length() > 15)
//        else if (edPassword.getText().toString().equals(edPasswordConfirm.getText().toString()))
//            return true; // coinciden ambas contraseñas
//
//        if (!edPassword.getText().toString().equals(edPasswordConfirm.getText().toString()))
//            edPasswordConfirm.setError(getString(R.string.err_edPasswordConfirm_notTheSame));
//        return false;
//    }
//
//    /**
//     * determina si el editText está vacío o no
//     * @param editText
//     * @return
//     */
//    private boolean isEmpty(EditText editText){
//        if (editText.getText().toString().isEmpty())
//            return true;
//        return false;
//    }


}
