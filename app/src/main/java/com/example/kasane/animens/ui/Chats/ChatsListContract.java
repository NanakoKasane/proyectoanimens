package com.example.kasane.animens.ui.Chats;

import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Chats;

import java.util.ArrayList;

public interface ChatsListContract {
    interface View{
        void showChats(ArrayList<Chatroom> chats);
        void onServerError();
    }

    interface Presenter{
        void loadChats();
    }
}
