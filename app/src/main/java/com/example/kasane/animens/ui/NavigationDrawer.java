package com.example.kasane.animens.ui;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.ui.Anime.AnimeActivity;
import com.example.kasane.animens.ui.Chats.ChatsActivity;
import com.example.kasane.animens.ui.Genres.GenresListActivity;
import com.example.kasane.animens.ui.MessageBoard.MessageBoardActivity;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.example.kasane.animens.ui.adapter.NavigationDrawerAdapter;
import com.example.kasane.animens.data.network.repository.IconosListRepository;
import com.example.kasane.animens.ui.prefs.PreferencesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

// clase en desuso
public class NavigationDrawer extends AppCompatActivity {

    private String[] titulosMenu;
    private List<Integer> iconos;

    // @BindView(R.id.aboutView) AboutView aboutView;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
    @BindView(R.id.left_drawer) ListView lstDrawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        ButterKnife.bind(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        lstDrawer = (ListView) findViewById(R.id.left_drawer);

        titulosMenu = getResources().getStringArray(R.array.array_titulosNavigationDrawer);
        iconos = IconosListRepository.getIconosListRepository().getIconos();

        lstDrawer.setAdapter(new NavigationDrawerAdapter(NavigationDrawer.this, R.layout.item_navigationdrawer, titulosMenu, iconos)); //(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, titulosMenu));
        drawerLayout.openDrawer(Gravity.START);
        drawerLayout.setDrawerShadow(android.R.color.darker_gray, GravityCompat.START);


//        // About Us (lo que se muestra por defecto):
//        aboutUs();

        // Opciones seleccionadas
        lstDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;

                switch (position){
                    // Perfil
                    case 0:
                        intent = new Intent(NavigationDrawer.this, ProfileActivity.class);
                        startActivity(intent); // TODO. del usuario actual. Pasarle con putExtra el id del usuario (que está guardado en las preferencias, o el nick)
                    break;

                    // Gente
                    case 1:
                        intent = new Intent(NavigationDrawer.this, UserListActivity.class);
                        startActivity(intent);
                    break;

                    // Animes
                    case 2:
                        intent = new Intent(NavigationDrawer.this, AnimeActivity.class);
                        startActivity(intent);
                        break;

                    // Géneros
                    case 3:
                        intent = new Intent(NavigationDrawer.this, GenresListActivity.class);
                        startActivity(intent);
                        break;

                    // Chats
                    case 4:
                        intent = new Intent(NavigationDrawer.this, ChatsActivity.class);
                        startActivity(intent);
                        break;

                    // Tablón
                    case 5:
                        intent = new Intent(NavigationDrawer.this, MessageBoardActivity.class);
                        startActivity(intent);
                        break;

                    // Configuración
                    case 6:
                        intent = new Intent(NavigationDrawer.this, PreferencesActivity.class);
                        startActivity(intent);
                        break;
                }

                //startActivity(intent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.application_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itmenu_Preferences:
                Intent intent = new Intent(NavigationDrawer.this, PreferencesActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}
