package com.example.kasane.animens.ui.MessageBoard;


import android.content.Context;
import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.MessageBoard;

public class MessageBoardAddPresenter implements MessageBoardAddContract.Presenter, MessageBoardAddInteractor.EditMessageBoard, MessageBoardAddInteractor.ValidateMessageBoard {
    private MessageBoardAddContract.View view;
    private MessageBoardAddInteractor interactor;

    public MessageBoardAddPresenter(MessageBoardAddContract.View view){
        this.view = view;
        interactor = new MessageBoardAddInteractor();
    }


    @Override
    public void editMessageBoard(MessageBoard messageBoardAEditar, MessageBoard messageBoardEditado, Bitmap bitmap, Context context) {
        interactor.editMessageBoard(messageBoardAEditar, messageBoardEditado, bitmap, context, this);
    }

    @Override
    public void validateMessageBoard(String title, String message) {
        interactor.validateMessageBoard(title, message, this);
    }

    // Interactor Edit
    @Override
    public void onEdited() {
        view.onEdited();
    }

    // Interactor Validate
    @Override
    public void onTitleEmptyError() {
        view.showTitleEmptyError();
    }

    @Override
    public void onMessageEmptyError() {
        view.showMessageEmptyError();
    }

    @Override
    public void onValidated() {
        view.onValidated();
    }
}
