package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kasane.animens.R;

import java.util.List;

// En desuso
public class NavigationDrawerAdapter extends ArrayAdapter {
    Context context;
    List<Integer> iconos;

    public NavigationDrawerAdapter(@NonNull Context context, int resource, String[] nombres, List<Integer> iconos) {
        super(context, resource, nombres);
        this.context = context;
        this.iconos = iconos;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;

        if (convertView == null) // Hay que inflar
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_navigationdrawer, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.tvOpcionNavigation = view.findViewById(R.id.tvOpcionNavigation);
            viewHolder.ivIcono = view.findViewById(R.id.ivIcono);
            view.setTag(viewHolder);
        }
        else { // view reutilizada
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.ivIcono.setImageResource(iconos.get(position));
        viewHolder.tvOpcionNavigation.setText((String) getItem(position));


        return view;
    }

    private class ViewHolder{
        ImageView ivIcono;
        TextView tvOpcionNavigation;

    }


}
