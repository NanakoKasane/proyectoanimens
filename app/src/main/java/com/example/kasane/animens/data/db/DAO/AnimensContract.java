package com.example.kasane.animens.data.db.DAO;

/**
 * Clase contract para la creación de las tablas SQLite
 * @author Marina Espinosa
 */
public class AnimensContract {
    static final int VERSION = 3;
    static final String DATABASE_NAME = "Animens.db";

    public static class AnimeEntry{
        public static String TABLE_NAME = "anime";

        // Columnas
        public static String COLUMN_ANIME_ID = "anime_id";
        public static int INDEX_COLUMN_ANIME_ID = 0 ; // TODO. 0 o 1?
        public static String COLUMN_NAME = "name";
        public static int INDEX_COLUMN_NAME = 1;
        public static String COLUMN_GENRE = "genre";
        public static int INDEX_COLUMN_GENRE = 2;
        public static String COLUMN_TYPE = "type";
        public static int INDEX_COLUMN_TYPE = 3;
        public static String COLUMN_EPISODES = "episodes";
        public static int INDEX_COLUMN_EPISODES = 4;
        public static String COLUMN_RATING = "rating";
        public static int INDEX_COLUMN_RATING = 5;
        public static String COLUMN_PICTURE = "picture";
        public static int INDEX_COLUMN_PICTURE = 6;
        public static String COLUMN_ANIME_ID_INCREMENT = "anime_idIncrement";
        public static int INDEX_COLUMN_ANIME_ID_INCREMENT = 7;

        // Orden
        public static String DEFAULT_SORT = COLUMN_RATING;

        // Todas las columnas
        public static String[] ALL_COLUMN = {COLUMN_ANIME_ID, COLUMN_NAME, COLUMN_GENRE, COLUMN_TYPE,
        COLUMN_EPISODES, COLUMN_RATING, COLUMN_PICTURE, COLUMN_ANIME_ID_INCREMENT};

        // Crear tabla // TODO. %s real
        public static String SQL_CREATE_ENTRIES = String.format("create table %s (%s text primary key, %s text not null, %s text, %s text, %s text, %s double, %s text not null," +
                        " %s integer not null)",
                TABLE_NAME,
                COLUMN_ANIME_ID,
                COLUMN_NAME,
                COLUMN_GENRE,
                COLUMN_TYPE,
                COLUMN_EPISODES,
                COLUMN_RATING,
                COLUMN_PICTURE,
                COLUMN_ANIME_ID_INCREMENT
                );

        // Borar
        public static String SQL_DELETE_ENTRIES = "drop table if exists " + TABLE_NAME;

    }

    public static class Anime_Genres_Entry{
        public static String TABLE_NAME = "anime_genres";

        // Columnas
        public static String COLUMN_ANIME_ID = "anime_id";
        public static String COLUMN_GENRE = "genre";

        // Orden
        public static String DEFAULT_SORT = COLUMN_GENRE;

        // Todas las columnas
        public static String[] ALL_COLUMN = {COLUMN_ANIME_ID, COLUMN_GENRE};

        // Foreign key
        public static String FOREIGN_KEY = String.format("foreign key (%s) references %s (%s) on update cascade on delete restrict ",
                COLUMN_ANIME_ID, AnimensContract.AnimeEntry.TABLE_NAME, AnimeEntry.COLUMN_ANIME_ID);

        // Crear tabla
        public static String SQL_CREATE_ENTRIES = String.format("create table %s (%s text not null, %s text not null, primary key (%s, %s), %s )",
                TABLE_NAME,
                COLUMN_ANIME_ID,
                COLUMN_GENRE,
                COLUMN_ANIME_ID,
                COLUMN_GENRE,
                FOREIGN_KEY
                );

        // Borrar
        public static String SQL_DELETE_ENTRIES = "drop table if exists " + TABLE_NAME;

    }


    public static class GenresEntry{
        public static String TABLE_NAME = "genres";

        // Columnas
        public static String COLUMN_ID = "id";
        public static String COLUMN_GENRE = "genre";

        // TABLA
        public static String SQL_CREATE_ENTRIES = String.format("create table %s (%s integer primary key, %s text not null)",
                TABLE_NAME,
                COLUMN_ID,
                COLUMN_GENRE
        );

        // Borrar
        public static String SQL_DELETE_ENTRIES = "drop table if exists " + TABLE_NAME;
    }

}
