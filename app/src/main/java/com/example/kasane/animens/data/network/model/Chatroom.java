package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase modelo que define la entidad Chatroom
 * @author Marina Espinosa
 */
public class Chatroom implements Serializable, Comparable<Chatroom> {
    private int chat_id;
    private String nick1;
    private String nick2;
    private String picture_nick1;
    private String picture_nick2;
    private String lastMessage;
    private String timeLastMessage;
    private String senderLastMessage;
    private int isReaded; // 0 false, 1 true
    private String dateLastMessage;

    public Chatroom(int chat_id, String nick1, String nick2, String picture_nick1, String picture_nick2, String lastMessage, String timeLastMessage, String senderLastMessage, int isReaded, String dateLastMessage) {
        this.chat_id = chat_id;
        this.nick1 = nick1;
        this.nick2 = nick2;
        this.picture_nick1 = picture_nick1;
        this.picture_nick2 = picture_nick2;
        this.lastMessage = lastMessage;
        this.timeLastMessage = timeLastMessage;
        this.senderLastMessage = senderLastMessage;
        this.isReaded = isReaded;
        this.dateLastMessage = dateLastMessage;
    }

    public Chatroom() {
    }

    public int getChat_id() {
        return chat_id;
    }

    public void setChat_id(int chat_id) {
        this.chat_id = chat_id;
    }

    public String getNick1() {
        return nick1;
    }

    public void setNick1(String nick1) {
        this.nick1 = nick1;
    }

    public String getNick2() {
        return nick2;
    }

    public void setNick2(String nick2) {
        this.nick2 = nick2;
    }

    public String getPicture_nick1() {
        return picture_nick1;
    }

    public void setPicture_nick1(String picture_nick1) {
        this.picture_nick1 = picture_nick1;
    }

    public String getPicture_nick2() {
        return picture_nick2;
    }

    public void setPicture_nick2(String picture_nick2) {
        this.picture_nick2 = picture_nick2;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getTimeLastMessage() {
        return timeLastMessage;
    }

    public void setTimeLastMessage(String timeLastMessage) {
        this.timeLastMessage = timeLastMessage;
    }

    public String getSenderLastMessage() {
        return senderLastMessage;
    }

    public void setSenderLastMessage(String senderLastMessage) {
        this.senderLastMessage = senderLastMessage;
    }

    public int getIsReaded() {
        return isReaded;
    }

    public void setIsReaded(int isReaded) {
        this.isReaded = isReaded;
    }

    public String getDateLastMessage() {
        return dateLastMessage;
    }

    public void setDateLastMessage(String dateLastMessage) {
        this.dateLastMessage = dateLastMessage;
    }

    /**
     * Comparando por fecha y hora del último mensaje. De más reciente a más antiguo
     */
    @Override
    public int compareTo(@NonNull Chatroom o) {
        // si son null ambos son iguales
        if (this.getTimeLastMessage() == null && o.getTimeLastMessage() == null)
            return 0;
        if (this.getDateLastMessage() == null && o.getDateLastMessage() == null)
            return 0;

        // si alguno no es null va antes en la ordenación
        if (this.getDateLastMessage() == null && o.getDateLastMessage() != null)
            return 1;
        if (this.getDateLastMessage() != null && o.getDateLastMessage() == null)
            return -1;
        if (this.getTimeLastMessage() == null && o.getTimeLastMessage() != null)
            return 1;
        if (this.getTimeLastMessage() != null && o.getTimeLastMessage() == null)
            return -1;

        // TODO. solo comparo el time si la fecha es la misma
        if (this.getDateLastMessage().equals(o.getDateLastMessage())){
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            try {
                Date this_timeLastMessage = sdf.parse(this.getTimeLastMessage());
                Date other_timeLastMessage = sdf.parse(o.getTimeLastMessage());
                if (this_timeLastMessage.after(other_timeLastMessage))
                    return -1;
                else
                    return 1;
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }

        // TODO. Me vale con comparar las fechas
        else{
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date this_dateLastMessage = sdf.parse(this.getDateLastMessage());
                Date other_dateLastMessage = sdf.parse(o.getDateLastMessage());
                if (this_dateLastMessage.after(other_dateLastMessage))
                    return -1;
                else
                    return 1;
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }


    }
}
