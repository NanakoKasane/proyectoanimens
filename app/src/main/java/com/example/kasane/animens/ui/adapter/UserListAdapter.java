package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Adapter para mostrar el listado de usuarios de la aplicación
 * @author Marina Espinosa
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {
    private Context context;
    private ArrayList<User> users;
    private View.OnClickListener listenerUser;
    ArrayList<User> usersCopy = new ArrayList<>();

    public UserListAdapter(Context context, ArrayList<User> users){
        this.context = context;
        this.users = users;
    }


    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.userlist_item, parent, false);
        view.setOnClickListener(listenerUser);
        UserHolder userHolder = new UserHolder(view);
        return userHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final UserHolder holder, int position) {
        User user = users.get(position);

//        holder.imgUser.setImageResource(user.getPhoto());
        Picasso.get()
                .load(user.getPicture())
                .fit()
                .centerInside()
                .into(holder.imgUser, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onError(Exception e) {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }

                });


        holder.tvNombre.setText(user.getName());
        holder.tvNick.setText(user.getNick());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UserHolder extends RecyclerView.ViewHolder{
        ImageView imgUser;
        TextView tvNick;
        TextView tvNombre;
        ProgressBar progressBar;

        public UserHolder(View itemView) {
            super(itemView);
            imgUser = itemView.findViewById(R.id.imgUser);
            tvNick = itemView.findViewById(R.id.tvNick);
            tvNombre = itemView.findViewById(R.id.tvNombre);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    public User getItem(int position){
        return users.get(position);
    }

    public void setListenerUser(View.OnClickListener listenerUser) {
        this.listenerUser = listenerUser;
    }


    public void setUsers(ArrayList<User> users) {
        this.users = users;
        usersCopy = new ArrayList<>();
        usersCopy.addAll(users);
    }


    public void filter(String text) {
        users.clear();
        if(text.isEmpty()){
            users.addAll(usersCopy);
        } else{
            text = text.toLowerCase();
            for(User user : usersCopy){
                if(user.getNick().toLowerCase().contains(text) || user.getNick().toUpperCase().contains(text)){
                    users.add(user);
                }
            }
        }
        notifyDataSetChanged();
    }
}
