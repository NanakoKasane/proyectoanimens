package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

// Clase actualmente en desuso
public class Conversation implements Comparable<Conversation> {
    private String message_id;
    private String usersender_id;
    private String message;
    private String time_sended; // datetime
    private boolean isReaded;

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getUsersender_id() {
        return usersender_id;
    }

    public void setUsersender_id(String usersender_id) {
        this.usersender_id = usersender_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime_sended() {
        return time_sended;
    }

    public void setTime_sended(String time_sended) {
        this.time_sended = time_sended;
    }

    public boolean isReaded() {
        return isReaded;
    }

    public void setReaded(boolean readed) {
        isReaded = readed;
    }

    // Comparando por id
    @Override
    public int compareTo(@NonNull Conversation o) {
        return this.getMessage_id().compareTo(o.getMessage_id());
    }

    // Comparando por el id del user que lo manda
    public static class OrderByUserId implements Comparator<Conversation>{
        @Override
        public int compare(Conversation o1, Conversation o2) {
            return o1.getUsersender_id().compareTo(o2.getUsersender_id());
        }
    }
}
