package com.example.kasane.animens.data.db.DAO;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.kasane.animens.data.db.model.Genre;

import java.util.ArrayList;

/**
 * DAO de Genre (para sqlite)
 * @author Marina Espinosa
 */
public class GenreDAO {

    public int getGenresCount(){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
        long count = DatabaseUtils.queryNumEntries(sqLiteDatabase, AnimensContract.GenresEntry.TABLE_NAME);

        AnimensOpenHelper.getInstance().closeDatabase();
        return (int) count;
    }

    public boolean insertAllGenres(ArrayList<Genre> genres){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        sqLiteDatabase.beginTransaction();

        String sql = "INSERT INTO " + AnimensContract.GenresEntry.TABLE_NAME + " VALUES(?, ?)";
        SQLiteStatement stmt = sqLiteDatabase.compileStatement(sql);
        for (int i = 0; i  < genres.size(); i++) {
            stmt.bindLong(1, genres.get(i).getId());
            stmt.bindString(2, genres.get(i).getGenre());

            stmt.execute();
            stmt.clearBindings();
        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

        AnimensOpenHelper.getInstance().closeDatabase();
        return true;

    }

    public ArrayList<String> getGenreString(){
        ArrayList<String> genres = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
        Cursor cursor = sqLiteDatabase.query(AnimensContract.GenresEntry.TABLE_NAME, new String[]{AnimensContract.GenresEntry.COLUMN_GENRE}, null, null, null, null, null);
        if (cursor.moveToFirst()){
            do{
                genres.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }

        AnimensOpenHelper.getInstance().closeDatabase();
        return genres;
    }

}
