package com.example.kasane.animens.ui.Register;

public interface RegisterView {
    void showPasswordError();
    void showPasswordConfirmError();
    void showNickEmptyError();
    void showInvalidEmailOrPhoneError();

    void successValidation();

    void showProgressBar(String title, String mensaje);
    void hideProgressBar();

    void onEmailExists();
    void onNickExists();
    void onPhoneExists();
    void onError();
    void onNickError();

    void onRegistered(String email);

}
