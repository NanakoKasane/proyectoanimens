package com.example.kasane.animens.ui.Genres;

import com.example.kasane.animens.data.db.model.Anime_genresView;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Anime_genres;
import com.example.kasane.animens.data.network.model.Genres;

import java.util.ArrayList;

public interface GenresListContract {
    interface View{
        void showGenreList(ArrayList<Anime_genresView> genres);
        void onServerError();

        void onAnimesFromGenreObtained(ArrayList<Anime> animes, String genre);
    }

    interface Presenter{
        void getGenreList();

        void getAnimesFromGenre(String genre);
    }

}
