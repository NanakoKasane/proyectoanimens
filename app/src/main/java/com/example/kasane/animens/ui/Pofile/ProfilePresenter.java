package com.example.kasane.animens.ui.Pofile;

import android.content.Context;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.User.UserListInteractor;

import java.util.ArrayList;

public class ProfilePresenter implements ProfileContact.Presenter, ProfileInteractor.GetProfile, ProfileInteractor.SendFriendRequest, ProfileInteractor.RequestCanBeSent,  UserListInteractor.GetUserList, ProfileInteractor.DeleteFriend, ProfileInteractor.GetChatroom{ //, ProfileInteractor.GetAnimes, ProfileInteractor.GetFriends {
    private ProfileContact.View view;
    private ProfileInteractor interactor;

    public ProfilePresenter(ProfileContact.View view){
        this.view = view;
        interactor = new ProfileInteractor();
    }

    // Presenter
    @Override
    public void getProfile(Context context) {
        interactor.getProfile(context, this);
    }

    @Override
    public void getProfile(User user) {
        interactor.getProfile(user, this);
    }

    @Override
    public void sendFriendRequest(String nickCurrentUser, String nickUserFriend) {
        interactor.sendFriendRequest(nickCurrentUser, nickUserFriend, this);
    }

    @Override
    public void checkIfRequestCanBeSent(String nick) {
        interactor.checkIfRequestCanBeSent(nick, this);
    }

    @Override
    public void getFriendList() {
        UserListInteractor userListInteractor = new UserListInteractor();
        userListInteractor.getFriendList(CommonUtils.getCurrentUserNick(AnimensApplication.getContext()), this);
    }

    @Override
    public void deleteFriend(String friendNick) {
        interactor.deleteFriend(friendNick, this);
    }

    @Override
    public void getChatroom(String nick) {
        interactor.getChatroom(nick, this);
    }

//    @Override
//    public void getAnimes(Profile profile) {
//        interactor.getAnimes(profile, this);
//    }
//
//    @Override
//    public void getFriends(Profile profile) {
//        interactor.getFriends(profile, this);
//    }



    // Interactor getProfile
    @Override
    public void profileObtained(Profile profile) {
        view.loadProfile(profile);
    }

    @Override
    public void onUserListObtained(String nick, String email, ArrayList<User> users) {
        view.onFriendListObtained(users);
    }

    @Override
    public void onDeleted(String friendNick) {
        view.onFriendDeleted(friendNick);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onNoFriends() {

    }

    @Override
    public void onFriendRequestSended() {
        view.onFriendRequestSended();
    }

    @Override
    public void onRequestCanBeSent() {
        view.onRequestCanBeSent();
    }

    @Override
    public void onNoRequestCanBeSent() {
        view.onNoRequestCanBeSent();
    }


    // interactor chatroom
    @Override
    public void onObtainedChatroom(Chatroom chatroom) {
        view.onChatroomObtained(chatroom);
    }

    // interactor getAnimes
//    @Override
//    public void animesObtained(ArrayList<Anime> animes) {
//        view.loadAnimes(animes);
//    }
//
//    // interactor getFriends
//    @Override
//    public void friendsObtained(ArrayList<User> users) {
//        view.loadFriends(users);
//    }
}
