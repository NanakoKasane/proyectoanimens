package com.example.kasane.animens.Utils;

/**
 * Clase modelo de utilidad
 * @author Marina Espinosa
 */
public class Count {
    private String count;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

}
