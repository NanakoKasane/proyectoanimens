package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Chats;

import java.util.ArrayList;

public class ChatsListRepository {
    static private ChatsListRepository chatsListRepository;
    private ArrayList<Chats> chats;

    static {
        chatsListRepository = new ChatsListRepository();
    }

    private ChatsListRepository(){
        chats = new ArrayList<>();
        add();
    }
    private void add(){
        chats.add(new Chats("1",  "jose23", R.drawable.emirem, "Hola que tal?"));
        chats.add(new Chats("3",  "maria22", R.drawable.fondo, "Buenaaas ^^"));
        chats.add(new Chats("4",  "elena33", R.drawable.fondoabout));
        chats.add(new Chats("6",  "lulu11", R.drawable.emirem, "ues eso, que es verdad, podemos quedar mañana a las 5 de la tarde como dijiste, si te parece bien claro está ^^"));
        chats.add(new Chats("7",  "nerea", R.drawable.emirem));
        chats.add(new Chats("9",  "maria", R.drawable.emirem));
        chats.add(new Chats("10",  "jose23", R.drawable.emirem, "Hola soy jose"));
        chats.add(new Chats("11",  "jose23", R.drawable.emirem));
        chats.add(new Chats("12",  "jose23", R.drawable.emirem));
        chats.add(new Chats("14",  "jose23", R.drawable.emirem));
        chats.add(new Chats("16",  "jose23", R.drawable.emirem, "jajaja"));
        chats.add(new Chats("17",  "jose23", R.drawable.emirem));
        chats.add(new Chats("18",  "jose23", R.drawable.emirem));

    }

    public ArrayList<Chats> getChats() {
        return chats;
    }

    public static ChatsListRepository getChatsListRepository() {
        return chatsListRepository;
    }
}
