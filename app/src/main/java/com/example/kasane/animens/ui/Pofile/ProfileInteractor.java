package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.data.network.repository.AnimeListRepository;
import com.example.kasane.animens.data.network.repository.ProfileRepository;
import com.example.kasane.animens.data.network.repository.UserListRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class ProfileInteractor {

    public void getProfile(Context context, final ProfileInteractor.GetProfile listener){
        // Obtengo el usuario que inició sesión (guardado en las preferencias)
        String userNick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());


        // TODO. Obtengo el perfil de ese usuario (Consulta a la base de datos)

        try {
            String url = AnimensApplication.BASE_URL+"animens/getUserProfile.php?nick=" + userNick;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Profile> profiles = gson.fromJson(response.getJSONArray("profile").toString(), new TypeToken<ArrayList<Profile>>(){}.getType());
                        listener.profileObtained(profiles.get(0));

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }






        // Devolvemos el perfil de esa consulta
//        listener.profileObtained(ProfileRepository.getProfileRepository().getProfile()); // TODO. Ahora es de un repository pero será una consulta

    }

    public void getProfile(User user, ProfileInteractor.GetProfile listener){

        // TODO. Obtengo el perfil de ese usuario (Consulta a la base de datos)



        // Devolvemos el perfil de esa consulta
//        listener.profileObtained(ProfileRepository.getProfileRepository().getProfile()); // TODO. Ahora es de un repository pero será una consulta

    }


    // TODO. MANDO PETICIÓN de amistad
    public void sendFriendRequest(String nickCurrentUser, String nickUserFriend, ProfileInteractor.SendFriendRequest listener) {

        // TODO. Insert en la tabla notificaciones con los 2 nick de usuarios

        try {
            String url = AnimensApplication.BASE_URL+"animens/addFriendRequest.php?userWhoSendsRequest_nick=" + nickCurrentUser + "&userWhoReceivedRequest_nick=" + nickUserFriend ;
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    // TODO. listener.onSuccess();
                    Log.d("AnimensPHP", "ONSUCCESS (insertado en addFriendRequest)" + responseString );

                    listener.onFriendRequestSended();
                    // TODO. Luego Deshabilito el botón
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
//            listener.onServerError();
        }





        // TODO. En el navigation se mostrará una notificación nueva


    }


    public void checkIfRequestCanBeSent(String nick, RequestCanBeSent listener) {

        String nickUserActual = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        // TODO. Esto será si está en la tabla amigos o si existe en friendrequests el usuario actual con este nick (los 2 user)
        try {
            String url = AnimensApplication.BASE_URL+ "animens/checkFriendRequestCanBeSent.php?nick1=" + nick + "&nick2=" + nickUserActual;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("canBeSent")){
                            listener.onRequestCanBeSent();
                        }
                        else
                            listener.onNoRequestCanBeSent();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" +  throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }




    }

    public void deleteFriend(String friendNick, DeleteFriend listener) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("yourNick", CommonUtils.getCurrentUserNick(AnimensApplication.getContext()));
        requestParams.put("friendNick", friendNick);
        RestClient.post(AnimensApplication.BASE_URL+"animens/deleteFriend.php", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("ProfileInteractor", responseString + "\n" + throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                super.onFailure(statusCode, headers, responseBytes, throwable);
                listener.onServerError();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                listener.onDeleted(friendNick);
            }
        });
    }


    public void getChatroom(String nick, GetChatroom listener){
        RestClient.get(AnimensApplication.BASE_URL+"animens/getChatRoom.php?nick1=" + CommonUtils.getCurrentUserNick(AnimensApplication.getContext()) + "&nick2=" + nick, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<Chatroom> chatrooms = gson.fromJson(response.getJSONArray("chatroom").toString(), new TypeToken<ArrayList<Chatroom>>(){}.getType());
                    listener.onObtainedChatroom(chatrooms.get(0));

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensPHP", e.getMessage());
                    listener.onServerError(); //e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensPHP", responseString + "\n" + throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                listener.onServerError();
            }
        });


    }

    interface GetChatroom{
        void onObtainedChatroom(Chatroom chatroom);
        void onServerError();
    }

    interface DeleteFriend{
        void onDeleted(String friendNick);
        void onServerError();
    }

    public interface RequestCanBeSent{
        void onRequestCanBeSent();
        void onNoRequestCanBeSent();
        void onServerError();
    }

    public interface SendFriendRequest{
        void onFriendRequestSended();
    }


    public interface GetProfile{
        void profileObtained(Profile profile);
        void onServerError();
    }



//    public void getAnimes(Profile profile, ProfileInteractor.GetAnimes listener) {
//        // TODO. Consulta a la BD para obtener Animes favoritos DEL USUARIO ACTUAL. Si se sacan 0 resultados de la consulta, indicarlo ("No hay animes añadidos")
//
//        ArrayList<Anime> animes = AnimeListRepository.getAnimeListRepository().getAnimeList();
//        listener.animesObtained(animes);
//
//    }
//
//    public void getFriends(Profile profile, ProfileInteractor.GetFriends listener) {
//        // TODO. Consulta a la BD para obtener los amigos. Si se sacan 0 resultados de la consulta, indicarlo ("No hay amigos añadidos")
//
//        ArrayList<User> users = UserListRepository.getUserListRepository().getUsers();
//        listener.friendsObtained(users);
//
//
//    }





//    interface GetAnimes{
//        void animesObtained(ArrayList<Anime> animes);
//    }
//
//    interface GetFriends{
//        void friendsObtained(ArrayList<User> users);
//    }

}
