package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.Profile;

public interface ProfileEditContact {
    interface View{
        void onEdited(); // Hay que cargar de nuevo el perfil
        void onServerError();
        void onPhotoTooBig();
    }

    interface Presenter{
        // void validateProfile(String fechaNac, String imagen);

        void editProfile(String nombre, String genero, String fechaNac, String descripcion, String image, Context context, Bitmap bitmap); // TODO.
    }

}
