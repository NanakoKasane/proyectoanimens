package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.R;

import java.util.ArrayList;
import java.util.List;

public class IconosListRepository {
    private List<Integer> iconos;
    private static IconosListRepository iconosListRepository;
    static {
        iconosListRepository = new IconosListRepository();
    }

    private IconosListRepository(){
        iconos = new ArrayList<Integer>();
        anadirIconos();
    }
    private void anadirIconos(){
        iconos.add(R.drawable.ic_action_profile);
        iconos.add(R.drawable.ic_action_users);
        iconos.add(R.drawable.ic_action_animesearch);
        iconos.add(R.drawable.ic_action_animesearch);
        iconos.add(R.drawable.ic_action_chats);
        iconos.add(R.drawable.ic_action_email);
        iconos.add(R.drawable.ic_action_settings);
    }

    public List<Integer> getIconos() {
        return iconos;
    }

    public static IconosListRepository getIconosListRepository() {
        return iconosListRepository;
    }
}
