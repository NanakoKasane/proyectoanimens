package com.example.kasane.animens.ui.Notifications;

import android.content.Context;

import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;

public interface NotificationsContract {
    interface View{
        void showNotifications(ArrayList<FriendRequests> friendRequests);

        void onProfileObtained(Profile profile);
        void onFriendRequestCancelled();
        void onFriendRequestAccepted();
        void onNoNotifications();
        void onServerError();
    }

    interface Presenter{
        void getNotifications(Context context); // TODO. De este user (nick del que inició sesión), siempre

        void getUserProfile(String nick);
        void cancelFriendRequest(String friendrequest_id);
        void acceptFriendRequest(String friendrequest_id);

    }
}
