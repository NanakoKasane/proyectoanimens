package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.User;

import java.util.ArrayList;
import java.util.Collections;

public class UserListRepository {
    static private UserListRepository userListRepository;
    private ArrayList<User> users;
    static {
        userListRepository = new UserListRepository();
    }

    private UserListRepository(){
        users = new ArrayList<>();
        addAll();
    }

    private void addAll(){
        User user = new  User("NanakoKasane", "Marina Espinosa", R.drawable.emirem, "123456");
        user.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(user);

        User ismael = new User("@Ismael22", "Ismael Bueno Molina", R.drawable.fondo, "123456");
        ismael.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(ismael);

        User negri = new User("@Negri", "María", R.drawable.fondoabout, "123456");
        negri.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(negri);

        User nana = new User("@Nana", "Nana Galvez", R.drawable.emirem, "123456");
        nana.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(nana);

        User pepi = new User("@Pepi", "Pepita Espinosa", R.drawable.emirem, "123456");
        pepi.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(pepi);

        User lourdes = new User("lourdes", "Lourdes", R.drawable.emirem, "123456");
        lourdes.setPicture("https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png");
        users.add(lourdes);

    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public static UserListRepository getUserListRepository() {
        return userListRepository;
    }

    public boolean validateAuthentication(String user, String password){
        for (User u : users){
            if ( u.getNick().equals(user) && u.getPasswordEncrypted().equals(password) ) //
                return true;
        }
        return false;
    }

    public User getUserWithNick(String nick){
        for (User u : users){
            if (u.getNick().equals(nick) ) //
                return u;
        }
        return null; // No hay ningun usuario con ese nick
    }
}
