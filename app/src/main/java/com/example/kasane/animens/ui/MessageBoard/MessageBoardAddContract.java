package com.example.kasane.animens.ui.MessageBoard;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.MessageBoard;

public interface MessageBoardAddContract {
    interface View{
        void onEdited();

        void showTitleEmptyError();
        void showMessageEmptyError();
        void onValidated();
    }

    interface Presenter{
        void editMessageBoard(MessageBoard messageBoardAEditar, MessageBoard messageBoardEditado, Bitmap bitmap, Context context);
        void validateMessageBoard(String title, String message);
    }
}
