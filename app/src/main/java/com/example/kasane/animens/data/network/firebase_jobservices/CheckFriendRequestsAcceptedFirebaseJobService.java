package com.example.kasane.animens.data.network.firebase_jobservices;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.ui.User.UserListActivity;
//import com.firebase.jobdispatcher.JobParameters;
//import com.firebase.jobdispatcher.JobService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * JobService (de Firebase) que comprueba si hay mensajes si te han aceptado peticiones de amistad.
 * Este JobService se inicializa en AnimensApplication y se programa para que se ejecute cada X tiempo
 * Al funcionar con Firebase aunque la aplicación haya finalizado se siguen ejecutando
 * @author Marina Espinosa
 */
public class CheckFriendRequestsAcceptedFirebaseJobService{

}
/*
public class CheckFriendRequestsAcceptedFirebaseJobService extends JobService {
    @Override
    public boolean onStartJob(@NonNull JobParameters job) {
        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        RestClient.get("https://marina.edufdezsoy.es/animens/checkIfMyFriendRequestsAreAccepted.php?userWhoSendsRequest_nick=" + nick , new JsonHttpResponseHandler() {

            // TODO. On success -> obtengo la lista de peticiones aceptadas y mando con sus datos notificación
            // Mediante Broadcast notifico -> Le paso en el bundle serializable las notificaciones
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<FriendRequests> friendRequests = gson.fromJson(response.getJSONArray("friendrequests").toString(), new TypeToken<ArrayList<FriendRequests>>() {}.getType());

                    int contador = 0;
                    for (FriendRequests f : friendRequests) {
                        contador++;
                        Toast.makeText(AnimensApplication.getContext(), "El usuario " + f.getUserWhoReceivedRequest_nick() + " te ha aceptado la petición de amistad", Toast.LENGTH_SHORT).show();
                    }

                    if (contador > 1){
                        Intent intentPerfil = new Intent(AnimensApplication.getContext(), UserListActivity.class); // TODO. Esto luego irá a TUS AMIGOS
                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentPerfil, PendingIntent.FLAG_UPDATE_CURRENT);
                        createNotification("Varios usuarios han aceptado sus peticiones de amistad", pendingIntent, "Petición aceptada");
                    }
                    else{
                        Intent intentPerfil = new Intent(AnimensApplication.getContext(), UserListActivity.class); // TODO. Esto luego irá a TUS AMIGOS
                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentPerfil, PendingIntent.FLAG_UPDATE_CURRENT);
                        createNotification("El usuario " + friendRequests.get(0).getUserWhoReceivedRequest_nick() + " te ha aceptado la petición de amistad", pendingIntent, "Petición aceptada");
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensPHP", e.getMessage());
                    Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                }
            }

            // On failure -> será que no hay ninguna petición
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensPHP", "OnFailure" + responseString );
//                    listener.onServerError();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (throwable != null && throwable.getMessage() != null)
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (throwable != null && throwable.getMessage() != null)
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
            }
        });

        jobFinished(job, true);
        return true;
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        return false;
    }

    private void createNotification(String text, PendingIntent pendingIntent, String title){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AnimensApplication.getContext(), AnimensApplication.notificationChannel);
        notificationBuilder.setSmallIcon(R.mipmap.animenslogo);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(1, notificationBuilder.build());
    }

}
*/
