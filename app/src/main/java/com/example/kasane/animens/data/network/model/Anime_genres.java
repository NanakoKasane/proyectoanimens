package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Clase modelo que define la entidad Anime_genres
 * @author Marina Espinosa
 */
public class Anime_genres implements Comparable<Anime_genres> {
    private String anime_id;
    private String genre; // Nombre del género
    private int animeCount; // count(*) de animes de ese género

    public Anime_genres(String anime_id, String genre) {
        this.anime_id = anime_id;
        this.genre = genre;
    }

    public Anime_genres(String anime_id, String genre, int animeCount) {
        this.anime_id = anime_id;
        this.genre = genre;
        this.animeCount = animeCount;
    }


    public String getAnime_id() {
        return anime_id;
    }

    public void setAnime_id(String anime_id) {
        this.anime_id = anime_id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getAnimeCount() {
        return animeCount;
    }

    public void setAnimeCount(int animeCount) {
        this.animeCount = animeCount;
    }


    /**
     * Comparación por género (ordenado alfabéticamente)
     */
    @Override
    public int compareTo(@NonNull Anime_genres o) {
        return this.genre.compareTo(o.getGenre());
    }

    /**
     * Otros criterios de comparación
     */
    // Ordenación de más animes de ese género a menos (por count)
    public static class OrderByCount implements Comparator<Anime_genres> {
        @Override
        public int compare(Anime_genres o1, Anime_genres o2) {
            return o2.getAnimeCount()- o1.getAnimeCount();
        }
    }
}
