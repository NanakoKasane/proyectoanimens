package com.example.kasane.animens.ui.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.Utils.Count;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Profile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que realiza validaciones y peticiones al servidor
 * @author Marina Espinosa
 */
public class LoginInteractor {
    public LoginInteractor(){
    }

    public void validateCredentials(String user, String password, final LoginInteractor.ValidateCredentials listener){
        // user
        if (user.isEmpty() || user.length() > 15){
            listener.onError();
            return;
        }

        // password
        if (!CommonUtils.validatePassword(password)) {
            listener.onError();
            return;
        }


//        if (UserListRepository.getUserListRepository().validateAuthentication(user, password)) {
//            listener.onSuccess();
//            return;
//        }



        // TODO. Comprobar que el user exista en la BD -> esto será necesario, pero sin password, solo comprobar que exista el nick
        RequestParams params = new RequestParams();        // params.put("nombre", "valor");
        try {
            params.put("nick", user);
//            params.put("password", CommonUtils.sha256(password)); // TODO. Paso el SHA256 de la contraseña
//            Log.d("sha2", CommonUtils.sha256(password));

            String url = AnimensApplication.BASE_URL+ "animens/validateNick.php?"; // "nick=" + user + "&password=" + password;
            RestClient.post(url, params, new JsonHttpResponseHandler() { // "http://89.39.159.65/marina/animens/validateLogin.php
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Count> counts = gson.fromJson(response.getJSONArray("counts").toString(), new TypeToken<ArrayList<Count>>(){}.getType());
                        String count = counts.get(0).getCount();
                        if (Integer.parseInt(count) == 1){

                            // TODO. Obtengo el email de este user
                            try {
                                String url = AnimensApplication.BASE_URL+"animens/getUserProfile.php?nick=" + user;
                                RestClient.get(url, new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                        try {
                                            Gson gson = new Gson();
                                            ArrayList<Profile> profiles = gson.fromJson(response.getJSONArray("profile").toString(), new TypeToken<ArrayList<Profile>>(){}.getType());
                                            if (profiles.size() > 0){
                                                listener.onSuccess(profiles.get(0).getEmail());
                                            }
                                            else
                                                listener.onServerError();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Log.d("AnimensPHP", e.getMessage());
                                            Log.d("AnimensPHP", response.toString());
                                            listener.onServerError();
                                        }
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                        super.onFailure(statusCode, headers, responseString, throwable);
                                        Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                                        listener.onServerError();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                        super.onFailure(statusCode, headers, throwable, errorResponse);
                                        Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                                        listener.onServerError();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                                        super.onFailure(statusCode, headers, throwable, errorResponse);
                                        Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                                        listener.onServerError();
                                    }
                                });
                            }
                            catch (Exception e) {
                                Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
                                listener.onServerError();
                            }

                        }
                        else
                            listener.onError();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }


    }






    // TODO
//    public void getCurrentUser(Context context, LoginInteractor.CurrentUserObtained listener){
//        String nick = CommonUtils.getCurrentUserNick(context);
//
//
//
//        // TODO. Consulta. Obtengo el user que ha iniciado sesión
//
//        listener.onUserObtained(UserListRepository.getUserListRepository().getUserWithNick(nick));
//
//    }

    public void getCredentials(LoginInteractor.GetCredentials listener, Context context) {

        SharedPreferences prefs = context.getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);
        String user = prefs.getString("user", "");
        String password = prefs.getString("password", "");
        String email = prefs.getString("email", "");

        if (TextUtils.isEmpty(user) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password) ){
            listener.onNotObtainedCredentials();
        }
        else if (!TextUtils.isEmpty(user) && !TextUtils.isEmpty(email)){
            listener.onSuccessCredentials(user, email, password);
        }

    }

    public void saveCredentials(String user, String email, String password, Context context, LoginInteractor.SaveCredentials listener) {
        SharedPreferences prefs = context.getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user", user);
        editor.putString("email", email);
        if (!TextUtils.isEmpty(password)) {
            editor.putString("password", password);
            editor.commit();
            listener.onSavedCredentials();
        }
        else{
            editor.commit();
            listener.onSavedOnlyUser();
        }

    }

//    interface CurrentUserObtained{
//        void onUserObtained(User user);
//    }

    interface ValidateCredentials{
        void onError();
        void onSuccess(String email);
        void onServerError();
    }

    interface GetCredentials{
        void onSuccessCredentials(String user, String email, String password);
        void onNotObtainedCredentials();

    }

    interface SaveCredentials{
        void onSavedCredentials();
        void onSavedOnlyUser();
    }

}
