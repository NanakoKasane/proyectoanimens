package com.example.kasane.animens.ui.Login;

import android.content.Context;

public interface LoginsFirebaseContract {
    interface View{

        void onRegistered();
        void onNoRegistered();

        void onNickExists(String nick);
        void onNickDoesntExists(String nick);

        void onSuccessRegister();
        void onServerError();
        void onDataObtained();
    }

    interface Presenter{
        void getCurrentUserData(Context context, String email);
        void getIfUserIsRegistered(String email);

        void getIfNickExists(String nick);
        void registerUser(String email, String nick);
    }
}
