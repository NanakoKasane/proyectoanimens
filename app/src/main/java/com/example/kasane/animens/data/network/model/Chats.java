package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

// Clase actualmente en desuso
public class Chats implements Serializable, Comparable<Chats> {
    private String chat_id;
    private String nick; // usuario con el que se tiene el chat
    private int pictureUser; // TODO, esto lo quito de aquí, se sacará por una consulta de la foto de un usuario del cual es el NICK, no de la conversación

    private String time; // posible Datetime (hace 1 hora)
    private String last_message;
    Date hora;

    public Chats(String chat_id, String nick, int pictureUser) {
        this.chat_id = chat_id;
        this.nick = nick;
        this.pictureUser = pictureUser;
    }

    public Chats(String chat_id, String nick) {
        this.chat_id = chat_id;
        this.nick = nick;
    }

    public Chats(String chat_id, String nick, int pictureUser, String last_message) {
        this.chat_id = chat_id;
        this.nick = nick;
        this.pictureUser = pictureUser;
        this.last_message = last_message;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getPictureUser() {
        return pictureUser;
    }

    public void setPictureUser(int pictureUser) {
        this.pictureUser = pictureUser;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    // Comparo por id
    @Override
    public int compareTo(@NonNull Chats o) {
//        if (this.hora.before(o.hora))
//            return -1; // Si la hora está antes, va después (ya que es menos reciente)
//        return 1; // si no, irá antes
        return this.getChat_id().compareTo(o.getChat_id());
    }

    // Comparo por id
    public static class OrderByNick implements Comparator<Chats>{
        @Override
        public int compare(Chats o1, Chats o2) {
            return o1.getNick().compareTo(o2.getNick());
        }
    }


}
