package com.example.kasane.animens.data.network.repository;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.kasane.animens.data.network.model.Anime;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class AnimeListRepository {
    ArrayList<Anime> animeList;
    static private AnimeListRepository animeListRepository;
    static {
        animeListRepository = new AnimeListRepository();
    }

    private AnimeListRepository(){
        animeList = new ArrayList<>();
        anadirAnimes();
    }

    private void anadirAnimes(){
        animeList.add(new Anime("2", 1, "Charlotte", "13", "https://myanimelist.cdn-dena.com/images/anime/12/74683l.jpg", 7.91F));
        animeList.add(new Anime("10013", 5, "Shouwa Monogatari (Movie)", "1", "https://myanimelist.cdn-dena.com/images/anime/8/41119.jpg", 6.49F));
        animeList.add(new Anime("1", 2, "Cowboy Bebop", "26", "https://myanimelist.cdn-dena.com/images/anime/4/19644.jpg", 8.82F));
        animeList.add(new Anime("10000", 3, "Esper Mami Special: My Angel Mami-chan", "26", "https://myanimelist.cdn-dena.com/images/anime/2/27652.jpg", 6.52F));
        animeList.add(new Anime("10012", 4, "Carnival Phantasm", "12", "https://myanimelist.cdn-dena.com/images/anime/1018/92921.jpg", 8.01F));

//
//        Collections.sort(animeList); // TODO. Lista ordenada por ID
//
//        // TODO. Para ordenar por otros criterios sería (NOMBRE en este caso):
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            animeList.sort(new Anime.OrderByName());
//        }

        // TODO
//        Collections.sort(animeList, new Anime.OrderByName());
    }


    public static AnimeListRepository getAnimeListRepository() {
        return animeListRepository;
    }

    public ArrayList<Anime> getAnimeList() {
        return animeList;
    }

    public ArrayList<Bitmap> getImagesBitMap(){
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        for(Anime animeTmp : animeList){
            Bitmap bitmapTmp = getBitmapFromURL(animeTmp.getPicture());
            bitmaps.add(bitmapTmp);
        }
        return bitmaps;
    }

    // todo. A utils
    private static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Log.d("Marina", "Error al convertir a bitmap la imagen: " + src);
            return null;
        }
    }


}
