package com.example.kasane.animens.data.network.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
/**
 * (Actualmente sustituido por CheckFriendRequestsReceivedJobServiceFirebase)
 * Consulto si las peticiones de amistad enviadas a otros usuarios han sido aceptada
 */
public class CheckFriendRequestsReceivedService extends IntentService {
    private static final int MAX_TIMEOUT = 2000;
    private static final int RETRIES = 2;
    public static final String ACTION_SUCCESS = "com.example.kasane.animens.received.success";

    public CheckFriendRequestsReceivedService() {
        super("CheckFriendRequestsReceivedService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(0, null);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(RETRIES, MAX_TIMEOUT);

        String url = AnimensApplication.BASE_URL+ "animens/friendRequestsList.php?userWhoReceivedRequest_nick=" + nick;
        client.get(url, new JsonHttpResponseHandler() {

            // TODO. On success -> obtengo la lista de peticiones y me interesa cuántas hay
            // Mediante Broadcast notifico -> Le paso en el bundle serializable las notificaciones
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("AnimensPHP", "onsuccess" + response);
                try {
                    Gson gson = new Gson();
                    ArrayList<FriendRequests> friendRequests = gson.fromJson(response.getJSONArray("friendrequests").toString(), new TypeToken<ArrayList<FriendRequests>>(){}.getType());

                    // if (friendRequests.size() > 0 && friendRequests.size() > CommonUtils.getCurrentFriendRequestsCount()){
                        // Esto para el broadcast
                        CommonUtils.saveCountOfFriendRequests(friendRequests.size());
                        Bundle bundle = new Bundle();
                        bundle.putInt("peticiones", friendRequests.size());
                        Intent intentSuccess = new Intent();
                        intentSuccess.setAction(CheckFriendRequestsReceivedService.ACTION_SUCCESS);
                        intentSuccess.putExtras(bundle);
                        LocalBroadcastManager.getInstance(AnimensApplication.getContext()).sendBroadcast(intentSuccess);

                        Log.d("AnimensPHP", "Ahora habra una notificacion");


                        // Esto NO (sera en el broadcast)
//                        Intent intentNotificaciones = new Intent(AnimensApplication.getContext(), NotificationsActivity.class);
//                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentNotificaciones, PendingIntent.FLAG_UPDATE_CURRENT);
//                        int numRecibidas = friendRequests.size();
//                        if (numRecibidas > 1)
//                            createNotification("Te han mandado " + numRecibidas + " peticiones de amistad", pendingIntent, "Peticiones de amistad");
//                        else
//                            createNotification("Te han mandado una petición de amistad", pendingIntent, "Peticiones de amistad");
                  //  }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensPHP", e.getMessage());
                    Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
            }
        });
    }

    private void createNotification(String text, PendingIntent pendingIntent, String title){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AnimensApplication.getContext(), AnimensApplication.notificationChannel);
        notificationBuilder.setSmallIcon(R.mipmap.animenslogo);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

//        startForeground(0, notificationBuilder.build());
    }

    @Override
    public void onDestroy() {

        // Antes del super ?
//        Log.d("ServiceResucitado", "Destroying meee!");
////        Intent intent = new Intent(this, ServiceBroadcast.class);
////        sendBroadcast(intent); // TODO
//        Intent intent = new Intent(this, ServiceResucitante.class);
//        startService(intent); // TODO

        super.onDestroy();

    }
}
