package com.example.kasane.animens.ui.Login;

import android.content.Context;

import com.example.kasane.animens.data.network.model.User;

public class LoginPresenter implements LoginContract.Presenter, LoginInteractor.ValidateCredentials, LoginInteractor.GetCredentials, LoginInteractor.SaveCredentials{
    LoginContract.View view;
    LoginInteractor loginInteractor;

    public LoginPresenter(LoginContract.View view){
        this.view = view;
        loginInteractor = new LoginInteractor();
    }

    // presenter
    @Override
    public void validateCredentials(String user, String password) {
        loginInteractor.validateCredentials(user, password, this);
    }

//    @Override
//    public void getCurrentUser(Context context) {
//        loginInteractor.getCurrentUser(context, this);
//    }

    // Obtiene las credenciales (Si están guardadas)
    @Override
    public void getCredentials(Context context) {
        loginInteractor.getCredentials(this, context);
    }

    // Guarda las credenciales
    @Override
    public void saveCredentials(String user, String email, String password, Context context) {
        loginInteractor.saveCredentials(user, email, password, context, this);
    }


    // interactor validate credentials
    @Override
    public void onError() {
        view.showMessageError();
    }

    @Override
    public void onSuccess(String email) {
        view.sucessfulLogin(email);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }


    // interactor get credentials
    @Override
    public void onSuccessCredentials(String user, String email, String password) {
        view.onLoadCredentials(user, email, password);
    }

    @Override
    public void onNotObtainedCredentials() {
        view.onNotObtainedCredentials();
    }



    // interactor save credentials
    @Override
    public void onSavedCredentials() {
        view.onSavedCredentials();
    }


    // Solo se guarda el usuario y no se recuerda la contraseña ni nada
    @Override
    public void onSavedOnlyUser() {
    }


//    @Override
//    public void onUserObtained(User user) {
//        view.onCurrentUserObtained(user);
//    }
}
