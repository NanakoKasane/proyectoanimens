package com.example.kasane.animens.ui.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Chats;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Adapter para mostrar el listado de chatroom (salas de chat, 1 con cada usuario)
 * @author Marina Espinosa
 */
public class ChatroomAdapter extends RecyclerView.Adapter<ChatroomAdapter.ChatsHolder> {
    private Context context;
    private ArrayList<Chatroom> chats;
    private View.OnClickListener onClickListener;
    private ArrayList<Chatroom> chatroomsCopy;

    public ChatroomAdapter(Context context){
        this.context = context;
        this.chats = new ArrayList<>();
        this.chatroomsCopy = new ArrayList<>();
    }

    public void setChats(ArrayList<Chatroom> chats) {
        this.chats = chats;
        chatroomsCopy = new ArrayList<>();
        chatroomsCopy.addAll(chats);
    }


    @NonNull
    @Override
    public ChatsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.chatslist_item, parent, false);

        view.setOnClickListener(onClickListener);

        ChatsHolder chatsHolder = new ChatsHolder(view);
        return chatsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsHolder holder, int position) {
        Chatroom chat = chats.get(position);
        String pictureOtherUser;
        String otherUser;

        if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(chat.getNick1())){
            pictureOtherUser = chat.getPicture_nick2();
            otherUser = chat.getNick2();
        }
        else{
            pictureOtherUser = chat.getPicture_nick1();
            otherUser = chat.getNick1();
        }

        if (chat.getSenderLastMessage() != null && chat.getSenderLastMessage().equals(CommonUtils.getCurrentUserNick(AnimensApplication.getContext()))){
            holder.tvYou.setText("Tú: ");
            holder.tvYou.setVisibility(View.VISIBLE);
        }
        else {
            holder.tvYou.setText("");
            holder.tvYou.setVisibility(View.INVISIBLE);
        }

        holder.tvNick.setText(otherUser);
        holder.progressBar.setVisibility(View.VISIBLE);

        // IMAGEN
        Picasso.get()
                .load(pictureOtherUser)
                .fit()
                .centerInside()
                .error(R.drawable.imagenotfound)
                .into(holder.imgUser, new com.squareup.picasso.Callback()
                {
                    @Override
                    public void onSuccess() {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }
                    @Override
                    public void onError(Exception e) {
                        if (holder.progressBar != null) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }

                });

        // MOSTRAR HORA
        if (chat.getDateLastMessage() != null && chat.getTimeLastMessage() != null){
            try {
                // TODO. Si es de hoy, muestro la hora
                SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
                Date dateLastMessage = sdfDate.parse(chat.getDateLastMessage());
                if (DateUtils.isToday(dateLastMessage.getTime())){
                    Log.d("Animens", dateLastMessage +" Es hoy");
                    holder.tvHora.setText(chat.getTimeLastMessage());
                }
                else{
                    // TODO. si no es hoy, muestro la fecha en vez de la hora:
                    try {
                        Date date = sdfDate.parse(chat.getDateLastMessage());
                        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM", new Locale("es", "ES")); // new Locale("es_ES")); // dd 'de' MMMM
                        String horaFormated = sdf2.format(date);
                        holder.tvHora.setText(horaFormated);
                    } catch (ParseException e) {
                        holder.tvHora.setText(chat.getDateLastMessage());
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
                Log.d("Animens", e.getMessage());
            }
        }
        else
            holder.tvHora.setText("");


        // Si no está leído mensaje lo muestro en negrita:
        if (chat.getIsReaded() == 0 && chat.getSenderLastMessage() != null && chat.getSenderLastMessage().equals(otherUser)){
            holder.tvLastMessage.setTypeface(null, Typeface.BOLD);
        }
        else{
            holder.tvLastMessage.setTypeface(null, Typeface.NORMAL);
        }
        holder.tvLastMessage.setText(chat.getLastMessage());
        holder.tvLastMessage.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public class ChatsHolder extends RecyclerView.ViewHolder{
        TextView tvNick;
        TextView tvHora;
        TextView tvLastMessage;
        CircleImageView imgUser;
        ProgressBar progressBar;
        TextView tvYou;

        public ChatsHolder(View itemView) {
            super(itemView);
            tvNick = itemView.findViewById(R.id.tvNick);
            tvHora = itemView.findViewById(R.id.tvHora);
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage);
            imgUser = itemView.findViewById(R.id.imgUser);
            progressBar = itemView.findViewById(R.id.progressBar);
            tvYou = itemView.findViewById(R.id.tvYou);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public Chatroom getItem(int position){
        return chats.get(position);
    }

    public void filter(String text) {
        chats.clear();
        if(text.isEmpty()){
            chats.addAll(chatroomsCopy);
        } else{
            text = text.toLowerCase();
            for(Chatroom chatroom : chatroomsCopy){
                String nickOther;
                if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(chatroom.getNick1()))
                    nickOther =  chatroom.getNick2();
                else
                    nickOther = chatroom.getNick1();

                if(nickOther.toLowerCase().contains(text) || nickOther.toUpperCase().contains(text)){
                    chats.add(chatroom);
                }
            }
        }
        notifyDataSetChanged();
    }
}
