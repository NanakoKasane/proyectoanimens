package com.example.kasane.animens.ui.Splash;

import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.data.db.Repository.AnimeRepository;
import com.example.kasane.animens.data.db.Repository.Anime_genres_Repository;
import com.example.kasane.animens.data.db.Repository.GenresRepository;
import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Genre;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Anime;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class SplashInteractor {

    public void downloadAnimeData(final SplashInteractor.Download listener){

        // Solo descargaré los datos si no están ya descargados. Es decir si SQLITE tiene count(*) > 0 (filas insertadas con los datos)

        int anime_genresCount = Anime_genres_Repository.getInstance().getAnime_GenresCount();
        int animeCount = AnimeRepository.getInstance().getAnimeCount();

        if (anime_genresCount != 0 & animeCount != 0)
            listener.itWasAlreadyDownloaded();
        else if (animeCount == 0){
            Log.d("AnimensDB", "Insert ANIMES");

            RestClient.get(AnimensApplication.BASE_URL+"animens/animelist.php", new JsonHttpResponseHandler() {  // "http://192.168.1.132/listaAnimens/animelist.php
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Anime> animes = gson.fromJson(response.getJSONArray("animes").toString(), new TypeToken<ArrayList<Anime>>(){}.getType());

                        listener.onAnimeDownloaded(animes);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensDB", e.getMessage());
                        listener.onError(e.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                    listener.onError(responseString);
                }

            });

        }


    }

    public void downloadAnime_genresData(final SplashInteractor.Download listener){
        int anime_genresCount = Anime_genres_Repository.getInstance().getAnime_GenresCount();

        if (anime_genresCount == 0){
            Log.d("AnimensDB", "Insert ANIME_GENRES");

            RestClient.get(AnimensApplication.BASE_URL+"animens/genreslist.php", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Anime_genres> anime_genres = gson.fromJson(response.getJSONArray("anime_genres").toString(), new TypeToken<ArrayList<Anime_genres>>(){}.getType());

                        listener.onAnime_genres_Downloaded(anime_genres);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensDB", e.getMessage());
                        listener.onError(e.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                    listener.onError(responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensDB", errorResponse + "\n" + throwable.getMessage());
                    listener.onError(errorResponse.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensDB", errorResponse.toString() + "\n" + throwable.getMessage());
                    listener.onError(errorResponse.toString());
                }
            });
        }

    }

    public void downloadGenresData(final SplashInteractor.Download listener){
        int genresCount = GenresRepository.getInstance().getGenresCount();

        if (genresCount == 0){
            Log.d("AnimensDB", "Insert GENRES");

            RestClient.get(AnimensApplication.BASE_URL+"animens/genreStringList.php", new JsonHttpResponseHandler() { // http://89.39.159.65/marina/animens/genreStringList.php
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Genre> genres = gson.fromJson(response.getJSONArray("genres").toString(), new TypeToken<ArrayList<Genre>>(){}.getType());

                        listener.onGenresDownloaded(genres);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensDB", e.getMessage());
                        listener.onError(e.getMessage());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                    listener.onError(responseString);
                }

            });
        }

    }

    public void insertAnimes(ArrayList<Anime> animes, SplashInteractor.Insert listener) {
        if (AnimeRepository.getInstance().insertAnimes(animes)) // Si se han insertado
            listener.onInsertedAnimes();
    }

    public void insertAnime_genres(ArrayList<Anime_genres> anime_genres, SplashInteractor.Insert listener) {
        if (Anime_genres_Repository.getInstance().insertAnime_genres(anime_genres))
            listener.onInsertedAnime_genres();
    }

    public void insertGenres(ArrayList<Genre> genres, SplashInteractor.Insert listener){
        if (GenresRepository.getInstance().insertAllGenres(genres))
            listener.onInsertedGenres();
    }


    interface Download{
        void onAnimeDownloaded(ArrayList<Anime> animes);
        void onAnime_genres_Downloaded(ArrayList<Anime_genres> anime_genres);
        void onGenresDownloaded(ArrayList<Genre> genres);
        void onError(String errorMessage);

        void itWasAlreadyDownloaded();
    }

    interface Insert{
        void onInsertedAnimes();
        void onInsertedAnime_genres();
        void onInsertedGenres();
    }
}
