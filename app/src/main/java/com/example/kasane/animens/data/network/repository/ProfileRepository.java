package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;
import java.util.Date;

public class ProfileRepository {
    private static ProfileRepository profileRepository;
    private Profile profile;

    static {
        profileRepository = new ProfileRepository();
    }
    private ProfileRepository(){
        profile = new Profile("1", "lourdes", "1997/12/10", "Mujer", "Soy Lourdes. No me gustan los Animes pero probaré esta aplicación",
               "https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png", "lourdes" );
    }

    public static ProfileRepository getProfileRepository() {
        return profileRepository;
    }

    public Profile getProfile() {
        return profile;
    }
}
