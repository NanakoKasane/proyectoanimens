package com.example.kasane.animens.ui.Genres;

import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.data.db.model.Anime_genresView;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.repository.AnimeListRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class GenresListInteractor {


    public void getGenresList(GenresListInteractor.GetGenresList listener){
        //  Obtenemos el resultado de la consulta a la base de datos de:
        //    select genre, count(*) from anime_genres group by genre;

//        listener.onGenresObtained(Anime_genres_Repository.getInstance().getGenresAndCount());
        RestClient.get(AnimensApplication.BASE_URL+"animens/genresCountList.php", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<Anime_genresView> genres = gson.fromJson(response.getJSONArray("genres").toString(), new TypeToken<ArrayList<Anime_genresView>>(){}.getType());
                    listener.onGenresObtained(genres);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensDB", e.getMessage());
                    listener.onServerError(); //e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB", throwable.getMessage());
                listener.onServerError();
            }
        });




    }


    // En desuso (ahora en Anime):
    public void getAnimesFromGenre(String genre, GenresListInteractor.GetAnimeList listener) {

        // Consulta a la BD que obtiene los animes de este género:
        //     select * from anime where anime_id in (select anime_id from anime_genres where genre = 'Drama');    // limit (10) y al hacer scroll otra consulta

        ArrayList<Anime> animes = AnimeListRepository.getAnimeListRepository().getAnimeList();
        listener.onAnimesFromGenreObtained(animes, genre);

    }

    interface GetGenresList{
        void onGenresObtained(ArrayList<Anime_genresView> genres);
        void onServerError();
    }

    interface GetAnimeList{
        void onAnimesFromGenreObtained(ArrayList<Anime> animes, String genre);
    }
}
