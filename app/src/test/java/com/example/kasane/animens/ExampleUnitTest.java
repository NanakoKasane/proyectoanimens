package com.example.kasane.animens;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <ocultarTeclado href="http://d.android.com/tools/testing">Testing documentation</ocultarTeclado>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}