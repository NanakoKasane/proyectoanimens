package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class Categories implements Comparable<Categories>{
    private int id;
    private String category; // max 20

    public Categories(int id, String category) {
        this.id = id;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    // Comparo por id
    @Override
    public int compareTo(@NonNull Categories o) {
        return Integer.compare(this.getId(), o.getId());
    }

    // Comparo por nombre de la categoría
    public static class OrderByCategory implements Comparator<Categories>{
        @Override
        public int compare(Categories o1, Categories o2) {
            return o1.getCategory().compareTo(o2.getCategory());
        }
    }
}
