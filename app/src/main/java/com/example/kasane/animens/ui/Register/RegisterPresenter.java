package com.example.kasane.animens.ui.Register;

public interface RegisterPresenter {
    void validateCredentials(String nick, String email, String phone, String password, String passwordConfirm);
    void insertUser(String nick, String email, String phone, String password);
    void deleteUser(String email);
}
