package com.example.kasane.animens.ui.Anime;

import android.content.DialogInterface;
import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Genres;
import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;

public interface AnimeListContact {

    interface View {
        void loadAnimes(ArrayList<Anime> animes, ArrayList<Bitmap> images);
        void showProgressBar(String titulo, String mensaje, boolean cancelable, DialogInterface.OnCancelListener cancelListener);
        void hideProgressBar();

        void onDeleted();
        void onServerError();
        void onNoResults();

        void onUserProfile(Profile profile);
    }

    interface ViewEditRating{
        void onRatingEdited(Anime anime, float rating);
    }

    interface Presenter{
      //  void getAnimeList();
        void getAnimeList(ArrayList<Anime> animes);
        void getAnimeListOfThisGenre(String genre);
        void getAnimeListOfThisUser(String nick);

        void editRating(Anime anime, float rating);

        void deleteAnime(Anime anime);

        void getUserProfile(String nick);
    }

}
