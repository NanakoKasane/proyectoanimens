package com.example.kasane.animens.ui.Anime;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.ui.Pofile.ProfileFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Fragment que muestra el detalle de un Anime
 * @author Marina Espinosa
 */
public class AnimeDetailFragment extends Fragment implements AnimeListContact.ViewEditRating {
    public static String TAG = "AnimeDetailFragment";
    TextView tvTituloAnime;
    ImageView imgDetalleAnime;
    TextView tvNumCapitulos;
    TextView tvTipo;
    TextView tvFav;
    MaterialRatingBar ratingBarAnime;
    TextView tvValoracionSobre10;
    TextView tvGenres;
    FloatingActionButton btnEdit;

    Anime anime;
    Bitmap bitmap;

    AnimeListPresenter presenter;
    OnAnimeEdit listener;

    ProgressBar progressBar;
//    Bitmap imageBitmap;

    interface OnAnimeEdit{
        void onEdit(Anime anime);
    }

    public static AnimeDetailFragment newInstance(Bundle bundle){
        AnimeDetailFragment animeDetailFragment = new AnimeDetailFragment();
        if (bundle != null)
            animeDetailFragment.setArguments(bundle);
        return animeDetailFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            this.listener = (OnAnimeEdit)context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements OnAnimeEdit");
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_anime_detail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        inicializar();

        if (getArguments() != null) {
            anime = (Anime) getArguments().getSerializable("Anime");

            //        if (anime.getImage() != null) {
//            byte[] byteArray = anime.getImage();
//            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//        }

            if (anime != null && anime.getPicture() != null) {
                Picasso.get()
                        .load(anime.getPicture())
                        .fit()
                        .centerInside()
                        .error(R.drawable.imagenotfound)
                        .into(imgDetalleAnime, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                if (progressBar != null) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onError(Exception e) {
                                if (progressBar != null) {
                                    progressBar.setVisibility(View.GONE);
                                }
                            }

                        });

//                Picasso.get()
//                        .load(anime.getPicture())
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                imageBitmap = bitmap;
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
//
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                            }
//                        });
            }


            binding();

        }

//        else if (anime.getPicture() != null && !TextUtils.isEmpty(anime.getPicture())){
//            Toast.makeText(this, "Debe recargar para ver la foto", Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setRetainInstance(true);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        anime = (Anime) getArguments().getSerializable("Anime");
////        if (anime.getImage() != null) {
////            byte[] byteArray = anime.getImage();
////            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
////        }
//
//        if (anime.getPicture() != null){
//            Picasso.get()
//                    .load(anime.getPicture())
//                    .fit()
//                    .centerInside()
//                    .into(imgDetalleAnime,  new com.squareup.picasso.Callback() {
//                        @Override
//                        public void onSuccess() {
//                            if (progressBar != null) {
//                                progressBar.setVisibility(View.GONE);
//                            }
//                        }
//
//                        @Override
//                        public void onError(Exception e) {
//                            if (progressBar != null) {
//                                progressBar.setVisibility(View.GONE);
//                            }
//                        }
//
//                    });
//        }
//
//        binding();
//    }


    private void inicializar(){
        if (getView() == null)
            return;
        progressBar = getView().findViewById(R.id.progressBar);
        tvTituloAnime = getView().findViewById(R.id.tvTituloAnime);
        imgDetalleAnime = getView().findViewById(R.id.imgDetalleAnime);
        tvNumCapitulos = getView().findViewById(R.id.tvNumCapitulos);
        tvTipo = getView().findViewById(R.id.tvTipo);
        tvFav = getView().findViewById(R.id.tvFav);
        ratingBarAnime = getView().findViewById(R.id.ratingBarAnime);
        tvValoracionSobre10 = getView().findViewById(R.id.tvValoracionSobre10);
        tvGenres = getView().findViewById(R.id.tvGenres);
        btnEdit = getView().findViewById(R.id.btnEdit);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onEdit(anime);

//                Intent intent = new Intent(AnimeDetailFragment.this, AnimeAddFragment.class);
//                // todo pasar los datos a editar y boolean editar
//                intent.putExtra("editar", true);
//                intent.putExtra("Anime", anime);
//
//                startActivity(intent);

               // getFragmentManager().popBackStack(); // TODO. No guardar en la pila
            }
        });

//        imgDetalleAnime.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (getContext() != null && imageBitmap != null) { // solo si ya se ha cargado la imagen, la mostraré en grande
//                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
//                    LayoutInflater factory = LayoutInflater.from(getContext());
//                    View mView = factory.inflate(R.layout.dialog_animeimage, null);// ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_profileimage, null);
//                    ImageView photoView = mView.findViewById(R.id.dialog_imageview);
//                    photoView.setImageBitmap(imageBitmap);
//
//                    mBuilder.setView(mView);
//                    AlertDialog mDialog = mBuilder.create();
//                    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                    mDialog.show();
////                    Window window = mDialog.getWindow();
////                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//                }
//            }
//        });

        // TODO. Quito posibilidad de editar:
        btnEdit.setVisibility(View.GONE);

        ratingBarAnime.setOnRatingBarChangeListener(new myOnRatingBarChangesListener());

    }

    private void binding(){
        tvTituloAnime.setText(anime.getName());
        imgDetalleAnime.setImageBitmap(bitmap);
        tvNumCapitulos.setText(anime.getEpisodes());
        tvTipo.setText(anime.getType());
        tvFav.setText(""); // todo, de la base de datos
        ratingBarAnime.setRating((float) anime.getRatingPer5());
        tvValoracionSobre10.setText(String.valueOf(anime.getRating()) + "/10");
        tvGenres.setText(anime.getGenre());
    }

    @Override
    public void onRatingEdited(Anime anime, float rating) {
//        Toast.makeText(AnimensApplication.getContext(), "Voto registrado: " + rating + " para el anime " + anime.getName() , Toast.LENGTH_SHORT).show();
    }

    class myOnRatingBarChangesListener implements MaterialRatingBar.OnRatingBarChangeListener {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            // Toast.makeText(AnimeDetailFragment.this, "Voto registrado: " + rating , Toast.LENGTH_SHORT).show();

            tvValoracionSobre10.setText(String.valueOf(CommonUtils.getRatingPer10(rating)) + "/10");

            if (fromUser) {
                Toast.makeText(AnimensApplication.getContext(), "Voto registrado: " + CommonUtils.getRatingPer10(rating) + " para el anime " + anime.getName(), Toast.LENGTH_SHORT).show();

                presenter = new AnimeListPresenter((AnimeListContact.ViewEditRating) AnimeDetailFragment.this);
                presenter.editRating(anime, CommonUtils.getRatingPer10(rating));
//                Toast.makeText(AnimensApplication.getContext(), "Voto registrado: " + rating + " para el anime " + anime.getName() , Toast.LENGTH_SHORT).show();


            }

        }
        // después de esto, se irá a la base de datos y se calculará el nuevo voto teniendo en cuenta este voto y los anteriores
    }


}
