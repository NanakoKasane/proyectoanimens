package com.example.kasane.animens.ui.Base;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;

/**
 * BaseFragment que contiene métodos comunes usados por los fragments
 * @author Marina Espinosa
 */
public class BaseFragment extends Fragment {
    private ProgressDialog progressDialog;

    public void hideProgressBar() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar(String titulo, String mensaje, boolean cancelable, DialogInterface.OnCancelListener listener) {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(cancelable);
        progressDialog.setOnCancelListener(listener);

        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(mensaje); // R.string.msg_loading
        if (titulo != null)
            progressDialog.setTitle(titulo); // R.string.app_name
        progressDialog.show();
    }

    public void buildNotification(String title, String text, PendingIntent pendingIntent, Context context){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, AnimensApplication.notificationChannel);
        builder.setSmallIcon(R.mipmap.animenslogo);
        builder.setContentTitle(title);
        builder.setContentText(text);
        builder.setAutoCancel(true);
        builder.setContentIntent(pendingIntent);

        if (notificationManager != null)
            notificationManager.notify(0, builder.build()); // id de la notificación
    }

    public void showInformationDialog(String title, String text, DialogInterface.OnClickListener listenerOk){
        if (getContext() != null) {
            AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            alertDialog.setTitle(title);
            alertDialog.setMessage(text);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.dialog_ok), listenerOk);
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }



}
