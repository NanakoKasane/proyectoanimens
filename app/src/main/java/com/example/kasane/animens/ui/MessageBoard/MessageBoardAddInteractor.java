package com.example.kasane.animens.ui.MessageBoard;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.repository.MessageBoardListRepository;
import com.example.kasane.animens.data.network.repository.ProfileRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageBoardAddInteractor {

    public void editMessageBoard(final MessageBoard messageBoardAEditar, final MessageBoard messageBoardEditado, final Bitmap bitmap, Context context, final MessageBoardAddInteractor.EditMessageBoard listener) {
        final ArrayList<MessageBoard> messageBoards = MessageBoardListRepository.getMessageBoardListRepository().getMessageBoards();

        // Es añadir
//        if (messageBoardAEditar == null){
//            messageBoards.add(messageBoardEditado);
//        }
//        else{
//            int indice = messageBoards.indexOf(messageBoardAEditar);
//            if (indice != -1) {
//                messageBoards.set(indice, messageBoardEditado);
//            }
//        }



        // TODO. SUBO IMAGEN AL SERVER:
        if (bitmap != null){
//            uploadImage(bitmap, context);

            String url = AnimensApplication.BASE_URL+ "animens/imagenes/upload.php" ;// "http://marina.alumno.mobi/imagenes/upload.php";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String Response = jsonObject.getString("response");
//                    Toast.makeText(getContext(), "La imagen está en: " + "http://marina.alumno.mobi/imagenes/" + Response, Toast.LENGTH_LONG).show();

                        String imagen = AnimensApplication.BASE_URL+"animens/imagenes/" + Response; // "http://marina.alumno.mobi/imagenes/" + Response;





                        // TODO. UPDATE AL MESSAGEBOARD, LE AÑADO ESTA IMAGEN SUBIDA:

                        // AÑADIR
                        if (messageBoardAEditar == null){
                            messageBoardAEditar.setPicture(imagen);
                            messageBoards.add(messageBoardEditado);

                        }
                        else{ // EDITAR
                            int indice = messageBoards.indexOf(messageBoardAEditar);
                            if (indice != -1) {
                                messageBoardEditado.setPicture(imagen);
                                messageBoards.set(indice, messageBoardEditado);
                            }
                        }



                        // TODO. Ya se ha editado:
                        listener.onEdited();




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), "error: " + error.toString(), Toast.LENGTH_LONG).show();
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    String gambar = CommonUtils.imagetoString(bitmap);
                    params.put("foto", gambar);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }
        else{ // SI LA IMAGEN ES NULL
            // AÑADIR
            if (messageBoardAEditar == null){
                messageBoards.add(messageBoardEditado);

            }
            else{ // EDITAR
                int indice = messageBoards.indexOf(messageBoardAEditar);
                if (indice != -1) {
                    messageBoards.set(indice, messageBoardEditado);
                }
            }
            listener.onEdited();
        }

    }


    public void validateMessageBoard(String title, String message, MessageBoardAddInteractor.ValidateMessageBoard listener) {
        if (TextUtils.isEmpty(title)){
            listener.onTitleEmptyError();
            return;
        }
        if (TextUtils.isEmpty(message)){
            listener.onMessageEmptyError();
            return;
        }
        listener.onValidated();
    }

    interface EditMessageBoard{
        void onEdited();
    }

    interface ValidateMessageBoard{
        void onTitleEmptyError();
        void onMessageEmptyError();

        void onValidated();
    }
}
