package com.example.kasane.animens.ui.User;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.Anime.AnimeActivity;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.Pofile.ProfileFragment;
import com.example.kasane.animens.ui.adapter.UserListAdapter;
import com.example.kasane.animens.ui.prefs.MyPreferenceFragment;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity que gestiona el listado de usuarios. Carga los fragment
 * @author Marina Espinosa
 */
public class UserListActivity extends BaseActivity implements UserListFragment.GoToUser, ProfileFragment.Listener {
    Fragment userListFragment;
    Fragment profileFragment;
    Profile userProfile;
    boolean notificacion = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_user_list);
//        drawerLayout.closeDrawers();
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.nav_users);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open,  R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        if (getIntent().getBooleanExtra("PrimeraVez", false)){
            drawerLayout.openDrawer(GravityCompat.START);
            getIntent().putExtra("PrimeraVez", false);
        }

        // Viene de notificación y va a los amigos
        if (getIntent().getStringExtra("nick") != null){
            notificacion = true;
            this.getFriends(getIntent().getStringExtra("nick"));
        }
        // Carga los usuarios normal
        else {
            userListFragment = getSupportFragmentManager().findFragmentByTag(UserListFragment.TAG);
            if (userListFragment == null) {
                userListFragment = new UserListFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.mcontent, userListFragment, UserListFragment.TAG);
                fragmentTransaction.commit();
            }
        }




    }


    // TODO.
    @Override
    public void goToUser(Profile profile) {
        this.userProfile = profile;
        // TODO. Cargo el  perfil del user


//        Toast.makeText(this, "Yendo al perfil del usuario " + profile.getNick(), Toast.LENGTH_SHORT).show();
//        profileFragment = getSupportFragmentManager().findFragmentByTag(ProfileFragment.TAG);
//        if (profileFragment == null){

        // TODO. Sale el nombre del user:
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(profile.getNick());
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Bundle bundle = new Bundle();
            bundle.putSerializable("Profile", profile);
            profileFragment = ProfileFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mcontent, profileFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
       // }


    }



    @Override
    public void getFriends(String nick) {
//        Toast.makeText(this, "Amigos del usuario " + nick, Toast.LENGTH_SHORT).show();

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.friendfromprofile);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // TODO. Pasar bundle con el nick para luego obtener sus amigos
        Bundle bundle = new Bundle();
        bundle.putString("Nick", nick);
        userListFragment = UserListFragment.newInstance(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mcontent, userListFragment, UserListFragment.TAG);
        if (!notificacion)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void getAnimes(String nick) {
//        Toast.makeText(AnimensApplication.getContext(), "Animes favoritos del usuario: " + nick, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(UserListActivity.this, AnimeActivity.class);
        intent.putExtra("Nick", nick);
        startActivity(intent);

    }

    @Override
    public void onProfileEdit(Profile profile) {

        // TODO. Deshabilitar el botón si el user no es null (en profileFragment)

        Toast.makeText(AnimensApplication.getContext(), "No puede editar un perfil que no es suyo", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else {
                super.onBackPressed();
                if (getSupportActionBar() != null) {
//                if (getSupportFragmentManager().getBackStackEntryCount() == 1 && userProfile != null && !CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(userProfile.getNick()))
//                    getSupportActionBar().setTitle(userProfile.getNick());
//                else
                    getSupportActionBar().setTitle(R.string.nav_users);

                }
            }
        }
    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        super.onBackPressed();
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                FragmentManager fm = getSupportFragmentManager();
//                if (fm.getBackStackEntryCount() > 0) {
//                    fm.popBackStack();
//                }
//                return true;
//            default:
//                if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//                    return true;
//                }
//                return true;
//
//
//        }
//    }




}
