package com.example.kasane.animens.ui.MessageBoard;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.adapter.MessageBoardListAdapter;

import java.util.ArrayList;
import java.util.Collections;

public class MessageBoardListFragment extends Fragment implements MessageBoardListContract.View {
    public static String TAG = "MessageBoardListFragment";

    RecyclerView rcvMessageBoard;
    FloatingActionButton btnAdd;
    ArrayList<Bitmap> bitmaps;
    MessageBoardListAdapter messageBoardListAdapter;
    SwipeRefreshLayout swipeRefreshLayout;

    MessageBoardListPresenter presenter;
    Listener listener;

    interface Listener{
        void onDetail(MessageBoard messageBoard);
        void onEdit(MessageBoard messageBoard);
        void onUserProfile(Profile profile);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            listener = (Listener) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context + " must implements Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messageboard_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        rcvMessageBoard = view.findViewById(R.id.rcvMessageBoard);
        btnAdd = view.findViewById(R.id.btnAdd);

        // SwipeRefreshLayout:
        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimaryDark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true); // Se muestra cargando, el símbolo

                presenter.getMessages();

                swipeRefreshLayout.setRefreshing(false); // Deja de refrescar
            }
        });



        // Click del botón añadir
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onEdit(null);
//                Intent intent = new Intent(getContext(), MessageBoardAddFragment.class);
//                startActivity(intent);
            }
        });
        rcvMessageBoard.setLayoutManager(new LinearLayoutManager(getContext()));


        presenter = new MessageBoardListPresenter(this);
        presenter.getMessages();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getMessages();
    }

    @Override
    public void showMessages(ArrayList<MessageBoard> messageBoards, ArrayList<Bitmap> bitmaps) {
//        messageBoardListAdapter.setMessageBoards(messageBoards);
//        messageBoardListAdapter.setBitmaps(bitmaps);
//        messageBoardListAdapter.notifyDataSetChanged();


        messageBoardListAdapter = new MessageBoardListAdapter(getContext(), messageBoards, bitmaps);

        // Listeners
        messageBoardListAdapter.setListenerDetallesMensaje(new ListenerDetallesActivity());
        messageBoardListAdapter.setListenerPerfilUser(new ListenerPerfilUser());
        messageBoardListAdapter.setListenerComment(new ListenerComment());
        messageBoardListAdapter.setListenerEditarMensaje(new ListenerEditarMensaje());
        messageBoardListAdapter.setListenerLongClickDelete(new ListenerLongClickDelete());

        rcvMessageBoard.setAdapter(messageBoardListAdapter);
    }


    // De View
    @Override
    public void onMessageDeleted() {
        presenter.getMessages(); // Recargo los mensajes
    }

    @Override
    public void onUserProfile(Profile profile) {
        listener.onUserProfile(profile);
    }


    // TODO. Esto irá a base
    private ProgressDialog progressDialog;

    public void hideProgressBar() {

        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar() {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getString((R.string.msg_loading)));
        progressDialog.setTitle(R.string.app_name);
        progressDialog.show();

    }



    private class ListenerDetallesActivity implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), "Yendo al detalle del mensaje.. ", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(getContext(), MessageBoardDetailFragment.class);
//
//            intent.putExtra("MessageBoard", messageBoardListAdapter.getItem(rcvMessageBoard.getChildAdapterPosition(v)));
//            startActivity(intent);

            // TODO
            //   messageBoardListAdapter.getItem(rcvMessageBoard.getChildAdapterPosition(v))
            listener.onDetail(messageBoardListAdapter.getItem(rcvMessageBoard.getChildAdapterPosition(v))); // TODO ->   (MessageBoard) v.getTag());
        }
    }

    private class ListenerComment implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), "Yendo los comentarios del mensaje.. ", Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(getContext(), MessageBoardDetailFragment.class);
//
//            intent.putExtra("MessageBoard", (MessageBoard)v.getTag());
//            startActivity(intent);




            listener.onDetail((MessageBoard) v.getTag());


        }
    }

    private class ListenerPerfilUser implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            Toast.makeText(getContext(), "Yendo al perfil del usuario.. ", Toast.LENGTH_SHORT).show();


            // TODO. CON EL NICK PUEDO SACAR EL PERFIL DEL USER

            presenter.getUserProfile(v.getTag().toString()); // TODO. Obtendré el Profile a partir del nick (CONSULTA BBDD)


        }
    }

    private class ListenerEditarMensaje implements View.OnClickListener{
        @Override
        public void onClick(View v) {
//            Intent intent = new Intent(getContext(), MessageBoardAddFragment.class);
//            intent.putExtra("MessageBoard", (MessageBoard) v.getTag());
//            // TODO. intent.putExtra();
//
//            startActivity(intent);


            listener.onEdit((MessageBoard) v.getTag());
        }
    }


    private class ListenerLongClickDelete implements View.OnLongClickListener{

        @Override
        public boolean onLongClick(View v) {

            // TODO. Solo se puede borrar si eres el usuario que publicó el messageBoard
            final MessageBoard msgABorrar = (MessageBoard)messageBoardListAdapter.getItem(rcvMessageBoard.getChildAdapterPosition(v));
            if (CommonUtils.messageBoardIsFromThisUser(getContext(), msgABorrar ) ){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                alertDialog.setMessage(R.string.dialog_delete_msgboard);
                alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                                  Toast.makeText(MessageBoardActivity.this, "Borrando", Toast.LENGTH_SHORT).show();

                        presenter.deleteMessage(msgABorrar);

                    }
                });
                alertDialog.setNegativeButton(R.string.no, null);
                alertDialog.show();


            }
            else{
                Toast.makeText(getContext(), "No puede borrarse un Mensaje del Tablón si no eres el creador del mismo", Toast.LENGTH_SHORT).show();
            }


            return true; // Cancelo el click normal
        }
    }

    @Override
    public void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }


}
