package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.io.Serializable;
/**
 * Clase modelo que define la entidad FriendRequests
 * @author Marina Espinosa
 */
public class FriendRequests implements Comparable<FriendRequests>, Serializable {
    private String friendrequests_id;
    private String userWhoSendsRequest_nick;
    private String userWhoReceivedRequest_nick;
    private boolean isAccepted;
    private String picture;

    public FriendRequests(String friendrequests_id, String userWhoSendsRequest_nick, String userWhoReceivedRequest_nick, String picture) {
        this.friendrequests_id = friendrequests_id;
        this.userWhoSendsRequest_nick = userWhoSendsRequest_nick;
        this.userWhoReceivedRequest_nick = userWhoReceivedRequest_nick;
        this.picture = picture;
    }

    public String getFriendrequests_id() {
        return friendrequests_id;
    }

    public void setFriendrequests_id(String friendrequests_id) {
        this.friendrequests_id = friendrequests_id;
    }

    public String getUserWhoSendsRequest_nick() {
        return userWhoSendsRequest_nick;
    }

    public void setUserWhoSendsRequest_nick(String userWhoSendsRequest_nick) {
        this.userWhoSendsRequest_nick = userWhoSendsRequest_nick;
    }

    public String getUserWhoReceivedRequest_nick() {
        return userWhoReceivedRequest_nick;
    }

    public void setUserWhoReceivedRequest_nick(String userWhoReceivedRequest_nick) {
        this.userWhoReceivedRequest_nick = userWhoReceivedRequest_nick;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }



    @Override
    public int compareTo(@NonNull FriendRequests o) {
        return this.friendrequests_id.compareTo(o.friendrequests_id);
    }

    @Override
    public boolean equals(Object obj) {
        return this.friendrequests_id.equals(((FriendRequests)obj).getFriendrequests_id());
    }
}
