package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;
/**
 * Clase modelo que define la entidad Genres
 * @author Marina Espinosa
 */
public class Genres implements Comparable<Genres>{
    private int id;
    private String genre; // varchar(30)

    public Genres(int id, String genre) {
        this.id = id;
        this.genre = genre;
    }

    public Genres(String genre) {
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    // Comparando por id
    @Override
    public int compareTo(@NonNull Genres o) {
        return this.getId() - o.getId();
    }

    // Comparando por género
    public static class OrderByGenre implements Comparator<Genres>{
        @Override
        public int compare(Genres o1, Genres o2) {
            return o1.getGenre().compareTo(o2.getGenre());
        }
    }
}
