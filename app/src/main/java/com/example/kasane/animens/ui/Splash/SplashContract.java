package com.example.kasane.animens.ui.Splash;

import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Genre;
import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;

public interface SplashContract {
    interface View{
        void onDownloadedAnimeData(ArrayList<Anime> animesDownloaded);
        void onDownloadedAnime_genresData(ArrayList<Anime_genres> anime_genres);
        void onGenresDownloaded(ArrayList<Genre> genres);

        void onError(String errorMessage);
        void itWasAlreadyDownloaded();

        void onInsertedAnimes();
        void onInsertedAnime_genres();
    }

    interface Presenter{
//        void downloadData();

        // TODO
        void downloadAnimeData();
        void downloadAnime_genresData();
        void downloadGenres();

        void insertAnimes(ArrayList<Anime> animes);
        void insertAnime_genres(ArrayList<Anime_genres> anime_genres);
        void insertGenres(ArrayList<Genre> genres);
    }
}
