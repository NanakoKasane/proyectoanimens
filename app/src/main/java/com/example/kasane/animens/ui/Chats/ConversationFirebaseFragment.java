package com.example.kasane.animens.ui.Chats;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.firebase.model.Messages;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.ReportUser.ReportUserActivity;
import com.example.kasane.animens.ui.adapter.ConversationFirebaseAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Fragment que gestiona el listado de los Mensajes de Firebase con otro usuario
 * @author Marina Espinosa
 */
public class ConversationFirebaseFragment extends BaseFragment implements ConversationFirebaseContract.View {
    // vistas
    private ImageButton mSendButton;
//    private ListView messageListView;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private EditText mMessageEditText;

    // firebase
    public static final String MESSAGES_CHILD = "messages"; // nombre en el JSON de firebase del hijo. El objeto "messages", tiene los mensajes
    private DatabaseReference mFirebaseDatabaseReference;
    FirebaseRecyclerAdapter mFirebaseAdapter;

    // Objeto Conversation de la API con las imagenes de ambos user, ambos nick y el chatId
    Chatroom chatroom;
    ConversationFirebaseContract.Presenter presenter;


    public static ConversationFirebaseFragment newInstance(Bundle bundle){
        ConversationFirebaseFragment conversationFirebaseFragment = new ConversationFirebaseFragment();
        if (bundle != null){
            conversationFirebaseFragment.setArguments(bundle);
        }
        return conversationFirebaseFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation_firebase, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null){
            chatroom = (Chatroom) getArguments().getSerializable("chatroom");
            presenter = new ConversationFirebasePresenter(this);

            // inicializo (findViewById y click de botones)
            mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference(); // Referencia a la base de datos
            inicializar();


            // cambio isReaded a true si senderLastMessage != null y es el otro usuario (no el actual)
            String otherUser;
            if (chatroom.getNick1().equals(CommonUtils.getCurrentUserNick(AnimensApplication.getContext())))
                otherUser = chatroom.getNick2();
            else
                otherUser = chatroom.getNick1();

            if (chatroom.getSenderLastMessage() != null && chatroom.getSenderLastMessage().equals(otherUser)){
                Log.d("FirebaseChat", "Mensaje leído " + chatroom.getChat_id() + " - " + otherUser);

                presenter.setLastMessageReaded(String.valueOf(chatroom.getChat_id()));
            }
            //


            // TODO. Lógica de firebase
            SnapshotParser<Messages> parser = new SnapshotParser<Messages>() {
                @Override
                public Messages parseSnapshot(DataSnapshot dataSnapshot) {
                    Messages friendlyMessage = dataSnapshot.getValue(Messages.class); // Clase modelo para parseo.
                    if (friendlyMessage != null) {
                        friendlyMessage.setId(dataSnapshot.getKey());
                    }
                    return friendlyMessage;
                }
            };

            DatabaseReference messagesRef = mFirebaseDatabaseReference.child(MESSAGES_CHILD); // accedo a "messages"
            FirebaseRecyclerOptions<Messages> options =
                    new FirebaseRecyclerOptions.Builder<Messages>()
                            .setQuery(messagesRef, parser)
                            .build();

            mFirebaseAdapter = new ConversationFirebaseAdapter(chatroom, mProgressBar, options);

            mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                    int lastVisiblePosition =
                            mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (lastVisiblePosition == -1 ||
                            (positionStart >= (friendlyMessageCount - 1) &&
                                    lastVisiblePosition == (positionStart - 1))) {
                        mMessageRecyclerView.scrollToPosition(positionStart);
                    }
                }
            });

            mMessageRecyclerView.setAdapter(mFirebaseAdapter);
        }



    }


    // TODO. FindViewById y listeners
    private void inicializar(){
        if (getView() != null) {
            mProgressBar = getView().findViewById(R.id.progressBar);
            mMessageEditText = getView().findViewById(R.id.messageEditText);
            mSendButton = getView().findViewById(R.id.sendButton);
            mSendButton.setEnabled(false);

            mMessageRecyclerView = getView().findViewById(R.id.messageRecyclerView);
            mLinearLayoutManager = new LinearLayoutManager(getContext());
            mLinearLayoutManager.setStackFromEnd(true);
            mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);

            mMessageEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().trim().length() > 0) {
                        mSendButton.setEnabled(true);
                    } else {
                        mSendButton.setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            // Enviar mensaje
            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // actualizo en la base de datos mysql el último mensaje
                    presenter.updateLastChatMessage(mMessageEditText.getText().toString(),
                            CommonUtils.getFormattedCurrentTime(),
                            CommonUtils.getFormattedCurrentDate(),
                            CommonUtils.getCurrentUserNick(AnimensApplication.getContext()),
                            String.valueOf(chatroom.getChat_id()));
                    //

                    // obtengo el nick que soy yo (y será senderNick), y el otro es receiverNick
                    String usuarioActual = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());
                    String receiverNick;
                    if (usuarioActual.equals(chatroom.getNick1()))
                        receiverNick = chatroom.getNick2();
                    else
                        receiverNick = chatroom.getNick1();

                    // subo el mensaje a la base de datos de firebase
                    Messages friendlyMessage = new
                            Messages(mMessageEditText.getText().toString(),
                            usuarioActual,
                            receiverNick,
                            CommonUtils.getFormattedCurrentTime(),
                            String.valueOf(chatroom.getChat_id()),// chatId
                            CommonUtils.getFormattedCurrentDate()
                    );
                    mFirebaseDatabaseReference.child(MESSAGES_CHILD)
                            .push().setValue(friendlyMessage);  // AQUI se escribe en Firebase
                    mMessageEditText.setText("");

                }
            });
        }

    }


    // TODO. Cada 5 segundos mientras estés aquí, isReaded a true
    private void reloadIsReaded(){
        Handler handler = new Handler();
        Runnable runnableCode = new Runnable() {

            @Override
            public void run() {

                handler.postDelayed(this, 5000);
            }
        };
        handler.post(runnableCode);

//        handler.removeCallbacks(this);

    }


    @Override
    public void onPause() {
        mFirebaseAdapter.stopListening();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mProgressBar.setVisibility(View.VISIBLE);
        mFirebaseAdapter.startListening();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.report_user, menu);
    }

    // Ordenaciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reportuser){
            Intent intent = new Intent(getContext(), ReportUserActivity.class);
            String nickOtherUser = null;
            if (chatroom != null){
                if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(chatroom.getNick1()))
                    nickOtherUser = chatroom.getNick2();
                else
                    nickOtherUser = chatroom.getNick1();

                intent.putExtra("nick", nickOtherUser);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onUpdatedReaded() {
        Log.d("FirebaseChat", "updated readed");
    }

    @Override
    public void onUpdatedLastChatMessage() {
        Log.d("FirebaseChat", "updated last chat message");
    }

    @Override
    public void onServerError() {
        Log.d("FirebaseChat", "Server error");
    }
}


