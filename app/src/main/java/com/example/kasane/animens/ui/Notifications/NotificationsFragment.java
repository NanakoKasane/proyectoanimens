package com.example.kasane.animens.ui.Notifications;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.repository.FriendRequestsRepository;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.adapter.NotificationsAdapter;

import java.util.ArrayList;

/**
 * Fragment que gestiona el listado de notificaciones
 * @author Marina Espinosa
 */
public class NotificationsFragment extends BaseFragment implements NotificationsContract.View {
    RecyclerView rcvNotificaciones;
    NotificationsAdapter notificationsAdapter;
    NotificationsPresenter presenter;
    GoProfile goProfile;

    interface GoProfile{
        void goToUserProfile(Profile profile);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            goProfile = (GoProfile)context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements GoProfile");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rcvNotificaciones = view.findViewById(R.id.rcvNotificaciones);
        rcvNotificaciones.setLayoutManager(new LinearLayoutManager(getContext()));
        notificationsAdapter = new NotificationsAdapter(getContext());
        rcvNotificaciones.setAdapter(notificationsAdapter);

        presenter = new NotificationsPresenter(this);

        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
        }
        else
            presenter.getNotifications(getContext());



    }

    @Override
    public void showNotifications(ArrayList<FriendRequests> friendRequests) {
//        Toast.makeText(getContext(), "Cargada lista", Toast.LENGTH_SHORT).show();

        notificationsAdapter = new NotificationsAdapter(getContext());
        notificationsAdapter.setFriendRequests(friendRequests);
        notificationsAdapter.setClickAccept(new ClickAccept());
        notificationsAdapter.setClickCancel(new ClickCancel());
        notificationsAdapter.setClickGoToUser(new ClickGoToUser());

        rcvNotificaciones.setAdapter(notificationsAdapter);
    }

    @Override
    public void onProfileObtained(Profile profile) {
//        Intent intent = new Intent(getContext(), ProfileActivity.class);
//        intent.putExtra("Profile", profile);
//        startActivity(intent);

        goProfile.goToUserProfile(profile);
    }

    @Override
    public void onFriendRequestCancelled() {
        Toast.makeText(AnimensApplication.getContext(), "Petición de amistad cancelada", Toast.LENGTH_SHORT).show();
        presenter.getNotifications(getContext());
    }

    @Override
    public void onFriendRequestAccepted() {
        Toast.makeText(AnimensApplication.getContext(), "Petición de amistad aceptada", Toast.LENGTH_SHORT).show();

        presenter = new NotificationsPresenter(this);
        presenter.getNotifications(getContext());
    }

    @Override
    public void onNoNotifications() {
        rcvNotificaciones.setAdapter(null);
//        Toast.makeText(getContext(), "No tiene notificaciones", Toast.LENGTH_SHORT).show();

        DialogInterface.OnClickListener listenerOk =  new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
//                getFragmentManager().popBackStack();

                // TODO. voy a users ?
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        };

        showInformationDialog(getString(R.string.msg_notification_title), getString(R.string.msg_notification), listenerOk);

    }

    @Override
    public void onServerError() {
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_server);
    }


    // TODO. Rellenar OnClicks
    private class ClickGoToUser implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            String userWhoSentRequest = (String) v.getTag();
            presenter.getUserProfile(userWhoSentRequest);
        }
    }

    private class ClickCancel implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            presenter.cancelFriendRequest((String) v.getTag()); //TODO.  Le paso el id de la petición a cancelar
        }
    }

    private class ClickAccept implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            presenter.acceptFriendRequest((String) v.getTag()); // Le paso el id de la petición a aceptar
        }
    }


}
