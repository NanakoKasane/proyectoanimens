package com.example.kasane.animens.ui.User;

import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;

import java.util.ArrayList;

public class UserListPresenter implements UserListContract.Presenter, UserListInteractor.GetUserList, UserListInteractor.GetUserProfile {
    UserListContract.View view;
    UserListInteractor interactor;

    public UserListPresenter(UserListContract.View view){
        this.view = view;
        interactor = new UserListInteractor();
    }

    // Presenter
    @Override
    public void getUserList() {
        interactor.getUserList(null, this);
    }

    @Override
    public void getFriendList(String nick) {
        interactor.getFriendList(nick, this);
    }

    @Override
    public void getUserProfile(String nick) {
        interactor.getUserProfile(nick, this);
    }

    // Interactor
    @Override
    public void onUserListObtained(String nick, String email, ArrayList<User> users) {
        view.showUserList(users);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onNoFriends() {
        view.onNoFriends();
    }

    @Override
    public void onUserProfile(Profile profile) {
        view.onUserProfile(profile);
    }
}
