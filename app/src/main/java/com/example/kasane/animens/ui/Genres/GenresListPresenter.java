package com.example.kasane.animens.ui.Genres;

import com.example.kasane.animens.data.db.model.Anime_genresView;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Anime_genres;

import java.util.ArrayList;

public class GenresListPresenter implements GenresListContract.Presenter, GenresListInteractor.GetGenresList, GenresListInteractor.GetAnimeList{
    private GenresListContract.View view;
    GenresListInteractor interactor;

    public GenresListPresenter(GenresListContract.View view){
        this.view = view;
        interactor = new GenresListInteractor();
    }

    // Presenter
    @Override
    public void getGenreList() {
        interactor.getGenresList(this);
    }

    @Override
    public void getAnimesFromGenre(String genre) {
        interactor.getAnimesFromGenre(genre, this);
    }

    // Interactor
    @Override
    public void onGenresObtained(ArrayList<Anime_genresView> genres) {
        view.showGenreList(genres);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onAnimesFromGenreObtained(ArrayList<Anime> animes, String genre) {
        view.onAnimesFromGenreObtained(animes, genre);
    }
}
