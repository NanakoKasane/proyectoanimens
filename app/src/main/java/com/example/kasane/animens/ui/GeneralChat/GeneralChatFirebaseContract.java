package com.example.kasane.animens.ui.GeneralChat;

import com.example.kasane.animens.data.network.model.Profile;

public interface GeneralChatFirebaseContract {
    interface View{
        void onProfileObtained(Profile profile);
        void onServerError();
    }

    interface Presenter{
        void getCurrentUserProfile();
    }
}
