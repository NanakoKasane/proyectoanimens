package com.example.kasane.animens.data.network.jobservices;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.example.kasane.animens.ui.Notifications.NotificationsActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * JobService que comprueba si te han enviado peticiones de amistad.
 * Este JobService se inicializa en AnimensApplication y se programa para que se ejecute cada X tiempo
 * Aunque la aplicación haya finalizado se siguen ejecutando
 * @author Marina Espinosa
 */
public class CheckFriendRequestsReceivedJobService extends JobService {

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public boolean onStartJob(@NonNull JobParameters job) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AnimensApplication.getContext());
        boolean notificar = pref.getBoolean("notifications_requestreceived", true);

        if (!notificar){ // las notificaciones de las prefs estan OFF, no hay que notificar
            AnimensApplication.scheduleCheckFriendRequestsReceivedJobService(AnimensApplication.getContext());
            return true;
        }
        else {
            String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

            String url = AnimensApplication.BASE_URL+"animens/friendRequestsList.php?userWhoReceivedRequest_nick=" + nick;
            RestClient.get(url, new JsonHttpResponseHandler() {

                // TODO. On success -> obtengo la lista de peticiones y me interesa cuántas hay
                // Mediante Broadcast notifico -> Le paso en el bundle serializable las notificaciones
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Log.d("AnimensPHP", "success " + response.toString());
                        Gson gson = new Gson();
                        ArrayList<FriendRequests> friendRequests = gson.fromJson(response.getJSONArray("friendrequests").toString(), new TypeToken<ArrayList<FriendRequests>>() {
                        }.getType());

//                    if (friendRequests.size() > 0 && friendRequests.size() > CommonUtils.getCurrentFriendRequestsCount()){
                        CommonUtils.saveCountOfFriendRequests(friendRequests.size());


                        Intent intentNotificaciones = new Intent(AnimensApplication.getContext(), NotificationsActivity.class);
                        PendingIntent pendingIntent = PendingIntent.getActivity(AnimensApplication.getContext(), 0, intentNotificaciones, PendingIntent.FLAG_UPDATE_CURRENT);
                        int numRecibidas = friendRequests.size();
                        if (numRecibidas > 1)
                            createNotification("Te han mandado " + numRecibidas + " peticiones de amistad", pendingIntent, "Peticiones de amistad");
                        else
                            createNotification("Te han mandado una petición de amistad", pendingIntent, "Peticiones de amistad");
//                    }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
//                    listener.onServerError();
                }
            });

            AnimensApplication.scheduleCheckFriendRequestsReceivedJobService(AnimensApplication.getContext());
            return true;
        }
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        return true;
    }


    private void createNotification(String text, PendingIntent pendingIntent, String title){
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(AnimensApplication.getContext(), AnimensApplication.notificationChannel);
        notificationBuilder.setSmallIcon(R.mipmap.animenslogo);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(text);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null)
            notificationManager.notify(3, notificationBuilder.build());

//        startForeground(0, notificationBuilder.build());
    }


}
