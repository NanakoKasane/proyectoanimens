package com.example.kasane.animens.data.db.DAO;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Anime_genresView;

import java.util.ArrayList;

/**
 * DAO de Anime_genres (para sqlite)
 * @author Marina Espinosa
 */
public class Anime_genres_DAO {

    // Obtengo el número de Animes que hay insertados
    public int getAnime_GenresCount(){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
        long count = DatabaseUtils.queryNumEntries(sqLiteDatabase, AnimensContract.Anime_Genres_Entry.TABLE_NAME);

        AnimensOpenHelper.getInstance().closeDatabase();
        return (int) count;
    }

    /**
     * Insert con transacciones
     */
    public boolean insertAnime_genres(ArrayList<Anime_genres> anime_genres){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        sqLiteDatabase.beginTransaction();

        String sql = "INSERT INTO " + AnimensContract.Anime_Genres_Entry.TABLE_NAME + " VALUES(?, ?)";
        SQLiteStatement stmt = sqLiteDatabase.compileStatement(sql);
        for (int i = 0; i  < anime_genres.size(); i++) {
            stmt.bindString(1, anime_genres.get(i).getAnime_id());
            stmt.bindString(2, anime_genres.get(i).getGenre());

            stmt.execute();
            stmt.clearBindings();
        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

        AnimensOpenHelper.getInstance().closeDatabase();
        return true;
    }


    // Obtengo los géneros y su count de animes que tiene cada género
    public ArrayList<Anime_genresView> getGenresAndCount(){
        ArrayList<Anime_genresView> anime_genresViews = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        // TODO. select genre, count(*) from anime_genres group by genre;
        Cursor cursor = sqLiteDatabase.query(AnimensContract.Anime_Genres_Entry.TABLE_NAME, new String[]{AnimensContract.Anime_Genres_Entry.COLUMN_GENRE, "count(*)"},
                null, null, AnimensContract.Anime_Genres_Entry.COLUMN_GENRE, null, null );

        if (cursor.moveToFirst()){
            do{
                Anime_genresView anime_genresView = new Anime_genresView(cursor.getString(0), cursor.getInt(1));
                anime_genresViews.add(anime_genresView);
            }while (cursor.moveToNext());
        }

        AnimensOpenHelper.getInstance().closeDatabase();
        return anime_genresViews;
    }


}
