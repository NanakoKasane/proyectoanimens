package com.example.kasane.animens.ui.MessageBoard;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.repository.AnimeListRepository;
import com.example.kasane.animens.data.network.repository.MessageBoardListRepository;
import com.example.kasane.animens.data.network.repository.ProfileRepository;

import java.util.ArrayList;

public class MessageBoardListInteractor {

    public void getMessages(final MessageBoardListInteractor.GetMessageBoards listener) {

        /**
         * Me bajo las imágenes
         */
        new AsyncTask<Void, Void,  ArrayList<Bitmap>>()
        {
            @Override
            protected ArrayList<Bitmap> doInBackground(Void... voids) {
                return MessageBoardListRepository.getMessageBoardListRepository().getImagesBitMap();

            }

            @Override
            protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
                listener.onMessageBoardsObtained(MessageBoardListRepository.getMessageBoardListRepository().getMessageBoards(), bitmaps);
            }
        }.execute();

    }

    public void deleteMessage(MessageBoard messageBoard, MessageBoardListInteractor.DeleteMessageBoard listener) {
        ArrayList<MessageBoard> messageBoards = MessageBoardListRepository.getMessageBoardListRepository().getMessageBoards();
        messageBoards.remove(messageBoard);
        listener.onDeleted();
    }

    public void getUserProfile(String nick, MessageBoardListInteractor.GetUserProfile listener) {
        // TODO. Consulta que saque el perfil del usuario con el nick (nick)

        listener.onUserProfile(ProfileRepository.getProfileRepository().getProfile());
    }

    interface GetMessageBoards{
        void onMessageBoardsObtained(ArrayList<MessageBoard> messageBoards, ArrayList<Bitmap> bitmaps);
    }

    interface DeleteMessageBoard{
        void onDeleted();
    }

    interface GetUserProfile{
        void onUserProfile(Profile profile);
    }
}
