package com.example.kasane.animens.ui.Login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

/**
 * Activity que se encarga del Login con Google y Twitter (de Firebase)
 * @author Marina Espinosa
 */
public class LoginsFirebaseActivity extends LoginActivity implements LoginsFirebaseContract.View{
    FirebaseAuth mAuth;
    private final static int RC_SIGN_IN = 0;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;

    FirebaseAuth.AuthStateListener mAuthListener;
    LoginsFirebasePresenter presenter;
    FirebaseUser user;

    public static final String GOOGLE_OPTION = "GOOGLE";
    public static final String TWITTER_OPTION = "TWIITER";
    public static final String OPTION = "option";
    String opcion;
    String email;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_firebase);
        presenter = new LoginsFirebasePresenter(LoginsFirebaseActivity.this);

        opcion = getIntent().getStringExtra(OPTION);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();


        // TODO -> twitter.
        if (opcion.equals(TWITTER_OPTION)){ // Todo. Maybe el login se hace con botones especiales desde LoginActivity y ya cuando se ha accedido redirige a aquí solo para lo de getCredentials, etc etc
//            LoginsFirebaseActivity.this.mAuth = LoginActivity.mAuth;
//            LoginsFirebaseActivity.this.user = LoginActivity.user; // mAuth.getCurrentUser(); // TODO. Esto fallará ?

            showProgressBar(null, getString(R.string.msg_logingin));
            email = getIntent().getStringExtra("email");
            presenter.getIfUserIsRegistered(email);
        }
        // TODO -> google
        else if (opcion.equals(GOOGLE_OPTION)){
            user = mAuth.getCurrentUser();

            // TODO. Listener que te dice si ha cerrado o iniciado la sesión
            mAuthListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                if (firebaseAuth.getCurrentUser() != null){
//                    Intent intent = new Intent(LoginsFirebaseActivity.this, UserListActivity.class); // TODO. FirebasePruebas.class
////                    intent.putExtra("PrimeraVez", true);
//                    startActivity(intent);
//
//                }
                }
            };
            mAuth.addAuthStateListener(mAuthListener); // Aqui falla con twitter


            // Opciones para el Sign in
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id)) // "600365944008-geau5l10bkomdi1528eg6mcum8m0u70h.apps.googleusercontent.com") //R.string.default_web_client_id)) //"600365944008-geau5l10bkomdi1528eg6mcum8m0u70h.apps.googleusercontent.com") // AIzaSyARk4LlBpgbxVaciRB3hxfp0wPAWyh_WUk
                    .requestEmail()
                    .build();


            //   mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                            Toast.makeText(AnimensApplication.getContext(), R.string.msg_conectionfailed, Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();


            signIn();
        }



    }

    private void login(){
//        Toast.makeText(LoginsFirebaseActivity.this, "Inicio de sesión correcto", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(LoginsFirebaseActivity.this, UserListActivity.class); // TODO. FirebasePruebas.class
//                    intent.putExtra("PrimeraVez", true);
        startActivity(intent);

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        showProgressBar(null, getString(R.string.msg_logingin));
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        hideProgressBar();

        if (requestCode == RC_SIGN_IN) {
            showProgressBar(null,getString(R.string.msg_logingin) );

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if (account != null)
                    firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Log.d("Marina", "Fallo al iniciar sesión con Google", e);
                CommonUtils.showMessage(this, R.string.err_loging_google);
                hideProgressBar();
                finish();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Voy al interactor y hago consulta a la BD para obtener los datos de este USER a partir de su email
                            showProgressBar(null, getString(R.string.msg_validating));
                            user = mAuth.getCurrentUser();
                            if (user != null) {
                                email = user.getEmail();

                                presenter.getIfUserIsRegistered(user.getEmail());
//                            presenter.getCurrentUserData(LoginsFirebaseActivity.this, user.getEmail());
//                            user.getPhotoUrl(); // TODO. puedo coger esa foto de perfil
                            }

                            hideProgressBar();

                        } else {
                            if(task.getException() instanceof FirebaseAuthUserCollisionException) { //  FirebaseAuthUserCollisionException. An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address
                                CommonUtils.showMessage(LoginsFirebaseActivity.this,getString(R.string.err_logincollision) );
                            }
                            else
                                CommonUtils.showMessage(LoginsFirebaseActivity.this, getString(R.string.err_general_google ));
                            hideProgressBar();
                            finish();
                        }

                    }
                });
    }



    private ProgressDialog progressDialog;

    public void hideProgressBar() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar(String titulo, String mensaje) {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(LoginsFirebaseActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(mensaje); // R.string.msg_loading
        if (titulo != null)
            progressDialog.setTitle(titulo); // R.string.app_name
        progressDialog.show();
    }


    /**
     * Callbacks para registrar/obtener los datos si está registrado...
     */

    @Override
    public void onRegistered() {
        presenter.getCurrentUserData(LoginsFirebaseActivity.this, email);


    }

    // Obtenido el nick y guardado en las preferencias
    @Override
    public void onDataObtained() {
        hideProgressBar();
        login();
    }

    @Override
    public void onNoRegistered() {
        showEdDialog();
    }

    public void showEdDialog() {
        hideProgressBar();

        try {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

            dialogBuilder.setTitle("Nick");
            dialogBuilder.setMessage("Introduzca un nick (único)");
            dialogBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (edt.getText().toString().isEmpty()) {
                        CommonUtils.showMessage(LoginsFirebaseActivity.this, R.string.err_nick_unique);
                        showEdDialog();
                    } else if (!edt.getText().toString().matches("^[a-z0-9]*$")) {
                        CommonUtils.showMessage(LoginsFirebaseActivity.this, R.string.err_invalidNick);
                        showEdDialog();
                    } else if (!edt.getText().toString().isEmpty() && edt.getText().toString().matches("^[a-z0-9]*$")) {
                        showProgressBar(null, getString(R.string.msg_checkingnick));
                        presenter.getIfNickExists(edt.getText().toString().toLowerCase());
                    }
//                else
//                    CommonUtils.showMessage(LoginsFirebaseActivity.this, R.string.err_nickunique);
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
        catch (Exception e){}
    }


    @Override
    public void onNickExists(String nick) {
        CommonUtils.showMessage(this, R.string.err_nick_unique);
        showEdDialog();
    }

    /**
     * Si el nick no existe ya, registramos al usuario
     */
    @Override
    public void onNickDoesntExists(String nick) {
        presenter.registerUser(email, nick);

        // TODO. Esto al registrar
        Uri uri  = user.getPhotoUrl(); // TODO. puedo coger esa foto de perfil.
        //TODO. luego -> imageView.setImageURI(uri);       Si la pass es null es que es de firebase..
    }

    @Override
    public void onSuccessRegister() {
        presenter.getCurrentUserData(LoginsFirebaseActivity.this, email);
    }

    @Override
    public void onServerError() {
        hideProgressBar();
        CommonUtils.showMessage(this, R.string.err_server);
    }
}
