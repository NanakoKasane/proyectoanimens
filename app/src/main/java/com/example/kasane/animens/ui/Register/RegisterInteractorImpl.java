package com.example.kasane.animens.ui.Register;

import android.text.TextUtils;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.Utils.CountRegisterValidation;
import com.example.kasane.animens.data.firebase.model.Messages;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que valida los datos y realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class RegisterInteractorImpl {
    private RegisterInteractor listener; // Presenter, que implementa esta interfaz

    public RegisterInteractorImpl(RegisterInteractor listener){
        this.listener = listener;
    }

    public void validation(String nick, String email, String phone, String password, String passwordConfirm){
        boolean error = false;

        // Validar contraseñas
        if (!passwordConfirm.equals(password)){
            listener.onPasswordConfirmError();
            error = true;
        }

        if (!CommonUtils.validatePassword(password)) {
            listener.onPasswordError();
            error = true;
        }

        // Validar email o phone (solo uno debe ser válido)
//        if (!CommonUtils.validateEmail(email) && !CommonUtils.validatePhone(phone)){
//            listener.onInvalidEmailOrPhoneError();
//            error = true;
//        }

        if (TextUtils.isEmpty(email)){
            listener.onInvalidEmailOrPhoneError();
            error =true;
        }

        if (!CommonUtils.validateEmail(email)){
            listener.onInvalidEmailOrPhoneError();
            error = true;
        }


        // Validar nick:
        if (TextUtils.isEmpty(nick)) {
            listener.onNickEmptyError();
            error = true;
        }

        if (!nick.matches("^[a-z0-9]*$")){ // Debe ser alfanumérico
            listener.onNickError();
            error = true;
        }


        // TODO. De la base de datos -> Que el NICK, TELEFONO y EMAIL y  no existan ya (hay que comprobarlo en la BD)
        if (!error){
            RequestParams params = new RequestParams();        // params.put("nombre", "valor");
            try {
                params.put("nick", nick);
                params.put("email", email);
//                params.put("phonenumber", phone);

                String url = AnimensApplication.BASE_URL+ "animens/validateRegister.php?"; // nick=" + nick + "&email=" + email + "&phonenumber=" + phone;
                RestClient.post(url, params, new JsonHttpResponseHandler() { // "http://89.39.159.65/marina/animens/validateLogin.php
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            Gson gson = new Gson();
                            ArrayList<CountRegisterValidation> counts = gson.fromJson(response.getJSONArray("counts").toString(), new TypeToken<ArrayList<CountRegisterValidation>>(){}.getType());

                            int countNick = Integer.parseInt(counts.get(0).getCountNick());
                            int countEmail = Integer.parseInt(counts.get(0).getCountEmail());
//                            int countPhone = Integer.parseInt(counts.get(0).getCountPhone());

                            if (countNick == 1)
                                listener.onNickExists();
//                            if (countPhone == 1)
//                                listener.onPhoneExists();
                            if (countEmail == 1)
                                listener.onEmailExists();

                            if (countEmail == 0 & countNick == 0 ) // && countPhone == 0)
                                listener.onSuccess();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("AnimensPHP", e.getMessage());
                            Log.d("AnimensPHP", response.toString());
                            listener.onError();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                        listener.onError();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                        listener.onError();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d("AnimensPHP", "OnFailure" + errorResponse + "\n" + throwable.getMessage());
                        listener.onError();
                    }
                });
            }
            catch (Exception e) {
                Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
                listener.onError();
            }

        }




//        // todo ha ido bien
//        if (!error)
//            listener.onSuccess();


        // TODO. Ahora se haría un insert del usuario en la base de datos con los datos válidos (el teléfono o email solo el que sea válido de los 2 o los 2 si son válidos)
        // TODO. A parte
    }


    public void insertUser(String nick, String email, String phone, String password, final RegisterInteractorImpl.InsertUser insertUser) {
        try {
            RequestParams params = new RequestParams();
            params.put("nick", nick);
            params.put("email", email);
//            params.put("phonenumber", phone);
            params.put("password", CommonUtils.sha256(password));

            String url = AnimensApplication.BASE_URL+"animens/registerUser.php";
            RestClient.post(url, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    insertUser.onErrorInserting();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseBytes, throwable);
                    insertUser.onErrorInserting();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("AnimensPHP", responseString);

//                    createConversationsFirebase(nick);
//                    insertUser.getUserList(nick, email);
//                    insertUser.onInserted(email); // TODO

                    String imageRandom = responseString;
                    createChatRooms(nick, email, imageRandom, insertUser);

                }

            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            insertUser.onErrorInserting();
        }


    }



    private void createChatRooms(String nickNewUser, String email, String imageRandom, final RegisterInteractorImpl.InsertUser insertUser){
        // TODO. llamo al php -> createChatRooms.php
        String url = AnimensApplication.BASE_URL+"animens/createChatRooms.php?nick=" + nickNewUser + "&picture=" + imageRandom;

        try {
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("Chatroom", "Response: "+ responseString);
                    insertUser.onErrorInserting();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseBytes, throwable);
                    insertUser.onErrorInserting();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    insertUser.onInserted(email); // TODO

                }
            });
        }
        catch (Exception e) {
            Log.d("Chatroom", "Excepcion: "+ e.getMessage());
            insertUser.onErrorInserting(); // TODO...
        }

    }

    public void deleteUser(String email) {
        String url = AnimensApplication.BASE_URL+"animens/deleteUser.php";
        RequestParams requestParams = new RequestParams();
        requestParams.put("email", email);

        try {
            RestClient.post(url, requestParams, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("DeleteUser", "OnFailure: "+responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    // TODO. onDeleted()
                }
            });
        }
        catch (Exception e) {
            Log.d("DeleteUser", "Excepcion: "+ e.getMessage());
        }
    }


    public interface InsertUser{
        void onInserted(String email);
        void onErrorInserting();
    }


    public interface RegisterInteractor{
        void onSuccess();
        void onPasswordError();
        void onPasswordConfirmError();
        void onNickEmptyError();
        void onInvalidEmailOrPhoneError();

        void onEmailExists();
        void onNickExists();
        void onPhoneExists();
        void onNickError();
        void onError();
    }
}
