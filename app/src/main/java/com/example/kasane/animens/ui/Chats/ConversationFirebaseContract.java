package com.example.kasane.animens.ui.Chats;

public interface ConversationFirebaseContract {
    interface View{
        void onUpdatedReaded();
        void onUpdatedLastChatMessage();

        void onServerError();
    }

    interface Presenter{
        void setLastMessageReaded(String chat_id);
        void updateLastChatMessage(String lastMessage, String timeLastMessage, String dateLastMessage, String senderLastMessage, String chat_id);
    }
}
