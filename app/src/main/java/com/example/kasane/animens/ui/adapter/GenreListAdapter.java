package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.db.model.Anime_genresView;

import java.util.ArrayList;

/**
 * Adapter para mostrar los géneros
 * @author Marina Espinosa
 */
public class GenreListAdapter extends RecyclerView.Adapter<GenreListAdapter.GenreHolder> {
    private Context context;
    private ArrayList<Anime_genresView> anime_genres;
    private ArrayList<Anime_genresView> anime_genres_copy;
    private View.OnClickListener onClickListener;

    public GenreListAdapter(Context context, ArrayList<Anime_genresView> anime_genres){
        this.context = context;
        this.anime_genres = anime_genres;
        this.anime_genres_copy = new ArrayList<>();
        this.anime_genres_copy.addAll(anime_genres);
    }

    @NonNull
    @Override
    public GenreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.genreslist_item, parent, false);

        view.setOnClickListener(onClickListener);

        GenreHolder genreHolder = new GenreHolder(view);
        return genreHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GenreHolder holder, int position) {
        Anime_genresView anime_genre = anime_genres.get(position);

        holder.tvGenre.setText(anime_genre.getGenre());
        holder.tvAnimeCount.setText("(" + anime_genre.getCount() + ")");
    }

    @Override
    public int getItemCount() {
        return anime_genres.size();
    }

    public class GenreHolder extends RecyclerView.ViewHolder{
        TextView tvGenre;
        TextView tvAnimeCount;

        public GenreHolder(View itemView) {
            super(itemView);
            tvGenre = itemView.findViewById(R.id.tvGenre);
            tvAnimeCount = itemView.findViewById(R.id.tvAnimeCount);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public Anime_genresView getItem(int position){
        return anime_genres.get(position);
    }


    public void setAnime_genres(ArrayList<Anime_genresView> anime_genres) {
        this.anime_genres = anime_genres;
        this.anime_genres_copy = new ArrayList<>();
        this.anime_genres_copy.addAll(anime_genres);
    }

    public void filter(String text) {
        anime_genres.clear();
        if(text.isEmpty()){
            anime_genres.addAll(anime_genres_copy);
        } else{
            text = text.toLowerCase();
            for(Anime_genresView anime_genre : anime_genres_copy){
                if(anime_genre.getGenre().toLowerCase().contains(text) || anime_genre.getGenre().toLowerCase().contains(text)){
                    anime_genres.add(anime_genre);
                }
            }
        }
        notifyDataSetChanged();
    }

}
