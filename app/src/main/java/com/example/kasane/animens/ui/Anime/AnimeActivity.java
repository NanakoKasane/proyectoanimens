package com.example.kasane.animens.ui.Anime;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.Genres.GenresListActivity;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.Pofile.ProfileFragment;

/**
 * Activity que gestiona los Animes. Carga los diferentes fragment
 * @author Marina Espinosa
 */
public class AnimeActivity extends BaseActivity implements AnimeListFragment.OnAnime, AnimeDetailFragment.OnAnimeEdit{

    Fragment animeListFragment;
    Fragment animeDetailFragment;
    Fragment animeAddFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_anime);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.nav_anime);

        Bundle bundle = null;
        String genre = getIntent().getStringExtra("Genre");
        if (genre != null){
            bundle = new Bundle();
            bundle.putString("Genre", genre);
        }
        String nick = getIntent().getStringExtra("Nick");
        if (nick != null){
            bundle = new Bundle();
            bundle.putString("Nick", nick);
        }


//        animeListFragment = getSupportFragmentManager().findFragmentByTag(AnimeListFragment.TAG);
//        if (animeListFragment == null){
            animeListFragment = AnimeListFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mcontent, animeListFragment, AnimeListFragment.TAG);
            fragmentTransaction.commit();
//        }

    }

    @Override
    public void onEdit(Anime anime) {
        // Cargo fragment de Editar/Añadir
        Bundle bundle = new Bundle();
        bundle.putSerializable("Anime", anime);
        animeAddFragment = AnimeAddFragment.newInstance(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mcontent, animeAddFragment); //  , AnimeDetailFragment.TAG
        if (anime == null)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        //  TODO. No guardar en la pila?? (no quiero que se guarde en la pila SOLO si viene de onDetail)


    }

    @Override
    public void onDetail(Anime anime) {
        // Cargo fragment del detalle del Anime
     //   animeDetailFragment = getSupportFragmentManager().findFragmentByTag(AnimeDetailFragment.TAG);
     //   if (animeDetailFragment == null){
            Bundle bundle = new Bundle();
            bundle.putSerializable("Anime", anime);
            animeDetailFragment = AnimeDetailFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.mcontent, animeDetailFragment); //  , AnimeDetailFragment.TAG
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
       // }


    }

    @Override
    public void onProfile(Profile profile) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("Profile", profile);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        String nick = getIntent().getStringExtra("Nick");
        String genre = getIntent().getStringExtra("Genre");

        if (nick != null && getSupportFragmentManager().getBackStackEntryCount() == 0){
//            if (getSupportActionBar() != null && nick != null)
//                getSupportActionBar().setTitle(nick);
            AnimeListFragment.presenter.getUserProfile(nick);
        }
        if (genre != null && getSupportFragmentManager().getBackStackEntryCount() == 0){
            Intent intent = new Intent(this, GenresListActivity.class);
            startActivity(intent);
        }
        else if (nick == null && genre == null && getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                super.onBackPressed();
        }
    }


    /**
     *
     * @return
     */
//    @Override
//    public boolean onSupportNavigateUp() {
//        super.onBackPressed();
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                FragmentManager fm = getSupportFragmentManager();
//                if (fm.getBackStackEntryCount() > 0) {
//                    fm.popBackStack();
//                }
//                return true;
//            default:
//                if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//                    return true;
//                }
//                return true;
//
//
//        }
//    }

}