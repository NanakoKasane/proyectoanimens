package com.example.kasane.animens.data.db.model;

import android.support.annotation.NonNull;

/**
 * Clase modelo para sqlite
 * @author Marina Espinosa
 */
public class Anime_genres implements Comparable<Anime_genres> {
    private String anime_id;
    private String genre; // Nombre del género

    public Anime_genres(String anime_id, String genre) {
        this.anime_id = anime_id;
        this.genre = genre;
    }


    public String getAnime_id() {
        return anime_id;
    }

    public void setAnime_id(String anime_id) {
        this.anime_id = anime_id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    /**
     * Comparación por género (ordenado alfabéticamente)
     */
    @Override
    public int compareTo(@NonNull Anime_genres o) {
        return this.genre.compareTo(o.getGenre());
    }


}