package com.example.kasane.animens.ui.Login;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Profile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class LoginsFirebaseInteractor {
    public void getCurrentUserData(Context context, String email, GetUserData listener) {

        // Consulta a la BD  para obtener el nick del usuario actual, y luego guardarlo en las preferencias.

        try {
            String url = AnimensApplication.BASE_URL+ "animens/getCurrentUserData.php?email=" + email;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Profile> profiles = gson.fromJson(response.getJSONArray("profile").toString(), new TypeToken<ArrayList<Profile>>(){}.getType());

                        // luego lo guardo en las preferencias
                        String user =  profiles.get(0).getNick();
                        SharedPreferences prefs = context.getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("user", user);
                        editor.commit();

                        listener.onDataObtained();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onDataNotObtained();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure getCurrentUserData " + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.d("AnimensPHP", "OnFailure getCurrentUserData " + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    Log.d("AnimensPHP", "OnFailure getCurrentUserData " + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }




    }


    public void getIfUserIsRegistered(String email, RegisterUser listener) {
        // llamo a la api para saber si el usuario está registrado (si el email existe ya en la BD)
        try {

            String url = AnimensApplication.BASE_URL+"animens/checkIfEmailIsRegistered.php?email=" + email;
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure getIfUserIsRegistered " + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("AnimensPHP","getIfUserIsRegistered " + responseString);
                    if (responseString.equals("1")){ // 1 = true
                        listener.onRegistered();
                    }
                    else // equals("0") //
                        listener.onNoRegistered();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure getIfUserIsRegistered " + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
        }

    }

    public void getIfNickExists(String nick, GetIfNickExists listener) {
        // llamo a la api para saber si el nick existe en la BD
        try {
            String url = AnimensApplication.BASE_URL+"animens/getIfNickExists.php?nick=" + nick;
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure getIfNickExists " + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("AnimensPHP","getIfNickExists "+  responseString);
                    if (responseString.equals("1")){ // 1 = true
                        listener.onNickExists(nick);
                    }
                    else // equals("0") //
                        listener.onNickDoesntExists(nick);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure getIfNickExists " + new String(responseBytes) + "\n" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
        }

    }

    public void registerUser(String email, String nick, RegisterUser listener) {
        // insert en user (lo puedo copiar del register normal?)
        try {
            RequestParams params = new RequestParams();        // params.put("nombre", "valor");
            params.put("nick", nick);
            params.put("email", email);

            String url = AnimensApplication.BASE_URL+"animens/registerUserFirebase.php";
            RestClient.post(url, params, new TextHttpResponseHandler() { // "http://89.39.159.65/marina/animens/validateLogin.php
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure registerUser" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    Log.d("AnimensPHP", "OnFailure registerUser" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("AnimensPHP", "registerUser " + responseString);

                    createChatRooms(nick, email, responseString, listener);
//                    listener.onSuccessRegister();
                }

            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }

    }


    private void createChatRooms(String nickNewUser, String email, String imagerandom, final RegisterUser listener){
        String url = AnimensApplication.BASE_URL+"animens/createChatRooms.php?nick=" + nickNewUser + "&picture=" + imagerandom;

        try {
            RestClient.get(url, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("Chatroom", "Response: "+ responseString);
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    Log.d("Chatroom", "Response: "+ throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    listener.onSuccessRegister();

                }
            });
        }
        catch (Exception e) {
            Log.d("Chatroom", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }

    }

    interface GetUserData{
        void onDataObtained();
        void onDataNotObtained();
        void onServerError();
    }


    interface RegisterUser{
        void onRegistered();
        void onNoRegistered();

        void onSuccessRegister();
        void onServerError();
    }

    interface GetIfNickExists{
        void onNickExists(String nick);
        void onNickDoesntExists(String nick);
        void onServerError();
    }
}
