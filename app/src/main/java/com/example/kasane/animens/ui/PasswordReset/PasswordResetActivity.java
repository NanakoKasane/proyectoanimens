package com.example.kasane.animens.ui.PasswordReset;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

/**
 * Activity que se encarga de mandar un email para resetear la contraseña (por Firebase)
 * @author Marina Espinosa
 */
public class PasswordResetActivity extends AppCompatActivity {
    Button btResetPassword;
    EditText edEmail;
    TextView tvGoSignIn;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);

        mAuth = FirebaseAuth.getInstance();
        inicializar();
    }

    /**
     * findviewbyid y listeners
     */
    private void inicializar(){
        btResetPassword = findViewById(R.id.btResetPassword);
        edEmail = findViewById(R.id.edEmail);
        tvGoSignIn = findViewById(R.id.tvGoSignIn);

        btResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edEmail.getText())){
                    CommonUtils.showMessage(PasswordResetActivity.this,  getString(R.string.err_emptyemail_reset));
//                    Toast.makeText(AnimensApplication.getContext(), R.string.err_emptyemail_reset, Toast.LENGTH_SHORT).show();
                }
                else{
                    // TODO. hideKeyboard();
                    showProgressBar(null, "Enviando email...");
                    resetPassword(edEmail.getText().toString());
                }
            }
        });

        tvGoSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goLogin();
            }
        });
    }

    // Manda correo para resetear contraseña
    private void resetPassword(String email){
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        hideProgressBar();

                        if (task.isSuccessful()) {
                            Toast.makeText(PasswordResetActivity.this, R.string.checkemailforreset, Toast.LENGTH_LONG).show();
                            goLogin();
                        } else {
                            Log.d("Firebase", String.valueOf(task.getException()));
                            CommonUtils.hideKeyboard(PasswordResetActivity.this, getWindow().getDecorView());

                            // TODO. FirebaseAuthInvalidCredentialsException -> el email no es correcto
                            if(task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                CommonUtils.showMessage(PasswordResetActivity.this,  getString(R.string.err_emailnotvalid));
//                                Toast.makeText(PasswordResetActivity.this,  getString(R.string.err_emailnotvalid),
//                                        Toast.LENGTH_SHORT).show();
                            }
                            // TODO. FirebaseAuthInvalidUserException -> No hay usuarios con este email
                            else if(task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                CommonUtils.showMessage(PasswordResetActivity.this,  getString(R.string.err_nouserwithemail));
//                                Toast.makeText(PasswordResetActivity.this, getString(R.string.err_nouserwithemail),
//                                        Toast.LENGTH_SHORT).show();
                            }
                            else if (task.getException() instanceof FirebaseAuthInvalidUserException){
                                CommonUtils.showMessage(PasswordResetActivity.this,  getString(R.string.err_nouserwithemail));
//                                Toast.makeText(PasswordResetActivity.this, getString(R.string.err_nouserwithemail),
//                                        Toast.LENGTH_SHORT).show();
                            }
                            else
                                CommonUtils.showMessage(PasswordResetActivity.this, R.string.err_generalresetpass);
//                                Toast.makeText(PasswordResetActivity.this, R.string.err_generalresetpass, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void goLogin(){
        Intent intent = new Intent(PasswordResetActivity.this, LoginActivity.class);
        startActivity(intent);
        PasswordResetActivity.this.finish();
    }


    private ProgressDialog progressDialog;

    public void hideProgressBar() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar(String titulo, String mensaje) {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(PasswordResetActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(mensaje); // R.string.msg_loading
        if (titulo != null)
            progressDialog.setTitle(titulo); // R.string.app_name
        progressDialog.show();
    }
}
