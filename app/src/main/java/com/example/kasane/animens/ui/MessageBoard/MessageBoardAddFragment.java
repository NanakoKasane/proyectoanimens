package com.example.kasane.animens.ui.MessageBoard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MessageBoardAddFragment extends BaseFragment implements MessageBoardAddContract.View{
    public static String TAG = "MessageBoardAddFragment";

    EditText edTitulo;
    EditText edMensaje;
    Spinner spCategoria;
    FloatingActionButton btnEdit;

    MessageBoardAddPresenter presenter;
    MessageBoard messageBoard;

    private static final int SELECT_FILE = 1;
    ImageView imgCamera;
    TextView tvImagenTitle;
    ImageView imgPerfil;

    Bitmap bitmap;

    public static MessageBoardAddFragment newInstance(Bundle bundle){
        MessageBoardAddFragment messageBoardAddFragment = new MessageBoardAddFragment();
        if (bundle != null)
            messageBoardAddFragment.setArguments(bundle);
        return messageBoardAddFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_board_add, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        // TODO. Lo siguiente no irá aquí, es otra clase modelo
        ArrayList<String> categorias = new ArrayList<>();
        categorias.add("Búsqueda de Amigos");
        categorias.add("Quedadas");

        // FIND VIEW BY ID
        inicializar();

        spCategoria.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.spgenres, categorias));
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter = new MessageBoardAddPresenter(MessageBoardAddFragment.this);
                presenter.validateMessageBoard(edTitulo.getText().toString(), edMensaje.getText().toString());
            }
        });


        // TODO. Si es editar:
        messageBoard = (MessageBoard) getArguments().getSerializable("MessageBoard");
        if (messageBoard != null){
            Toast.makeText(getContext(), "Editando", Toast.LENGTH_SHORT).show();

            // Cargar el spinner la categoría:
            spCategoria.setSelection(categorias.indexOf(messageBoard.getCategory()));

            edTitulo.setText(messageBoard.getTitle());
            edMensaje.setText(messageBoard.getMessage());


            if (messageBoard.getPicture() != null){
                Picasso.get()
                        .load(messageBoard.getPicture())
                        .fit()
                        .centerInside()
                        .into(imgPerfil,  new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError(Exception e) {
                            }
                        });

            }

//            imgPerfil.setImageBitmap(CommonUtils.base64ToBitmap(messageBoard.getPicture64())); // TODO.
        }
    }

    // TODO. ONCREATE
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private void inicializar(){
        edTitulo = getView().findViewById(R.id.edTitulo);
        edMensaje = getView().findViewById(R.id.edMensaje);
        spCategoria = getView().findViewById(R.id.spCategoria);
        btnEdit = getView().findViewById(R.id.btnEdit);

        tvImagenTitle = getView().findViewById(R.id.tvImagenTitle);
        imgCamera = getView().findViewById(R.id.imgCamera);
        imgPerfil = getView().findViewById(R.id.imgPerfil);

        // Subir foto
        imgCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });
        tvImagenTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });
        imgPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirGaleria(v);
            }
        });

    }


    @Override
    public void onEdited() {
        hideProgressBar();
        Toast.makeText(getContext(), "Mensaje añadido", Toast.LENGTH_SHORT).show();
        getFragmentManager().popBackStack();
    }

    @Override
    public void showTitleEmptyError() {
//        Toast.makeText(getContext(), "El título no puede estar vacío", Toast.LENGTH_SHORT).show();
        CommonUtils.showMessage(getActivity(), R.string.err_emptyTitle);
    }

    @Override
    public void showMessageEmptyError() {
//        Toast.makeText(getContext(), "El mensaje no puede estar vacío", Toast.LENGTH_SHORT).show();
        CommonUtils.showMessage(getActivity(), R.string.err_emptyMessage);
    }

    @Override
    public void onValidated() {
        MessageBoard msg;
        if (messageBoard == null){
            msg = new MessageBoard("4", CommonUtils.getCurrentUserNick(getContext()), spCategoria.getSelectedItem().toString(), edTitulo.getText().toString(),
                    "", edMensaje.getText().toString()); // TODO. CommonUtils.bitmapToBase64(((BitmapDrawable)imgPerfil.getDrawable()).getBitmap())

        }
        else
        {
            msg = new MessageBoard(messageBoard.getMessageBoard_id(), messageBoard.getUser_nick(), spCategoria.getSelectedItem().toString(),
                    edTitulo.getText().toString(), messageBoard.getPicture(), edMensaje.getText().toString()); // TODO. CommonUtils.bitmapToBase64(((BitmapDrawable)imgPerfil.getDrawable()).getBitmap())
        }

        showProgressBar(null, "Actualizando mensaje...", false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        presenter.editMessageBoard(messageBoard, msg, bitmap, getContext());
    }




    /**
     * Subir foto
     */
    public void abrirGaleria(View v){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), SELECT_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Uri selectedImageUri = null;
        Uri selectedImage;

        String filePath = null;
        switch (requestCode) {
            case SELECT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();
                    String selectedPath=selectedImage.getPath();
                    if (requestCode == SELECT_FILE) {

                        if (selectedPath != null) {
                            InputStream imageStream = null;
                            try {
                                imageStream = getContext().getContentResolver().openInputStream(
                                        selectedImage);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }

                            // Transformamos la URI de la imagen a inputStream y este a un Bitmap
                            Bitmap bmp = BitmapFactory.decodeStream(imageStream);

                            // Guardo el bitmap
                            bitmap = bmp;


                            // Ponemos nuestro bitmap en un ImageView que tengamos en la vista
                            imgPerfil.setImageBitmap(bmp);

                        }
                    }
                }
                break;
        }
    }

}
