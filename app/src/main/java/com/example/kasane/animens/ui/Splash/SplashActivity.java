package com.example.kasane.animens.ui.Splash;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.db.DAO.AnimensOpenHelper;
import com.example.kasane.animens.data.db.Repository.AnimeRepository;
import com.example.kasane.animens.data.db.Repository.Anime_genres_Repository;
import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Genre;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.example.kasane.animens.ui.User.UserListActivity;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity implements SplashContract.View {

    ProgressBar progressBar;
    SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = findViewById(R.id.progressBar);
        presenter = new SplashPresenter(this);
        presenter.downloadAnimeData();
        presenter.downloadGenres();



//        progressBar.getProgressDrawable().setColorFilter(
//                Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
    }

    // Me he descargado los Animes y ya puedo insertarlos a SQLite (SOLO SE HACE SI NO HABIA NINGUNA INSERCIÓN)
    @Override
    public void onDownloadedAnimeData(ArrayList<Anime> animesDownloaded) {
        presenter.insertAnimes(animesDownloaded); // inserto los animes
    }

    @Override
    public void onDownloadedAnime_genresData(ArrayList<Anime_genres> anime_genres) {
        presenter.insertAnime_genres(anime_genres);

    }

    @Override
    public void onGenresDownloaded(ArrayList<Genre> genres) {
        presenter.insertGenres(genres);
    }


    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        logIn();
    }

    // Ya había inserciones
    @Override
    public void itWasAlreadyDownloaded() {
        logIn();
    }


    private void logIn(){
        Intent intent = new Intent(SplashActivity.this, UserListActivity.class);
        startActivity(intent);
        this.finish();
    }

    // Cuando se ha insertado
    @Override
    public void onInsertedAnimes() {
        // TODO. UNA VEZ INSERTADO YA DESCARGO LOS ANIME_GENRES, QUE TIENEN FOREIGN KEY DE ANIME
        presenter.downloadAnime_genresData(); // TODO. Solo hacerlo cuando se haya insertado correctamente los aniems
    }

    @Override
    public void onInsertedAnime_genres() {
        logIn();
    }
}
