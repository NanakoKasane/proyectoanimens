package com.example.kasane.animens.ui.Anime;

import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;

public interface AnimeAddContract {
    interface View{
        void onEdited(Anime anime);
        void onValidated();

        void showEmptyNameError();
        void showEmptyEpisodesError();
        void showEmptyPictureError();
        void showInvalidEpisodesNumber();

        void showGenresStringList(ArrayList<String> genres);
    }

    interface Presenter{
        void validateAnime(String nombre, String url, String tipo, String numCaps);
        void editAnime(Anime animeAEditar, Anime animeEditado);

        void getGenresStringList();
    }
}
