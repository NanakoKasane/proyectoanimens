package com.example.kasane.animens.ui.Login;

import android.content.Context;

import com.example.kasane.animens.data.network.model.User;

public interface LoginContract {
    interface View{
        void sucessfulLogin(String email);
        void showMessageError();

        void onLoadCredentials(String user, String email, String password);
        void onNotObtainedCredentials();

        void onSavedCredentials();

        void onServerError();
//        void onCurrentUserObtained(User user);

    }

    interface Presenter{
        void validateCredentials(String user, String password);

//        void getCurrentUser(Context context);
        void getCredentials(Context context);
        void saveCredentials(String user, String email, String password, Context context);

    }
}
