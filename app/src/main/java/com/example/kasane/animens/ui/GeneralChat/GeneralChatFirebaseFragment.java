package com.example.kasane.animens.ui.GeneralChat;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.firebase.model.Messages;
import com.example.kasane.animens.data.firebase.model.PublicMessages;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.Chats.ConversationFirebaseContract;
import com.example.kasane.animens.ui.Chats.ConversationFirebaseFragment;
import com.example.kasane.animens.ui.adapter.GeneralChatFirebaseAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Fragment que gestiona el listado de los Mensajes del chat general de Firebase con otro usuario
 * @author Marina Espinosa
 */
public class GeneralChatFirebaseFragment extends BaseFragment implements GeneralChatFirebaseContract.View {

    // vistas
    private ImageButton mSendButton;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private EditText mMessageEditText;

    // firebase
    public static final String MESSAGES_CHILD = "public_messages"; // nombre en el JSON de firebase del hijo. El objeto "messages", tiene los mensajes
    private DatabaseReference mFirebaseDatabaseReference;
    FirebaseRecyclerAdapter mFirebaseAdapter;

    GeneralChatFirebasePresenter presenter;
    Profile profile = null;


    public static ConversationFirebaseFragment newInstance(Bundle bundle){
        ConversationFirebaseFragment conversationFirebaseFragment = new ConversationFirebaseFragment();
        if (bundle != null){
            conversationFirebaseFragment.setArguments(bundle);
        }
        return conversationFirebaseFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation_firebase, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inicializo (findViewById y click de botones)
        inicializar();

        presenter = new GeneralChatFirebasePresenter(this);
//        showProgressBar(null, "Cargando...");

        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
        }
        else
            presenter.getCurrentUserProfile();


        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference(); // Referencia a la base de datos

        // TODO. Lógica de firebase
        SnapshotParser<PublicMessages> parser = new SnapshotParser<PublicMessages>() {
            @Override
            public PublicMessages parseSnapshot(DataSnapshot dataSnapshot) {
                PublicMessages friendlyMessage = dataSnapshot.getValue(PublicMessages.class); // TODO. Clase modelo para parseo.
                if (friendlyMessage != null) {
                    friendlyMessage.setId(dataSnapshot.getKey());
                    Log.d("Firebase", friendlyMessage.getId());
                }
                return friendlyMessage;
            }
        };

        DatabaseReference messagesRef = mFirebaseDatabaseReference.child(MESSAGES_CHILD); // TODO. accedo a "messages"
        FirebaseRecyclerOptions<PublicMessages> options =
                new FirebaseRecyclerOptions.Builder<PublicMessages>()
                        .setQuery(messagesRef, parser)
                        .build();

        // TODO. nuevo adapter
        mFirebaseAdapter = new GeneralChatFirebaseAdapter(mProgressBar, options); // TODO. En vez de chatId le pasaremos el objeto ChatRoom

        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition =
                        mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
             }
        });

        mMessageRecyclerView.setAdapter(mFirebaseAdapter);

    }




    // TODO. FindViewById y listeners
    private void inicializar(){
        if (getView() != null) {
            mProgressBar = getView().findViewById(R.id.progressBar);
            mMessageEditText = getView().findViewById(R.id.messageEditText);
            mSendButton = getView().findViewById(R.id.sendButton);
            mSendButton.setEnabled(false);

            mMessageRecyclerView = getView().findViewById(R.id.messageRecyclerView);
            mLinearLayoutManager = new LinearLayoutManager(getContext());
            mLinearLayoutManager.setStackFromEnd(true);
            mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);

            mMessageEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (charSequence.toString().trim().length() > 0) {
                        mSendButton.setEnabled(true);
                    } else {
                        mSendButton.setEnabled(false);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

            // TODO. Enviar mensaje
            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // TODO. subo el mensaje a la base de datos de firebase
                    if (profile != null) {
                        PublicMessages publicMessage = new PublicMessages(profile.getNick(),
                                profile.getPicture(),
                                mMessageEditText.getText().toString(),
                                CommonUtils.getFormattedCurrentDate(),
                                CommonUtils.getFormattedCurrentTime());

                        mFirebaseDatabaseReference.child(MESSAGES_CHILD)
                                .push().setValue(publicMessage);  // TODO. AQUI ESCRIBO EN LA BASE DE DATOS DE FIREBASE
                        mMessageEditText.setText("");
                    }

//                PublicMessages friendlyMessage = new
//                        PublicMessages(mMessageEditText.getText().toString(),
//                        usuarioActual, // "mary" TODo. Esto vendrá de la clase conversation
//                        receiverNick, //"anonymous" TODO. tambien, este es el otro usuario
//                        CommonUtils.getFormattedCurrentTime(),
//                        String.valueOf(chatroom.getChat_id()),// chatId
//                        CommonUtils.getFormattedCurrentDate()
//                );
//                mFirebaseDatabaseReference.child(MESSAGES_CHILD)
//                        .push().setValue(friendlyMessage);  // TODO. AQUI ESCRIBO EN LA BASE DE DATOS DE FIREBASE
//                mMessageEditText.setText("");

                }
            });
        }

    }




    @Override
    public void onPause() {
        mFirebaseAdapter.stopListening();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mProgressBar.setVisibility(View.VISIBLE);
        mFirebaseAdapter.startListening();
    }


    @Override
    public void onProfileObtained(Profile profile) {
//        hideProgressBar();
        this.profile = profile;
    }

    @Override
    public void onServerError() {
//        hideProgressBar();

        if (getActivity() != null){
            CommonUtils.showMessage(getActivity(), R.string.err_server);
        }
    }
}
