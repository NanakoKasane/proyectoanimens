package com.example.kasane.animens.ui.Pofile;

import android.content.Context;

import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;

import java.util.ArrayList;

public interface ProfileContact {
    interface View{
        void loadProfile(Profile profile);
        void onServerError();

        void onFriendRequestSended();

        void onRequestCanBeSent();
        void onNoRequestCanBeSent();

        void onFriendListObtained(ArrayList<User> friends);

        void onFriendDeleted(String friendNick);

        void onChatroomObtained(Chatroom chatroom);
//        void loadAnimes(ArrayList<Anime> animes);
//        void loadFriends(ArrayList<User> users);
    }

    interface Presenter{
        void getProfile(Context context);
        void getProfile(User user);

        void sendFriendRequest(String nickCurrentUser, String nickUserFriend);

        void checkIfRequestCanBeSent(String nick);

        void getFriendList();

        void deleteFriend(String friendNick);

        void getChatroom(String nick);
//        void getAnimes(Profile profile);
//        void getFriends(Profile profile);
    }
}
