package com.example.kasane.animens.data.network.model;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

public class MessageBoard implements Serializable, Comparable<MessageBoard> {
    private String messageBoard_id; // varchar100
    private String user_nick;
    private String user_id; // Todo. De tipo user y hacemos user.Nick
    private String category; // Todo. De tipo categoría y hacemos category.category
    private String title; // 20 max. TODO. Debería ser 10 max
    private String picture;
    private Bitmap image;
    private String message; // max 300. TODO. Debería ser 80 max
    private boolean isReported;

    private long time;
    private byte[] imageBytes;

//    private String picture64;

    Date tiempo;


    public MessageBoard(String messageBoard_id, String user_nick, String category, String title, String picture, String message) {
        this.messageBoard_id = messageBoard_id;
        this.user_nick = user_nick;
        this.category = category;
        this.title = title;
        this.picture = picture;
        this.message = message;
        this.isReported = false;
    }

    public MessageBoard(String user_id, String category, String title, String picture, String message) {
        this.user_id = user_id;
        this.category = category;
        this.title = title;
        this.picture = picture;
        this.message = message;
        this.isReported = false;
    }


    public String getMessageBoard_id() {
        return messageBoard_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isReported() {
        return isReported;
    }

    public void setReported(boolean reported) {
        isReported = reported;
    }

    public void setUser_nick(String user_nick) {
        this.user_nick = user_nick;
    }

    public String getUser_nick() {
        return user_nick;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }


    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }


    @Override
    public boolean equals(Object obj) {
        return this.messageBoard_id.equals(((MessageBoard)obj).messageBoard_id);
    }

    // Comparando por id
    @Override
    public int compareTo(@NonNull MessageBoard o) {

        // TODO. COMPARAR POR TIEMPO NO HACE FALTA YA QUE EL ID ES INCREMENTABLE AUTOMÁTICAMENTE. Cuanto más id (id mayor), más reciente es el mensaje también

//        if (this.tiempo.before(o.tiempo))
//            return -1; // Si la hora está antes, va después (ya que es menos reciente)
//        return 1; // si no, irá antes

        return this.getMessageBoard_id().compareTo(o.getMessageBoard_id());

    }


    // Comparando por id del user
    public static class OrderByUserId implements Comparator<MessageBoard> {
        @Override
        public int compare(MessageBoard o1, MessageBoard o2) {
            return o1.getUser_id().compareTo(o2.getUser_id());
        }
    }
}
