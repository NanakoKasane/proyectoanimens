package com.example.kasane.animens.ui.Chats;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Activity que gestiona los mensajes de una conversación de Firebase
 * @author Marina Espinosa
 */
public class ConversationFirebaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_firebase);

        Toolbar toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);


        //  Obtendré el objeto Conversation y lo pasaré al fragment
        Chatroom chatroom = (Chatroom) getIntent().getSerializableExtra("chatroom");


        //  pongo la imagen y nick del usuario en la toolbar personalizada
        TextView tv_username = findViewById(R.id.tv_username);
        CircleImageView img_user = findViewById(R.id.img_user);
        ProgressBar progressBar = findViewById(R.id.progressBar);

        String nickOtherUser;
        String imageOtherUser;
        if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(chatroom.getNick1())){
            nickOtherUser = chatroom.getNick2();
            imageOtherUser = chatroom.getPicture_nick2();
        }
        else{
            nickOtherUser = chatroom.getNick1();
            imageOtherUser = chatroom.getPicture_nick1();
        }

        tv_username.setText(nickOtherUser); // "usuario!"


        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);

        Picasso.get()// "https://d500.epimg.net/cincodias/imagenes/2018/11/13/lifestyle/1542113135_776401_1542116070_noticia_normal.jpg"
                .load(imageOtherUser)//Del objeto conversacion de la API
                .fit()
                .centerInside()
                .error(R.drawable.imagenotfound)
                .into(img_user, new com.squareup.picasso.Callback()
                {
                    @Override
                    public void onSuccess() {
//                                        if (holder.progressBar != null) {
//                                            holder.progressBar.setVisibility(View.GONE);
//                                        }
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                    }
                    @Override
                    public void onError(Exception e) {
//                                        if (holder.progressBar != null) {
//                                            holder.progressBar.setVisibility(View.GONE);
//                                        }
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                    }

                });


//        getSupportActionBar().setTitle("hola");
        // Icono de volver atrás
//        toolbar.setNavigationIcon(R.drawable.ic_action_send);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // Your code
//                finish();
//            }
//        });
////
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

//        getSupportActionBar().setLogo(R.mipmap.animenslogo);
//        getSupportActionBar().setDisplayUseLogoEnabled(true);




        // TODO. Obtendré el objeto Conversation y lo pasaré al fragment

        Bundle bundle = null;
        if (chatroom != null){
            bundle = new Bundle();
            bundle.putSerializable("chatroom", chatroom);
            getSupportFragmentManager().beginTransaction().replace(R.id.conversationcontent, ConversationFirebaseFragment.newInstance(bundle)).commit();
        }


    }

//     Volver Atras con back y con flechita de arriba
    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        // TODO.
        try {
            CommonUtils.hideKeyboard(this, getWindow().getDecorView().getRootView());
        }
        catch (Exception e){
            Log.d("Conversation", e.getMessage());
        }

        this.finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
