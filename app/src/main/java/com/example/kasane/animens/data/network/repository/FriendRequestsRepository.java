package com.example.kasane.animens.data.network.repository;

import com.example.kasane.animens.data.network.model.FriendRequests;

import java.util.ArrayList;

public class FriendRequestsRepository {
    private static FriendRequestsRepository friendRequestsRepository;
    private ArrayList<FriendRequests> friendRequests;

    private FriendRequestsRepository(){
        friendRequests = new ArrayList<>();
        friendRequests.add(new FriendRequests("1", "maria2131", "lourdes", "https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png"));
        friendRequests.add(new FriendRequests("2", "ismael212", "lourdes", "https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png"));

    }

    public ArrayList<FriendRequests> getFriendRequests() {
        return friendRequests;
    }

    public static FriendRequestsRepository getInstance() {
        if (friendRequestsRepository == null){
            friendRequestsRepository = new FriendRequestsRepository();
        }
        return friendRequestsRepository;
    }
}
