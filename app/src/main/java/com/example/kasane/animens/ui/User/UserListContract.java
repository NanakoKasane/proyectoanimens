package com.example.kasane.animens.ui.User;

import android.content.Context;

import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;

import java.util.ArrayList;

public interface UserListContract {
    interface View{
        void showUserList(ArrayList<User> users);
        void onServerError();

        void onUserProfile(Profile profile);
        void onNoFriends();
    }

    interface Presenter{
        void getUserList();
        void getFriendList(String nick);

        void getUserProfile(String nick);
    }
}
