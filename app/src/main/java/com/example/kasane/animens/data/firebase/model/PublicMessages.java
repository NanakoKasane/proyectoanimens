package com.example.kasane.animens.data.firebase.model;

/**
 * Clase modelo para los mensajes del chat público (chat general) Firebase
 * @author Marina Espinosa
 */
public class PublicMessages {
    private String id; // id del mensaje

    private String senderNick;
    private String senderPicture;
    private String message;
    private String daySended;
    private String hourSended;

    public PublicMessages(){

    }

    public PublicMessages(String senderNick, String senderPicture, String message, String daySended, String hourSended) {
        this.senderNick = senderNick;
        this.senderPicture = senderPicture;
        this.message = message;
        this.daySended = daySended;
        this.hourSended = hourSended;
    }

    public PublicMessages(String id, String senderNick, String senderPicture, String message, String daySended, String hourSended) {
        this.id = id;
        this.senderNick = senderNick;
        this.senderPicture = senderPicture;
        this.message = message;
        this.daySended = daySended;
        this.hourSended = hourSended;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderNick() {
        return senderNick;
    }

    public void setSenderNick(String senderNick) {
        this.senderNick = senderNick;
    }

    public String getSenderPicture() {
        return senderPicture;
    }

    public void setSenderPicture(String senderPicture) {
        this.senderPicture = senderPicture;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDaySended() {
        return daySended;
    }

    public void setDaySended(String daySended) {
        this.daySended = daySended;
    }

    public String getHourSended() {
        return hourSended;
    }

    public void setHourSended(String hourSended) {
        this.hourSended = hourSended;
    }
}
