package com.example.kasane.animens.ui.Login;

import android.content.Context;

public class LoginsFirebasePresenter implements LoginsFirebaseContract.Presenter, LoginsFirebaseInteractor.GetIfNickExists, LoginsFirebaseInteractor.RegisterUser, LoginsFirebaseInteractor.GetUserData {
    LoginsFirebaseInteractor interactor;
    LoginsFirebaseContract.View view;

    public LoginsFirebasePresenter(LoginsFirebaseContract.View view){
        interactor = new LoginsFirebaseInteractor();
        this.view = view;
    }

    @Override
    public void getCurrentUserData(Context context, String email) {
        interactor.getCurrentUserData(context, email, this);
    }

    @Override
    public void getIfUserIsRegistered(String email) {
        interactor.getIfUserIsRegistered(email, this);
    }

    @Override
    public void getIfNickExists(String nick) {
        interactor.getIfNickExists(nick, this);
    }

    @Override
    public void registerUser(String email, String nick) {
        interactor.registerUser(email, nick, this);
    }


    /**
     * interactor
     */
    @Override
    public void onRegistered() {
        view.onRegistered();
    }

    @Override
    public void onNoRegistered() {
        view.onNoRegistered();
    }

    @Override
    public void onSuccessRegister() {
        view.onSuccessRegister();
    }

    @Override
    public void onNickExists(String nick) {
        view.onNickExists(nick);
    }

    @Override
    public void onNickDoesntExists(String nick) {
        view.onNickDoesntExists(nick);
    }

    @Override
    public void onDataObtained() {
        view.onDataObtained();
    }

    @Override
    public void onDataNotObtained() {

    }

    @Override
    public void onServerError() {
        view.onServerError();
    }
}
