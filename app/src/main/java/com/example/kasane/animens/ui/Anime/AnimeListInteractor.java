package com.example.kasane.animens.ui.Anime;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.db.Repository.AnimeRepository;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Anime;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase Interactor que se comunica con el servidor y obtiene las listas necesarias
 */
public class AnimeListInteractor {

    public void editRating(Anime anime, float rating, AnimeListInteractor.EditRating listener){

        // Insert si no existe en ANIME_USER, update si ya existe

        RequestParams requestParams = new RequestParams();
        requestParams.put("nick", CommonUtils.getCurrentUserNick(AnimensApplication.getContext()));
        requestParams.put("animeid", anime.getAnime_id());
        requestParams.put("rating", rating);
        RestClient.post(AnimensApplication.BASE_URL+"animens/rateAnime.php", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                listener.onServerError();
                Log.d("AnimeListInteractor", responseString + "\n" + throwable.getMessage());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                listener.onRatingEdited(anime, rating);
            }
        });

        //  select anime.anime_id, user.user_id, nick, rating_useranime, anime.name from anime_user join user on user.user_id = anime_user.user_id join anime on anime.anime_id = anime_user.anime_id ;


//        RestClient.get("https://marina.edufdezsoy.es/animens/rateAnime.php?nick="+ CommonUtils.getCurrentUserNick(AnimensApplication.getContext()) + "&animeid=" + anime.getAnime_id() + "&rating=" + rating,
//                new TextHttpResponseHandler() {
//            @Override
//            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
////                listener.onServerError();
//                Log.d("AnimeListInteractor", responseString + "\n" + throwable.getMessage());
//            }
//
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                listener.onRatingEdited(anime, rating);
//            }
//        }); //?nick=lourdes&animeid=1&rating=9");



        // Update a la base de datos, la tabla ANIME_USER le pongo al usuario actual el nuevo rating (rating)
        // OPCIONAL, PORQUE SE PUEDE HACER CON UN TRIGGER BEFORE UPDATE EN ANIME_USER -> Update a la base de datos, la tabla ANIME, recalculo el nuevo rating

//        ArrayList<Anime> animes = AnimeListRepository.getAnimeListRepository().getAnimeList();
//        int indice = animes.indexOf(anime);
//        if (indice != -1){
//            animes.get(indice).setRating(rating);
//            listener.onRatingEdited(anime, rating);
//        }

    }


    public void deleteAnime(Anime anime, AnimeListInteractor.DeleteAnime listener) {
//        AnimeListRepository.getAnimeListRepository().getAnimeList().remove(anime);

        AnimeRepository.getInstance().delete(anime.getAnime_id());
        listener.onDeleted();
    }


    public void getAnimeList(final ArrayList<Anime> animes, final AnimeListInteractor.GetAnimeList listener){

        // debería hacer un limit algo y al hacer scroll volver a sacar consulta y cargar los siguientes


        // DE SQLITE
//        listener.onAnimeListObtained(AnimeRepository.getInstance().getAnimeList(), null);



        // Del servidor
        RestClient.get(AnimensApplication.BASE_URL+"animens/animelist.php", new JsonHttpResponseHandler() {  // "http://192.168.1.132/listaAnimens/animelist.php
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<Anime> animes = gson.fromJson(response.getJSONArray("animes").toString(), new TypeToken<ArrayList<Anime>>(){}.getType());

                    listener.onAnimeListObtained(animes, null);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensDB", e.getMessage());
                    listener.onServerError(); //e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB", throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB", throwable.getMessage());
                listener.onServerError();
            }

        });





//        /**
//         * Me bajo las imágenes
//         */
//        new AsyncTask<Void, Void,  ArrayList<Bitmap>>()
//        {
//            @Override
//            protected ArrayList<Bitmap> doInBackground(Void... voids) {
//                // return AnimeListRepository.getAnimeListRepository().getImagesBitMap();
//                if (animes == null)
//                    return AnimeListRepository.getAnimeListRepository().getImagesBitMap();
//                else
//                    return CommonUtils.getImagesBitMap(animes);
//
//            }
//
//            @Override
//            protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
//                // Los animes pueden ser null y deberemos obtenerlos. Si no, es que ya la tenemos y solo hay que descargar las imágenes.
//                if (animes == null)
//                    listener.onAnimeListObtained(AnimeListRepository.getAnimeListRepository().getAnimeList(), bitmaps);
//                else
//                    listener.onAnimeListObtained(animes, bitmaps);
//            }
//        }.execute();

    }



    public void getAnimeListOfThisGenre(final String genres, final AnimeListInteractor.GetAnimeList listener){
        RequestParams requestParams = new RequestParams();
        requestParams.put("genre", genres);
        RestClient.post(AnimensApplication.BASE_URL+"animens/animeofgenre.php", requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<Anime> animes = gson.fromJson(response.getJSONArray("animes").toString(),
                                                    new TypeToken<ArrayList<Anime>>(){}.getType());
                    listener.onAnimeListObtained(animes, null);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensDB", e.getMessage());
                    listener.onServerError(); //e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensDB", responseString + "\n" + throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("AnimensDB",  throwable.getMessage());
                listener.onServerError();
            }
        });

    }


    public void getAnimeListOfThisUser(final String nick, final AnimeListInteractor.GetAnimeList listener){

        // debería hacer un limit algo y al hacer scroll volver a sacar consulta y cargar los siguientes

        // Consulta a la BD de obtener los animes de ese USER (nick):

//            /**
//             * Me bajo las imágenes
//             */
//            new AsyncTask<Void, Void,  ArrayList<Bitmap>>()
//            {
//                @Override
//                protected ArrayList<Bitmap> doInBackground(Void... voids) {
//                    return AnimeListRepository.getAnimeListRepository().getImagesBitMap();
//                }
//
//                @Override
//                protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
//                    listener.onAnimeListObtained(AnimeListRepository.getAnimeListRepository().getAnimeList(), bitmaps);
//
//                }
//            }.execute();


        try {
            String url = AnimensApplication.BASE_URL+"animens/getFavoritesAnimes.php?nick=" + nick;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Anime> animes = gson.fromJson(response.getJSONArray("animes").toString(), new TypeToken<ArrayList<Anime>>(){}.getType());

                        if (animes.size() == 0)
                            listener.onNoResults();
                        else
                            listener.onAnimeListObtained(animes, null);


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }

            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }


    }





    interface GetAnimeList{
        void onAnimeListObtained(ArrayList<Anime> anime, ArrayList<Bitmap> images);

        void onServerError();
        void onNoResults();
    }

    interface EditRating{
        void onRatingEdited(Anime anime, float rating);

        void onServerError();
    }

    interface DeleteAnime{
        void onDeleted();
    }
}
