package com.example.kasane.animens.ui.ReportUser;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class ReportUserInteractor {

    public void reportUser(String nick, Bitmap imagenbitmap, String descripcion, String motivos, ReportUser listener){
        String currentUserNick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        //  SUBO IMAGEN AL SERVER:
        if (imagenbitmap != null){
            String url = AnimensApplication.BASE_URL+"animens/imagenes/upload.php";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String Response = jsonObject.getString("response");

                        String imagen = AnimensApplication.BASE_URL+"animens/imagenes/" + Response; //"http://marina.alumno.mobi/imagenes/" + Response;
                        Log.d("SubirFoto", imagen);


                        // UPDATE EN TABLA reportes
                        String urlEditar = AnimensApplication.BASE_URL+"animens/insertReport.php";
                        RequestParams requestParams = new RequestParams();
                        requestParams.put("nickUserReported", nick);
                        requestParams.put("nickUserWhoReports", currentUserNick);
                        requestParams.put("imagePrueba", imagen);
                        requestParams.put("motivos", motivos);
                        requestParams.put("descripcion", descripcion);

                        RestClient.post(urlEditar, requestParams, new TextHttpResponseHandler() {
                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                Log.d("ReportError", responseString);
                                listener.onServerError();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                                Log.d("ReportError", responseBytes != null ? new String(responseBytes) : "error");
                                listener.onServerError();
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                listener.onReported();
                            }
                        });



                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("SubirFoto", "Catch de subir foto 1 "+ e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), "error: " + error.toString(), Toast.LENGTH_LONG).show();
                    listener.onPhotoTooBig(); // foto demasiado grande ?
                    Log.d("SubirFoto", "On error response de Volley: "+ error.getMessage());
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    String gambar = CommonUtils.imagetoString(imagenbitmap);
                    params.put("foto", gambar);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AnimensApplication.getContext());
            requestQueue.add(stringRequest);
        }
        else{ // SIN FOTO
            // UPDATE EN TABLA reportes
            String urlEditar = AnimensApplication.BASE_URL+"animens/insertReport.php";
            RequestParams requestParams = new RequestParams();
            requestParams.put("nickUserReported", nick);
            requestParams.put("nickUserWhoReports", currentUserNick);
            requestParams.put("motivos", motivos);
            requestParams.put("descripcion", descripcion);

            RestClient.post(urlEditar, requestParams, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("ReportError", responseString);
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBytes, Throwable throwable) {
                    Log.d("ReportError", responseBytes != null ? new String(responseBytes) : "error");
                    listener.onServerError();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    listener.onReported();
                }
            });
        }
    }

    public interface ReportUser{
        void onPhotoTooBig();
        void onServerError();
        void onReported();
    }
}
