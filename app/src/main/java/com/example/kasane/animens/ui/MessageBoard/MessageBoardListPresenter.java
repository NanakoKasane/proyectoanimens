package com.example.kasane.animens.ui.MessageBoard;

import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;

public class MessageBoardListPresenter implements MessageBoardListContract.Presenter, MessageBoardListInteractor.GetMessageBoards, MessageBoardListInteractor.DeleteMessageBoard, MessageBoardListInteractor.GetUserProfile{
    private MessageBoardListContract.View view;
    private MessageBoardListInteractor interactor;

    public MessageBoardListPresenter(MessageBoardListContract.View view){
        this.view = view;
        interactor = new MessageBoardListInteractor();
    }


    // Presenter
    @Override
    public void getMessages() {
        view.showProgressBar();
        interactor.getMessages(this);
    }

    @Override
    public void deleteMessage(MessageBoard messageBoard) {
        interactor.deleteMessage(messageBoard, this);
    }

    @Override
    public void getUserProfile(String nick) {
        interactor.getUserProfile(nick, this);
    }

    // Interactor
    @Override
    public void onMessageBoardsObtained(ArrayList<MessageBoard> messageBoards, ArrayList<Bitmap> bitmaps) {
        view.hideProgressBar();
        view.showMessages(messageBoards, bitmaps);
    }


    @Override
    public void onDeleted() {
        view.onMessageDeleted();
    }

    @Override
    public void onUserProfile(Profile profile) {
        view.onUserProfile(profile);
    }
}
