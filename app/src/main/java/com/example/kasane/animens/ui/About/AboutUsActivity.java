package com.example.kasane.animens.ui.About;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.example.kasane.animens.R;
import com.vansuita.materialabout.builder.AboutBuilder;
import com.vansuita.materialabout.views.AboutView;

import butterknife.BindView;
import butterknife.ButterKnife;

// Actualmente en desuso
public class AboutUsActivity extends AppCompatActivity {
    @BindView(R.id.aboutView) AboutView aboutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        AboutView view = AboutBuilder.with(this)
                .setPhoto(R.drawable.emirem)
                .setCover(R.drawable.fondo)
                .setName("Animens S.A.")
                .setSubTitle("Compañía desarrolladora de aplicaciones android para gustos específicos")
                .setAppIcon(R.drawable.animenslogo)
                .setAppName(R.string.app_name)
                .addFiveStarsAction()
                .setVersionNameAsAppSubTitle()
                .addShareAction(R.string.app_name)
                .setWrapScrollView(true)
                .setLinksAnimated(true)
                .setShowAsCard(true)
                .build();

        addContentView(view, aboutView.getLayoutParams());
    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }
}
