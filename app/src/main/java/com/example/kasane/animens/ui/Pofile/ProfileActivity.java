package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Anime.AnimeActivity;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.NavigationDrawer;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.example.kasane.animens.ui.User.UserListFragment;
import com.example.kasane.animens.ui.prefs.PreferencesActivity;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Activity que gestiona el Perfil. Carga los fragment
 * @author Marina Espinosa
 */
public class ProfileActivity extends BaseActivity implements ProfileFragment.Listener, UserListFragment.GoToUser { // TODO.

    Fragment fragmentProfile;
    Fragment fragmentProfileEdit;
    Fragment fragmentUserList;
    String nick = null;

    @Override
    public void getFriends(String nick) {
        this.nick = nick;
        // TODO. Cargar fragment de userlist

//        Toast.makeText(this, "Mis amigos ", Toast.LENGTH_SHORT).show();

        // fragmentUserList = getSupportFragmentManager().findFragmentByTag(UserListFragment.TAG);
        //   if (fragmentUserList == null){

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.friendfromprofile);

        Bundle bundle = new Bundle();
        bundle.putString("Nick", nick);
        fragmentUserList = UserListFragment.newInstance(bundle);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mcontent, fragmentUserList);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        //  }
    }

    @Override
    public void getAnimes(String nick) {
        this.nick = null;
//        Toast.makeText(this, "Mis animes favoritos", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(ProfileActivity.this, AnimeActivity.class);
        intent.putExtra("Nick", nick);
        startActivity(intent);
    }

    @Override
    public void onProfileEdit(Profile profile) {
        this.nick = null;
        fragmentProfileEdit = new ProfileEditFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("profile", profile);
        fragmentProfileEdit.setArguments(bundle);

        // TODO. Voy a fragment de editar
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mcontent, fragmentProfileEdit, ProfileEditFragment.TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.nick = null;

        // setContentView(R.layout.activity_profile);


        // TODO. Cargo el primer fragment: ProfileFragment (ESTÁN EN android.R.layout.content, de la activity_base):


        Bundle bundle = null;
        Profile profile = (Profile) getIntent().getSerializableExtra("Profile");
        if (profile != null){
            bundle = new Bundle();
            bundle.putSerializable("Profile", profile);

            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(profile.getNick());
        }
        else{
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(R.string.nav_profile);
        }

        // fragmentProfile = getSupportFragmentManager().findFragmentByTag(ProfileFragment.TAG);

        if (fragmentProfile == null) {
            fragmentProfile = ProfileFragment.newInstance(bundle);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mcontent, fragmentProfile,ProfileFragment.TAG );
            fragmentTransaction.commit();

        }



        // Todo. Debo obtener los datos del usuario ACTUAL. (a esta actividad se le pasa el ID del usuario y se hace consulta a la BBDD, sacando el Profile del usuario)



        // TODO. Si se cambia la foto como actualizo el NavigationDrawer? :(

//        View hView =  navigationView.getHeaderView(0);
//        CircleImageView nav_user = hView.findViewById(R.id.profile_image);
//
//        Picasso.get()
//                .load(AnimensApplication.getCurrentUser().getPicture())
//                .fit()
//                .centerInside()
//                .into(nav_user,  new com.squareup.picasso.Callback() {
//                    @Override
//                    public void onSuccess() {
//                    }
//
//                    @Override
//                    public void onError(Exception e) {
//                    }
//                });

    }



    // TODO. Intent a UserListActivity y ir a tu perfil desde ahi


    // Para poder ir al fragment del user desde aquí hay que implementar goToUser
    @Override
    public void goToUser(Profile profile) {
        this.nick = null;

        // TODO. Cargo el  perfil del user

// //       Intent intent = new Intent(ProfileActivity.this, UserListActivity.class);
// //       startActivity(intent);

//        Toast.makeText(this, "Yendo al perfil del user desde tu perfil", Toast.LENGTH_SHORT).show();
//
//
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(profile.getNick());

        Bundle bundle = new Bundle();
        bundle.putSerializable("Profile", profile);
        fragmentProfile = ProfileFragment.newInstance(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mcontent, fragmentProfile);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        Profile profile = (Profile) getIntent().getSerializableExtra("Profile");

        // si no es del usuario actual, vuelvo al listado de Usuarios (UserList)
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 && profile != null && !CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(profile.getNick())){
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(R.string.nav_users);
            Intent intent = new Intent(this, UserListActivity.class);
            startActivity(intent);
        }
        else if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else {
                super.onBackPressed();

                if (getSupportActionBar() != null) {
//                if (profile != null && !CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(profile.getNick()))
//                    getSupportActionBar().setTitle(profile.getNick());
//                else

//                if (nick != null)
//                    getSupportActionBar().setTitle(R.string.friendfromprofile);

                    if (getSupportFragmentManager().getBackStackEntryCount() == 0)
                        getSupportActionBar().setTitle(R.string.nav_profile);

                    else
                        getSupportActionBar().setTitle(R.string.friendfromprofile);
//
                }
            }
        }

    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater menuInflater = getMenuInflater();
//        menuInflater.inflate(R.menu.application_menu, menu);
//
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.itmenu_Preferences:
//                Intent intent = new Intent(ProfileActivity.this, PreferencesActivity.class);
//                startActivity(intent);
//                break;
//
////            case android.R.id.home:
////                FragmentManager fm = getSupportFragmentManager();
////                if (fm.getBackStackEntryCount() > 0) {
////                    fm.popBackStack();
////                }
////                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

}
