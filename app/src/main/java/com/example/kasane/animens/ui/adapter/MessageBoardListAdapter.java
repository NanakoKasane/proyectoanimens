package com.example.kasane.animens.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class MessageBoardListAdapter extends RecyclerView.Adapter<MessageBoardListAdapter.MessageBoardHolder> {
    private Context context;
    private ArrayList<MessageBoard> messageBoards;
    private ArrayList<Bitmap> bitmaps;
    private View.OnClickListener listenerFav;
    private View.OnClickListener listenerReport;
    private View.OnClickListener listenerComment;
    private View.OnClickListener listenerPerfilUser;
    private View.OnClickListener listenerDetallesMensaje;
    private View.OnClickListener listenerEditarMensaje;
    private View.OnLongClickListener listenerLongClickDelete;


    public MessageBoardListAdapter(Context context, ArrayList<MessageBoard> messageBoards, ArrayList<Bitmap> bitmaps){
        this.context = context;
        this.messageBoards = messageBoards;
        this.bitmaps = bitmaps;
    }

    @NonNull
    @Override
    public MessageBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.messageboard_item, parent, false);
        view.setOnClickListener(listenerDetallesMensaje);
        view.setOnLongClickListener(listenerLongClickDelete);

        MessageBoardHolder messageBoardHolder = new MessageBoardHolder(view);
        return messageBoardHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MessageBoardHolder holder, int position) {
        MessageBoard messageBoard = messageBoards.get(position);

//        holder.imgPerfil.setImageBitmap(bitmaps.get(position));
        if (!TextUtils.isEmpty(messageBoard.getPicture())){
            Picasso.get()
                    .load(messageBoard.getPicture())
                    .fit()
                    .centerInside()
                    .into(holder.imgPerfil, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (holder.progressBar != null) {
                                holder.progressBar.setVisibility(View.GONE);
                            }
                        }
                        @Override
                        public void onError(Exception e) {
                            if (holder.progressBar != null) {
                                holder.progressBar.setVisibility(View.GONE);
                            }
                        }

                    });
        }
        else{
            if (holder.progressBar != null) {
                holder.progressBar.setVisibility(View.GONE);
            }
        }

//        if (messageBoard.getPicture64() != null){
//            holder.imgPerfil.setImageBitmap(CommonUtils.base64ToBitmap(messageBoard.getPicture64())); // TODO
//        }


//        holder.imgPerfil.setTag(messageBoard.getUser_nick());
//        holder.imgPerfil.setOnClickListener(listenerPerfilUser);
        holder.tvNick.setTag(messageBoard.getUser_nick());
        holder.tvNick.setOnClickListener(listenerPerfilUser); // Todo. Llevará al perfil del usuario que lo escribió


        holder.tvNick.setText(messageBoard.getUser_nick());

        holder.tvCategoria.setText(messageBoard.getCategory());

        holder.imgComment.setOnClickListener(listenerComment);
        holder.imgComment.setTag(getItem(position));

        holder.imgFav.setOnClickListener(listenerFav);

        holder.imgReport.setOnClickListener(listenerReport);

        holder.tvNumFavs.setText("" + 0); // Todo. Esto viene de la BD
        holder.tvNumComments.setText("" + 0);

        holder.tvMensaje.setText(messageBoard.getTitle() + "\n\n" + messageBoard.getMessage());

        // TODO minutos desde que se publicó     holder.tvHora.setText("" + (int)((System.currentTimeMillis() - messageBoard.getTime())/0.000000277778)); // System.currentTimeMillis() / 0.000000277778);


        // TODO. Editar visible si es de este usuario
        if (CommonUtils.messageBoardIsFromThisUser(context, messageBoard)){
            holder.btEditMessageBoard.setVisibility(View.VISIBLE);
            holder.btEditMessageBoard.setOnClickListener(listenerEditarMensaje); // TODO. Irá a editar
            holder.btEditMessageBoard.setTag(messageBoard);
        }
    }

    @Override
    public int getItemCount() {
        return messageBoards.size();
    }


    public class MessageBoardHolder extends RecyclerView.ViewHolder{
        ProgressBar progressBar;
        TextView tvNick;
        ImageView imgPerfil;
        TextView tvCategoria;
        ImageView imgComment;
        ImageView imgFav;
        ImageView imgReport;
        TextView tvNumFavs;
        TextView tvNumComments;
        TextView tvMensaje;
        TextView tvHora;
        ImageView btEditMessageBoard;


        public MessageBoardHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
            tvNick = itemView.findViewById(R.id.tvNick);
            imgPerfil = itemView.findViewById(R.id.imgPerfil);
            tvCategoria = itemView.findViewById(R.id.tvCategoria);
            imgComment = itemView.findViewById(R.id.imgComment);
            imgFav = itemView.findViewById(R.id.imgFav);
            imgReport = itemView.findViewById(R.id.imgReport);
            tvNumFavs = itemView.findViewById(R.id.tvNumFavs);
            tvNumComments = itemView.findViewById(R.id.tvNumComments);
            tvMensaje = itemView.findViewById(R.id.tvMensaje);
            tvHora = itemView.findViewById(R.id.tvHora);
            btEditMessageBoard = itemView.findViewById(R.id.btEditMessageBoard);
        }
    }

    public MessageBoard getItem(int position){
        MessageBoard messageBoard = messageBoards.get(position);
//        if (CommonUtils.bitmapAByteArray(bitmaps.get(position)) != null)
//            messageBoard.setImageBytes(CommonUtils.bitmapAByteArray(bitmaps.get(position)));
        return messageBoard;
    }

    /**
     * set de los Listeners
     */
    public void setListenerComment(View.OnClickListener listenerComment) {
        this.listenerComment = listenerComment;
    }

    public void setListenerFav(View.OnClickListener listenerFav) {
        this.listenerFav = listenerFav;
    }

    public void setListenerReport(View.OnClickListener listenerReport) {
        this.listenerReport = listenerReport;
    }

    public void setListenerPerfilUser(View.OnClickListener listenerPerfilUser) {
        this.listenerPerfilUser = listenerPerfilUser;
    }

    public void setListenerDetallesMensaje(View.OnClickListener listenerDetallesMensaje) {
        this.listenerDetallesMensaje = listenerDetallesMensaje;
    }

    public void setListenerEditarMensaje(View.OnClickListener listenerEditarMensaje){
        this.listenerEditarMensaje = listenerEditarMensaje;
    }

    public void setListenerLongClickDelete(View.OnLongClickListener listenerLongClickDelete) {
        this.listenerLongClickDelete = listenerLongClickDelete;
    }



//    public interface Listeners extends View.OnClickListener, View.OnLongClickListener {
//        @Override
//        boolean onLongClick(View v);
//
//        @Override
//        void onClick(View v);
//    }


    /**
     * set de las listas
     */
    public void setMessageBoards(ArrayList<MessageBoard> messageBoards) {
        this.messageBoards = messageBoards;
    }

    public void setBitmaps(ArrayList<Bitmap> bitmaps) {
        this.bitmaps = bitmaps;
    }

    public void clear(){
        this.messageBoards.clear();
        this.bitmaps.clear();
    }

    public void addMessageBoards(ArrayList<MessageBoard> messageBoards) {
        this.messageBoards.addAll(messageBoards);
    }

    public void addBitmaps(ArrayList<Bitmap> bitmaps) {
        this.bitmaps.addAll(bitmaps);
    }


}
