package com.example.kasane.animens.data.db.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Clase OpenHelper (para sqlite)
 * @author Marina Espinosa
 */
public class AnimensOpenHelper extends SQLiteOpenHelper {
    private AtomicInteger atomicInteger = new AtomicInteger(0);
    private static AnimensOpenHelper animensOpenHelper;
    private SQLiteDatabase sqLiteDatabase;

    private AnimensOpenHelper() {
        super(AnimensApplication.getContext(), AnimensContract.DATABASE_NAME, null, AnimensContract.VERSION);
    }

    public static AnimensOpenHelper getInstance(){
        if (animensOpenHelper == null)
            animensOpenHelper = new AnimensOpenHelper();
        return animensOpenHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            // transaction

            db.execSQL(AnimensContract.AnimeEntry.SQL_CREATE_ENTRIES);
            db.execSQL(AnimensContract.Anime_Genres_Entry.SQL_CREATE_ENTRIES);
            db.execSQL(AnimensContract.GenresEntry.SQL_CREATE_ENTRIES);
        }
        catch (SQLiteException e){
            Log.d("AnimensDB", e.getMessage());
            Log.d("AnimensDB", AnimensContract.AnimeEntry.SQL_CREATE_ENTRIES);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try{
            // transaction

            // Borro
            db.execSQL(AnimensContract.AnimeEntry.SQL_DELETE_ENTRIES);
            db.execSQL(AnimensContract.Anime_Genres_Entry.SQL_DELETE_ENTRIES);
            db.execSQL(AnimensContract.GenresEntry.SQL_DELETE_ENTRIES);

            // Creo de nuevo
            onCreate(db);
        }
        catch (SQLiteException e){
            Log.d("AnimensDB", e.getMessage());

        }
    }

    public synchronized SQLiteDatabase openDatabase(){
        if (atomicInteger.incrementAndGet() == 1){
            sqLiteDatabase = getWritableDatabase(); // Se llama a onOpen()
        }
        return sqLiteDatabase;
    }

    public synchronized void closeDatabase(){
        if (atomicInteger.decrementAndGet() == 0)
            sqLiteDatabase.close();
    }


    // Método callback que se llama siempre que se abre la BD ya sea en modo lectura o escritura
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()){
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }


}
