package com.example.kasane.animens.ui.Notifications;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.Pofile.ProfileFragment;
import com.example.kasane.animens.ui.prefs.MyPreferenceFragment;

/**
 * Activity que gestiona las notificaciones (peticiones de amistad). Carga los fragment
 * @author Marina Espinosa
 */
public class NotificationsActivity extends AppCompatActivity implements NotificationsFragment.GoProfile{
    Toolbar toolbar;
    Fragment fragmentProfile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        savedInstanceState = new Bundle();
        savedInstanceState.putBoolean("FromNotifications", true);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notifications);

        toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.notifications);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        getSupportFragmentManager().beginTransaction().add(R.id.notifications_content, new NotificationsFragment()).commit();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void goToUserProfile(Profile profile) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("Profile", profile);
        startActivity(intent);
    }
}
