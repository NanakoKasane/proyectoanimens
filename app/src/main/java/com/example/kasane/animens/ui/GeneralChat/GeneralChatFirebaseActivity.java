package com.example.kasane.animens.ui.GeneralChat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.prefs.MyPreferenceFragment;

/**
 * Activity que gestiona los Mensajes del chat general. Carga los diferentes fragment
 * @author Marina Espinosa
 */
public class GeneralChatFirebaseActivity extends BaseActivity {
//    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.generalchat);
        getSupportFragmentManager().beginTransaction().add(R.id.mcontent, new GeneralChatFirebaseFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                super.onBackPressed();
        }
    }
}
