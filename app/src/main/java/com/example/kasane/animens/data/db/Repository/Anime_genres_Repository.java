package com.example.kasane.animens.data.db.Repository;

import com.example.kasane.animens.data.db.DAO.Anime_genres_DAO;
import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Anime_genresView;

import java.util.ArrayList;

/**
 * Repository para sqlite
 * @author Marina Espinosa
 */
public class Anime_genres_Repository {
    private static Anime_genres_Repository anime_genres_repository;
    private Anime_genres_DAO dao;

    private Anime_genres_Repository(){
        dao = new Anime_genres_DAO();
    }

    public static Anime_genres_Repository getInstance() {
        if (anime_genres_repository == null)
            anime_genres_repository = new Anime_genres_Repository();
        return anime_genres_repository;
    }

//    public void insertAnimes(ArrayList<Anime> animes){
//        dao.insertAnimes(animes);
//    }

    public int getAnime_GenresCount(){
        return dao.getAnime_GenresCount();
    }

    public boolean insertAnime_genres(ArrayList<Anime_genres> anime_genres){
        return dao.insertAnime_genres(anime_genres);
    }

    public ArrayList<Anime_genresView> getGenresAndCount() {
        return dao.getGenresAndCount();
    }


}
