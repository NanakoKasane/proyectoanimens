package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.RestClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;


/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class ProfileEditInteractor {
    public void editProfile(final String nombre, final String genero, final String fechaNac, final String descripcion, String image, final ProfileEditInteractor.EditProfile listener, Context context, final Bitmap bitmap) {
        String userNick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        // TODO. Consulta que lo edita


//        Date fecha = CommonUtils.stringToDate(fechaNac);
//        if (fecha != null)
//            profile.setBirthdate(profile.getBirthdate()); // CommonUtils.stringToDate(fechaNac));



        // EDITO
//        Profile profile = ProfileRepository.getProfileRepository().getProfile();
//        profile.setGender(genero);
//        profile.setDescription(descripcion);
//        profile.setPicture(image);



        // TODO. SUBO IMAGEN AL SERVER:
        if (bitmap != null){
//            uploadImage(bitmap, context);

            String url = AnimensApplication.BASE_URL+"animens/imagenes/upload.php"; //"http://marina.alumno.mobi/imagenes/upload.php";

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String Response = jsonObject.getString("response");
//                    Toast.makeText(getContext(), "La imagen está en: " + "http://marina.alumno.mobi/imagenes/" + Response, Toast.LENGTH_LONG).show();

                        String imagen = AnimensApplication.BASE_URL+"animens/imagenes/" + Response; //"http://marina.alumno.mobi/imagenes/" + Response;
                        Log.d("SubirFoto", imagen);

                        // TODO. UPDATE A PERFIL, LE AÑADO ESTA IMAGEN SUBIDA:
//                        Profile profile = ProfileRepository.getProfileRepository().getProfile();
//                        profile.setPicture(imagen);
//
                        String generoN = genero;
                        String fechaNacN = fechaNac;
                        if (!TextUtils.isEmpty(fechaNac))
                            fechaNacN = CommonUtils.formatDateReverse(fechaNac);
                        String descripcionN = descripcion;

                        if (genero == null)
                            generoN = "";
                        if (fechaNacN == null)
                            fechaNacN = "";
                        if (descripcion == null)
                            descripcionN = "";

                        String urlEditar = AnimensApplication.BASE_URL+"animens/editProfile.php?nick=" + userNick + "&image=" + imagen + "&description=" + descripcionN
                                + "&genre=" + generoN + "&fechanacimiento=" + fechaNacN + "&nombre=" + nombre;

                        RestClient.get(urlEditar, new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                try {
                                    if (!response.getBoolean("error")){
                                        listener.onEdited();
                                    }
                                    else{
                                        Log.d("AnimensPHP", "JSON. Error a true ($statement es false)");
                                        listener.onServerError();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("AnimensPHP", "JSON. Entra por el catch, no existe boolean error?");
                                    listener.onServerError();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                super.onFailure(statusCode, headers, responseString, throwable);
                                Log.d("AnimensPHP", responseString + "\n" + throwable.getMessage());
                                listener.onServerError();
                            }

                        });




                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.d("SubirFoto", "Catch de subir foto 1 "+ e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), "error: " + error.toString(), Toast.LENGTH_LONG).show();
                    listener.onPhotoTooBig(); // TODO. foto demasiado grande ?
                    Log.d("SubirFoto", "On error response de Volley: "+ error.getMessage());
                }
            })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    String gambar = CommonUtils.imagetoString(bitmap);
                    params.put("foto", gambar);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }



        // TODO. Edito sin imagen:
        else{
            // Si no hay nada que editar no edito
            if (genero == null && fechaNac == null && descripcion == null){
                listener.onEdited();
            }
            else{
                String generoN = genero;
                String fechaNacN = fechaNac;
                if (!TextUtils.isEmpty(fechaNac))
                    fechaNacN = CommonUtils.formatDateReverse(fechaNac);
                String descripcionN = descripcion;

                if (genero == null)
                    generoN = "";
                if (fechaNacN == null)
                    fechaNacN = "";
                if (descripcion == null)
                    descripcionN = "";

                    String url = AnimensApplication.BASE_URL+"animens/editProfile.php?nick=" + userNick + "&description=" + descripcionN
                            + "&genre=" + generoN + "&fechanacimiento=" + fechaNacN + "&nombre=" + nombre;

                    RestClient.get(url, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                if (!response.getBoolean("error")){
                                    listener.onEdited();
                                }
                                else{
                                    Log.d("AnimensPHP", "JSON. Error a true ($statement es false)");
                                    Log.d("AnimensPHP", response.getString("query"));
                                    listener.onServerError();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("AnimensPHP", "JSON. Entra por el catch, no existe boolean error?");
                                listener.onServerError();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                            Log.d("AnimensPHP", responseString + "\n" + throwable.getMessage());
                            listener.onServerError();
                        }

                    });


            }



//            listener.onEdited();
        }


    }





//    private void uploadImage(final Bitmap bitmap, Context context){
//        String url = "http://marina.alumno.mobi/imagenes/upload.php";
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    String Response = jsonObject.getString("response");
////                    Toast.makeText(getContext(), "La imagen está en: " + "http://marina.alumno.mobi/imagenes/" + Response, Toast.LENGTH_LONG).show();
//
//                     String imagen = "http://marina.alumno.mobi/imagenes/" + Response;
//
//
//                     // TODO. UPDATE A PERFIL, LE AÑADO ESTA IMAGEN SUBIDA:
//
//                    Profile profile = ProfileRepository.getProfileRepository().getProfile();
//                    profile.setPicture(imagen);
//
//
//
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                Toast.makeText(getContext(), "error: " + error.toString(), Toast.LENGTH_LONG).show();
//            }
//        })
//        {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                String gambar = CommonUtils.imagetoString(bitmap);
//                params.put("foto", gambar);
//                return params;
//            }
//        };
//        RequestQueue requestQueue = Volley.newRequestQueue(context);
//        requestQueue.add(stringRequest);
//
//    }


    interface EditProfile{
        void onEdited();
        void onServerError();
        void onPhotoTooBig();
    }

}
