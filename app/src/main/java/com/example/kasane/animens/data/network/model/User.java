package com.example.kasane.animens.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Clase modelo que define la entidad User
 * @author Marina Espinosa
 */
public class User implements Serializable {//, Comparable<User>
    private String user_id;
    private String nick; // varchar(15)
    private String passwordEncrypted;
    private String name;

    // foto por defecto
    private int photo; // int resPhoto


    private String picture; // TODO. una de 2.
//    private String picture64;


    public User(String nick, String name, int photo, String passwordEncrypted) {
        this.nick = nick;
        this.name = name;
        this.photo = photo;

        this.passwordEncrypted = passwordEncrypted;
    }

    public User(String nick, String passwordEncrypted){
        this.nick = nick;
        this.passwordEncrypted = passwordEncrypted;
    }

    public User(String nick, String name, String picture) {
        this.nick = nick;
        this.name = name;
        this.picture = picture;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPasswordEncrypted() {
        return passwordEncrypted;
    }

    public void setPasswordEncrypted(String passwordEncrypted) {
        this.passwordEncrypted = passwordEncrypted;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }


    /**
     * Comparando por id
     */
//    @Override
//    public int compareTo(@NonNull User o) {
//        return this.user_id.compareTo(o.getUser_id());
//    }


    /**
     * Otros criterios de comparación
     */
    // Comparo por nick
    public static class OrderByNick implements Comparator<User>{
        @Override
        public int compare(User o1, User o2) {
            return o1.getNick().compareTo(o2.getNick());
        }
    }
    // Comparo por nombre
    public static class OrderByName implements Comparator<User>{
        @Override
        public int compare(User o1, User o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
