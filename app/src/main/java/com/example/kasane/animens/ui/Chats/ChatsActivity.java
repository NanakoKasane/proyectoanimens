package com.example.kasane.animens.ui.Chats;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Chats;
import com.example.kasane.animens.ui.Base.BaseActivity;
import com.example.kasane.animens.ui.adapter.ChatsListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity que gestiona los Chats. Carga los diferentes fragment
 * @author Marina Espinosa
 */
public class ChatsActivity extends BaseActivity implements ChatsListFragment.GoChatRoom {
    Fragment chatsListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_chat);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.nav_chats);
        chatsListFragment = getSupportFragmentManager().findFragmentByTag(ChatsListFragment.TAG);

        if (chatsListFragment == null){
            chatsListFragment = new ChatsListFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.mcontent, chatsListFragment, ChatsListFragment.TAG);
            fragmentTransaction.commit();
        }

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0){
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                CommonUtils.showDialogLogOut(this);
        }
        else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                super.onBackPressed();
        }
    }


    @Override
    public void loadChatRoom(Chatroom chatroom) {
        Intent intent = new Intent(this, ConversationFirebaseActivity.class);
        intent.putExtra("chatroom", chatroom);
        startActivity(intent);
    }
}
