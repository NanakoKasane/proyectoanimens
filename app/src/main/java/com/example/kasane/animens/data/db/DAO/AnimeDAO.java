package com.example.kasane.animens.data.db.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.os.Debug;
import android.util.Log;

import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;

/**
 * DAO de Anime (para sqlite)
 * @author Marina Espinosa
 */
public class AnimeDAO {

    /**
     * Insert con transacciones
     */
    public boolean insertAnimes(ArrayList<Anime> animes){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        sqLiteDatabase.beginTransaction();

        String sql = "INSERT INTO " + AnimensContract.AnimeEntry.TABLE_NAME + " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        SQLiteStatement stmt = sqLiteDatabase.compileStatement(sql);
        for (int i = 0; i  < animes.size(); i++) {
            stmt.bindString(1, animes.get(i).getAnime_id());
            stmt.bindString(2, animes.get(i).getName());
            stmt.bindString(3, animes.get(i).getGenre());
            stmt.bindString(4, animes.get(i).getType());
            stmt.bindString(5, animes.get(i).getEpisodes());
            stmt.bindDouble(6, animes.get(i).getRating());
            stmt.bindString(7, animes.get(i).getPicture());
            stmt.bindLong(8, animes.get(i).getAnime_idIncrement());

            stmt.execute();
            stmt.clearBindings();
        }

        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();

        AnimensOpenHelper.getInstance().closeDatabase();
        return true;
    }

    // Obtengo el número de Animes que hay insertados
    public int getAnimeCount(){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
        long count = DatabaseUtils.queryNumEntries(sqLiteDatabase, AnimensContract.AnimeEntry.TABLE_NAME);

        AnimensOpenHelper.getInstance().closeDatabase();
        return (int) count;
    }


    // Obtengo la lista de Animes
    public ArrayList<Anime> getAnimeList(){
        ArrayList<Anime> animes = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
//        sqLiteDatabase.beginTransaction();

        Cursor cursor = sqLiteDatabase.query(AnimensContract.AnimeEntry.TABLE_NAME, AnimensContract.AnimeEntry.ALL_COLUMN, null, null, null, null, AnimensContract.AnimeEntry.DEFAULT_SORT + " desc", null);
        if (cursor.moveToFirst()){
            do{
                Anime anime = new Anime(
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_ANIME_ID),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_NAME),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_GENRE),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_TYPE),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_EPISODES),
                        cursor.getFloat(AnimensContract.AnimeEntry.INDEX_COLUMN_RATING),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_PICTURE),
                        cursor.getInt(AnimensContract.AnimeEntry.INDEX_COLUMN_ANIME_ID_INCREMENT)
                );
                animes.add(anime);
            }while (cursor.moveToNext());
        }
//        sqLiteDatabase.setTransactionSuccessful();
//        sqLiteDatabase.endTransaction();
        AnimensOpenHelper.getInstance().closeDatabase();
        return animes;
    }


    // Obtengo los animes de un GÉNERO determinado
    public ArrayList<Anime> getAnimesOfThisGenre(String genre){
        ArrayList<Anime> animes = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        // TODO.  select * from anime where anime_id in (select anime_id from anime_genres where genre = 'Drama');
        String selection = AnimensContract.AnimeEntry.COLUMN_ANIME_ID + String.format(" in (select %s from %s where %s = ?)",
                AnimensContract.AnimeEntry.COLUMN_ANIME_ID, AnimensContract.Anime_Genres_Entry.TABLE_NAME, AnimensContract.Anime_Genres_Entry.COLUMN_GENRE);
        String[] selectionArgs = {genre};
        Log.d("AnimensDB", selection);

        Cursor cursor = sqLiteDatabase.query(AnimensContract.AnimeEntry.TABLE_NAME, AnimensContract.AnimeEntry.ALL_COLUMN, selection, selectionArgs, null, null, AnimensContract.AnimeEntry.DEFAULT_SORT + " desc", null);
        if (cursor.moveToFirst()){
            do{
                Anime anime = new Anime(
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_ANIME_ID),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_NAME),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_GENRE),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_TYPE),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_EPISODES),
                        cursor.getFloat(AnimensContract.AnimeEntry.INDEX_COLUMN_RATING),
                        cursor.getString(AnimensContract.AnimeEntry.INDEX_COLUMN_PICTURE),
                        cursor.getInt(AnimensContract.AnimeEntry.INDEX_COLUMN_ANIME_ID_INCREMENT)
                );
                animes.add(anime);
            }while (cursor.moveToNext());
        }


        AnimensOpenHelper.getInstance().closeDatabase();
        return animes;
    }

    /**
     * Obtengo el último ID
     */
    private int getLastAnimeId(){
        // TODO. Query para obtener el ID maximo de anime y que sea el id a insertar
        // TODO. GetLastID() -> select max(anime_id) from anime;

        String last_anime_id = "";
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        try {
            String[] select = {"max(anime_id)"};
            Cursor cursor = sqLiteDatabase.query(AnimensContract.AnimeEntry.TABLE_NAME, select, null, null, null, null, null);
            if (cursor.moveToFirst()) { // Solo devuelve una fila, así que no es necesario un bucle
                last_anime_id = cursor.getString(0); // Solo devuelve una columna
            }

//            new_anime_id = Integer.parseInt(last_anime_id) + 1;
            cursor.close();
        }
        catch (SQLiteException e){
            Log.d("AnimensDB", e.getMessage());
        }

        AnimensOpenHelper.getInstance().closeDatabase();
        return Integer.parseInt(last_anime_id);
    }

    /**
     * Obtengo el último anime_idIncrement
     */
    private int getLastAnimeIdIncrement(){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
        int animeIdIncrement = 0;

        try {
            String[] select = {"max(anime_idIncrement)"};
            Cursor cursor = sqLiteDatabase.query(AnimensContract.AnimeEntry.TABLE_NAME, select, null, null, null, null, null);
            if (cursor.moveToFirst()) { // Solo devuelve una fila, así que no es necesario un bucle
                animeIdIncrement = cursor.getInt(0); // Devuelve una columna
            }
        }
        catch (SQLiteException e){
            Log.d("AnimensDB", e.getMessage());
        }
        AnimensOpenHelper.getInstance().closeDatabase();

        return animeIdIncrement;
    }

    /**
     * Inserto un Anime en la base de datos.
     */
    public void insertAnime(Anime anime){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        int new_anime_id = getLastAnimeId() + 1;
        int animeIdIncrement = getLastAnimeIdIncrement() + 1;


        // TODO. Insert
        ContentValues contentValues = new ContentValues();
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_NAME, anime.getName());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_ANIME_ID, new_anime_id);
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_EPISODES, anime.getEpisodes());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_GENRE, anime.getGenre());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_TYPE, anime.getType());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_PICTURE, anime.getPicture());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_ANIME_ID_INCREMENT, (animeIdIncrement + 1));

        sqLiteDatabase.insert(AnimensContract.AnimeEntry.TABLE_NAME, null, contentValues);
        try {
            sqLiteDatabase.insertOrThrow(AnimensContract.AnimeEntry.TABLE_NAME, null, contentValues);
        }
        catch (SQLiteException e){
            Log.d("AnimensDB", "Error al insertar: " + e.getMessage() );
        }

        AnimensOpenHelper.getInstance().closeDatabase();
    }

    /**
     * Actualizo un Anime de la base de datos
     */
    public void updateAnime(Anime anime){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_EPISODES, anime.getEpisodes());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_GENRE, anime.getGenre());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_TYPE, anime.getType());
        contentValues.put(AnimensContract.AnimeEntry.COLUMN_PICTURE, anime.getPicture());

        sqLiteDatabase.update(AnimensContract.AnimeEntry.TABLE_NAME, contentValues, AnimensContract.AnimeEntry.COLUMN_ANIME_ID + " = ? ", new String[]{anime.getAnime_id()});

        AnimensOpenHelper.getInstance().closeDatabase();
    }


    /**
     * Borro anime de la BD con esa id
     */
    public void delete(String anime_id){
        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();

        // TODO. Debo borrar primero de la tabla anime_genres (por las claves ajenas), y luego ya de la tabla anime
        sqLiteDatabase.delete(AnimensContract.Anime_Genres_Entry.TABLE_NAME, AnimensContract.Anime_Genres_Entry.COLUMN_ANIME_ID + " = ? ", new String[]{anime_id});

        // Ahora borro de anime
        sqLiteDatabase.delete(AnimensContract.AnimeEntry.TABLE_NAME, AnimensContract.AnimeEntry.COLUMN_ANIME_ID + " = ?", new String[]{anime_id});

        AnimensOpenHelper.getInstance().closeDatabase();
    }


    // Esto al final lo haré en GenreDAO
//    public ArrayList<String> getGenresStringList(){
//        ArrayList<String> generos = new ArrayList<>();
//        SQLiteDatabase sqLiteDatabase = AnimensOpenHelper.getInstance().openDatabase();
////        sqLiteDatabase.query(AnimensContract.Genres.TABLE_NAME); // TODO. Tabla genres
//        AnimensOpenHelper.getInstance().closeDatabase();
//        return generos;
//    }





}
