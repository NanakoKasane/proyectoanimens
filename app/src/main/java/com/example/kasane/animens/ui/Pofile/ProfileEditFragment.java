package com.example.kasane.animens.ui.Pofile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Fragment que gestiona la edición del perfil
 * @author Marina Espinosa
 */
public class ProfileEditFragment extends BaseFragment implements ProfileEditContact.View {
    static final String TAG = "fragmentProfileEdit";
    ArrayList<String> generos = new ArrayList<>();

    ProfileEditPresenter presenter;

    Spinner spGenero;
    FloatingActionButton btEditarPerfil;

    EditText edDescription;
    EditText edFechaNac;
    EditText edNombre;

    private static final int SELECT_FILE = 1;
    ImageView imgCamera;
    TextView tvImagenTitle;
    CircleImageView imgPerfil;
    Bitmap bitmap;

    ProfileActivity profileActivity = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            profileActivity = (ProfileActivity) context;
        }
        catch (ClassCastException e){
//            Toast.makeText(context, "No se puede ATTACH", Toast.LENGTH_SHORT).show();
            Log.d("ProfileEditFragment", "can't attach");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_edit, container, false);
        return v;
    }

    public void inicializar(){
        if (getView() != null) {
            spGenero = getView().findViewById(R.id.spGenero);
            btEditarPerfil = getView().findViewById(R.id.btEditarPerfil);
            edDescription = getView().findViewById(R.id.edDescription);
            edFechaNac = getView().findViewById(R.id.edFechaNac);
            tvImagenTitle = getView().findViewById(R.id.tvImagenTitle);
            imgCamera = getView().findViewById(R.id.imgCamera);
            imgPerfil = getView().findViewById(R.id.imgPerfil);
            edNombre = getView().findViewById(R.id.edNombre);

            // Subir foto
            imgCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abrirGaleria(v);
                }
            });
            tvImagenTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abrirGaleria(v);
                }
            });

            imgPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    abrirGaleria(v);
                }
            });

            // Editar perfil
            btEditarPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter = new ProfileEditPresenter(ProfileEditFragment.this);

                    if (!CommonUtils.isNetworkOnline()) {
                        if (getActivity() != null)
                            CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                    } else {
                        showProgressBar(null, getString(R.string.msg_updating_profile), false, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                        presenter.editProfile(edNombre.getText().toString(), generos.get(spGenero.getSelectedItemPosition()), edFechaNac.getText().toString(), edDescription.getText().toString(),
                                null, getContext(), bitmap); // TODO. CommonUtils.bitmapToBase64(((BitmapDrawable)imgPerfil.getDrawable()).getBitmap())
                    }

                }
            });

            edFechaNac.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getContext() != null) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int ano, int mes, int dia) {
                                String date = "" + String.format("%02d", dia) + "/" + String.format("%02d", mes + 1) + "/" + String.format("%02d", ano);
                                edFechaNac.setText(date);
                            }
                        }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());

                        datePickerDialog.show();
                    }
                }
            });

        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inicializar();

        // TODO. Lo siguiente no irá aquí, es otra clase modelo
        generos.add("mujer");
        generos.add("hombre");
        if (getContext() != null)
            spGenero.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.spgenres, generos));


        // Obtengo los datos a editar
        Bundle bundle = getArguments();
        if (bundle != null) {
            Profile profile = (Profile) bundle.getSerializable("profile");
            if (profile != null) {
                edDescription.setText(profile.getDescription());
                edFechaNac.setText(CommonUtils.formatDate(profile.getBirthdate()));
                edNombre.setText(profile.getName());

//            edFechaNac.setText(profile.getBirthdate());

//            imgPerfil.setImageBitmap(CommonUtils.base64ToBitmap(profile.getPicture64()));
                if (profile.getPicture() != null) {
                    Picasso.get()
                            .load(profile.getPicture())
                            .fit()
                            .centerInside()
                            .into(imgPerfil, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError(Exception e) {
                                }
                            });

                }

                if (generos.size() > 0) {
                    if (generos.get(0).equals(profile.getGender())) // Mujer
                        spGenero.setSelection(0);
                    else
                        spGenero.setSelection(1); // Hombre
                }
            }
        }

    }


    @Override
    public void onEdited() {
        hideProgressBar();

        if (getActivity() != null && getView() != null)
            CommonUtils.hideKeyboard(getActivity(), getView());

        Toast.makeText(AnimensApplication.getContext(), "Datos editados", Toast.LENGTH_SHORT).show();

        if (getFragmentManager() != null)
            getFragmentManager().popBackStack();

        // TODO. YA SE RESFRESCA LUEGO LA IMAGEN

//        Bundle bundle = new Bundle();
//        bundle.putBoolean("editado", true);
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.mcontent, ProfileFragment.newInstance(bundle)).commit();

    }

    @Override
    public void onServerError() {
//        CommonUtils.showMessage(getActivity(), "Error en el servidor");
//        Snackbar.make(getActivity().findViewById(R.id.mcontent), "Error en el servidor", Snackbar.LENGTH_LONG).show();

        hideProgressBar();

        if (getActivity() != null){
//            Toast.makeText(AnimensApplication.getContext(), "Error en el servidor", Toast.LENGTH_SHORT).show();
            CommonUtils.showMessage(getActivity(), "Error en el servidor");
        }

    }

    @Override
    public void onPhotoTooBig() {
        hideProgressBar();

        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), "El tamaño máximo permitido para la imagen son 400kB. Por favor, suba otra imagen.");
    }


    /**
     * Subir foto
     */
    public void abrirGaleria(View v){
        Intent intent = new Intent();
        intent.setType("image/*");

        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"), SELECT_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        Uri selectedImageUri = null;
        Uri selectedImage;

        String filePath = null;
        switch (requestCode) {
            case SELECT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    selectedImage = imageReturnedIntent.getData();

                    try {
                        String selectedPath = selectedImage.getPath();
                        if (requestCode == SELECT_FILE) {

                            if (selectedPath != null) {
                                InputStream imageStream = null;
                                try {
                                    imageStream = getContext().getContentResolver().openInputStream(
                                            selectedImage);
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }

                                // Transformamos la URI de la imagen a inputStream y este a un Bitmap
                                Bitmap bmp = BitmapFactory.decodeStream(imageStream);
                                bitmap = bmp;

                                // Ponemos nuestro bitmap en un ImageView que tengamos en la vista
                                imgPerfil.setImageBitmap(bmp);

                            }
                        }
                    }
                    catch (Exception e){}
                }
                break;
        }
    }















}
