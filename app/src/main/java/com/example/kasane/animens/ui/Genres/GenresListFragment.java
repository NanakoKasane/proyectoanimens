package com.example.kasane.animens.ui.Genres;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.db.model.Anime_genresView;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Anime_genres;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.adapter.GenreListAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Fragment que gestiona el listado de Géneros (de Animes)
 * @author Marina Espinosa
 */
public class GenresListFragment extends BaseFragment implements GenresListContract.View{
    public static String TAG = "GenresListFragment";
    RecyclerView rvGenresList;
    GenreListAdapter genreListAdapter;
    GenresListPresenter presenter;
    GetAnimes getAnimes;

    interface GetAnimes{
        void getAnimesOfThisGenre(String genre);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            getAnimes = (GetAnimes) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements GetAnimes");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_genres_list, container, false);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        rvGenresList = view.findViewById(R.id.rvGenresList);

        rvGenresList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        genreListAdapter = new GenreListAdapter(getContext(), new ArrayList<Anime_genresView>());

        genreListAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.getAnimesFromGenre(genreListAdapter.getItem(rvGenresList.getChildAdapterPosition(v)).getGenre());

            }
        });

        rvGenresList.setAdapter(genreListAdapter);

        presenter = new GenresListPresenter(this);

        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
        }
        else {
            showProgressBar(null, getString(R.string.msg_loading_genres), false, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                }
            });
            presenter.getGenreList();
        }
    }



    @Override
    public void showGenreList(ArrayList<Anime_genresView> genres) {
        hideProgressBar();
        Collections.sort(genres, new Anime_genresView.OrderByCount());
        genreListAdapter.setAnime_genres(genres);
        genreListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onServerError() {
        hideProgressBar();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_server);
        else
            Toast.makeText(AnimensApplication.getContext(), getString(R.string.err_server), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAnimesFromGenreObtained(ArrayList<Anime> animes, String genre) {
        // TODO. Cargar fragment de AnimeList con los datos que le pasemos como extra (EN LA ACTIVITY, SE ACCEDERÁ CON UN EVENTO)

        getAnimes.getAnimesOfThisGenre(genre);


        // TODO. Quitar esto
      //  Toast.makeText(getContext(), "Yendo a los Animes del género: " + genre, Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(GenresListActivity.this, AnimeActivity.class); // TODO. filtro. Solo se mostrarán los animes de ese género
//        intent.putExtra("genre", genreListAdapter.getItem(rvGenresList.getChildAdapterPosition(v)).getGenre()); // Le paso el género para el filtro
//        Toast.makeText(GenresListActivity.this, genreListAdapter.getItem(rvGenresList.getChildAdapterPosition(v)).getGenre(), Toast.LENGTH_SHORT).show();
//        startActivity(intent);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                genreListAdapter.filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                genreListAdapter.filter(newText);
                return false;
            }
        });
    }
}
