package com.example.kasane.animens.ui.Anime;

import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;

public class AnimeAddPresenter implements AnimeAddContract.Presenter, AnimeAddInteractor.ValidateAnime, AnimeAddInteractor.EditAnime, AnimeAddInteractor.GetGenresStringList {
    private AnimeAddContract.View view;
    private AnimeAddInteractor interactor;

    public AnimeAddPresenter(AnimeAddContract.View view){
        this.view = view;
        interactor = new AnimeAddInteractor();
    }


    // Presenter
    @Override
    public void validateAnime(String nombre, String url, String tipo, String numCaps) {
        interactor.validateAnime(nombre, url, tipo,numCaps, this);
    }
    @Override
    public void editAnime(Anime animeAEditar, Anime animeEditado) {
        interactor.editAnime(animeAEditar, animeEditado, this);

    }
    @Override
    public void getGenresStringList() {
        interactor.getGenresStringList(this);
    }


    // Interactor ValidateAnime
    @Override
    public void onSuccessfulValidation() {
        view.onValidated();
    }

    @Override
    public void onEmptyNameError() {
        view.showEmptyNameError();
    }

    @Override
    public void onEmptyEpisodesError() {
        view.showEmptyEpisodesError();
    }

    @Override
    public void onEmptyPictureError() {
        view.showEmptyPictureError();
    }

    @Override
    public void onInvalidEpisodesNumber() {
        view.showInvalidEpisodesNumber();
    }


    // Interactor EditAnime
    @Override
    public void onEdited(Anime animeEditado) {
        view.onEdited(animeEditado);
    }

    // Interactor GetGenresList
    @Override
    public void onGenresListObtained(ArrayList<String> genres) {
        view.showGenresStringList(genres);
    }
}
