package com.example.kasane.animens.ui.User;

import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.Utils.Count;
import com.example.kasane.animens.data.network.RestClient;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.data.network.repository.ProfileRepository;
import com.example.kasane.animens.data.network.repository.UserListRepository;
import com.example.kasane.animens.ui.Register.RegisterInteractorImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Clase interactor que realiza peticiones al servidor
 * @author Marina Espinosa
 */
public class UserListInteractor {

    public void getUserList(String email, final UserListInteractor.GetUserList listener){

        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());
        String url;
        if (nick != null)
             url = AnimensApplication.BASE_URL+"animens/getUserList.php?nick=" + nick;
        else
            url = AnimensApplication.BASE_URL+"animens/getUserList.php";

        try {
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<User> users = gson.fromJson(response.getJSONArray("users").toString(), new TypeToken<ArrayList<User>>(){}.getType());
                        listener.onUserListObtained(nick, email, users);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure"+ throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }



        // Por si falla que se vea algo:
//        listener.onUserListObtained(UserListRepository.getUserListRepository().getUsers());

    }

    public void getUserProfile(String nick, final UserListInteractor.GetUserProfile listener) {

        // TODO. Consulta a la BD de obtener el perfil de ese user
        try {
            String url = AnimensApplication.BASE_URL+"animens/getUserProfile.php?nick=" + nick;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<Profile> profiles = gson.fromJson(response.getJSONArray("profile").toString(), new TypeToken<ArrayList<Profile>>(){}.getType());
                        listener.onUserProfile(profiles.get(0));

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
//                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" +throwable.getMessage());
//                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
//            listener.onServerError();
        }





//        listener.onUserProfile(ProfileRepository.getProfileRepository().getProfile());
    }

    public void getFriendList(String nick, UserListInteractor.GetUserList listener) {

        // TODO. Consulta a la BD para obtener los amigos de ese user (nick)
        try {
            String url = AnimensApplication.BASE_URL+"animens/getFriendList.php?nick=" + nick;
            RestClient.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Gson gson = new Gson();
                        ArrayList<User> users = gson.fromJson(response.getJSONArray("users").toString(), new TypeToken<ArrayList<User>>(){}.getType());
                        if (users.size() == 0)
                            listener.onNoFriends();
                        else
                            listener.onUserListObtained(nick, null, users);

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("AnimensPHP", e.getMessage());
                        Log.d("AnimensPHP", response.toString());
                        listener.onServerError();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" +  throwable.getMessage());
                    listener.onServerError();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Log.d("AnimensPHP", "OnFailure" + throwable.getMessage());
                    listener.onServerError();
                }
            });
        }
        catch (Exception e) {
            Log.d("AnimensPHP", "Excepcion: "+ e.getMessage());
            listener.onServerError();
        }






        // prueba para obtener una supuesta lista de amigos:
//        ArrayList<User> users = UserListRepository.getUserListRepository().getUsers();
//        if (users.size() > 0)
//            users.remove(0);
//        listener.onUserListObtained(users);

    }

    public interface GetUserList{
        void onUserListObtained(String nick, String email, ArrayList<User> users);
        void onServerError();

        void onNoFriends();
    }

    public interface GetUserProfile{
        void onUserProfile(Profile profile);
    }
}
