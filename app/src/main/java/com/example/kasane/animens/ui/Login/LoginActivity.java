package com.example.kasane.animens.ui.Login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.PasswordReset.PasswordResetActivity;
import com.example.kasane.animens.ui.Register.RegisterActivity;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Login de la aplicación
 * @author Marina Espinosa Gálvez
 */
public class LoginActivity extends AppCompatActivity implements LoginContract.View{
    @BindView(R.id.tvRegisterHere) TextView tvRegisterHere;
    @BindView(R.id.edUsername) EditText edUsername;
    @BindView(R.id.edPassword) EditText edPassword;
    @BindView(R.id.cbRememberme) CheckBox cbRememberme;
    @BindView(R.id.btSignIn)
    Button btSignIn;
    private ProgressDialog progressDialog;
    TwitterAuthClient twitterAuthClient;

    LoginContract.Presenter presenter;
    private FirebaseAuth mAuth;
    Result<TwitterSession> result;
    private FirebaseUser user;
    private boolean isTwitter = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setTitle(getString(R.string.login));
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this);
        isTwitter = false;
        btSignIn.setEnabled(true);

        if (!CommonUtils.isNetworkOnline()){
            CommonUtils.showMessage(LoginActivity.this, R.string.msg_nointernet);
        }
        else {

            // TODO. Sacar información de las preferencias si la hubiera e iniciar sesión
            presenter.getCredentials(getApplicationContext()); // en el onLoadCredentials(user, pass) (quiere decir que se han conseguido obtener las credenciales), hacemos LOG IN (intent a Navigation Drawer)
            // en el onNotObtainedCredentials() hago lo de presenter.validateCredentials

            //  presenter.downloadData();
        }


        // Para el inicio de sesión de twitter
        mAuth = FirebaseAuth.getInstance();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(getString(R.string.TWITTER_KEY), getString(R.string.TWITTER_SECRET));

        TwitterConfig twitterConfig=new TwitterConfig.Builder(this)
                .twitterAuthConfig(authConfig)
                .build();
        Twitter.initialize(twitterConfig);

        user = mAuth.getCurrentUser();

    }


    /**
     * OnClick del TextView de Registrar
     */
    @OnClick(R.id.tvRegisterHere) public void register(View v) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    /**
     * OnClick del botón de Iniciar sesión. Lleva al menú con las opciones (que posteriormente será un Navigation Drawer
     */
    @OnClick(R.id.btSignIn) void iniciarSesion(){
        ocultarTeclado();
        if (!CommonUtils.isNetworkOnline()){
            CommonUtils.showMessage(LoginActivity.this, R.string.msg_nointernet);
        }
        else {
            showProgressBar(null, getString(R.string.msg_validatingCredentials));
            btSignIn.setEnabled(false);
            presenter.validateCredentials(edUsername.getText().toString(), edPassword.getText().toString());
        }
    }


    /**
     * Bajo el teclado cuando se hace click en el constraint en vez de los editText
     */
    @OnClick(R.id.constraintLayout) void ocultarTeclado(){
        try {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(edUsername.getWindowToken(), 0);
        }
        catch (Exception e){}
    }


    /**
     * Onclick del TextView Resetear contraseña
     */
    @OnClick(R.id.tvForgotPassword) void resetPassword(){
        Intent intent = new Intent(LoginActivity.this, PasswordResetActivity.class);
        startActivity(intent);
    }


    // LoginContract.View

    @Override
    public void sucessfulLogin(String email) {
        hideProgressBar();

        // TODO
//        presenter.getCurrentUser(LoginActivity.this);

        logIn(email);

    }

//    @Override
//    public void onCurrentUserObtained(User user) {
//        logIn(user);
//    }


    // TODO. LOGIN
    public void logIn(String email){
        Intent intent = new Intent(LoginActivity.this, UserListActivity.class); // TODO. SplashActivity.class
//        if (user != null)
//            intent.putExtra("User", user);

        // TODO. Inicio de sesión con correo y contraseña con firebase (ya que no es google ni twitter en este caso)


        // TODO. Inicio de sesión
        showProgressBar(null, getString(R.string.msg_logingin));
        if (email != null && !TextUtils.isEmpty(email) && edPassword.getText().toString() != null && !TextUtils.isEmpty(edPassword.getText().toString())) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, edPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        hideProgressBar();

                        try {
                            mAuth.getCurrentUser().reload();
                            user = mAuth.getCurrentUser();

                            if (user.isEmailVerified() || email.equals("mary@gmail.com")) { // email de pruebas // TODO. Quitar luego el || con el email de prueba

                                // TODO GUARDO EN LAS PREFERENCIAS EL USUARIO Y LA CONTRASEÑA, y luego voy al NavigationDrawer.
                                if (cbRememberme.isChecked()) {
                                    presenter.saveCredentials(edUsername.getText().toString(), email, edPassword.getText().toString(), getApplicationContext());
                                } else
                                    presenter.saveCredentials(edUsername.getText().toString(), email, "", getApplicationContext());

                                startActivity(intent);
                                LoginActivity.this.finish();

                            } else {
                                btSignIn.setEnabled(true);
                                CommonUtils.showMessage(LoginActivity.this, getString(R.string.err_unverifiedemail));
//                            Toast.makeText(LoginActivity.this, R.string.err_unverifiedemail, Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception e){}

                    } else {
                        hideProgressBar();
                        btSignIn.setEnabled(true);
                        CommonUtils.showMessage(LoginActivity.this, R.string.err_invalidpassoremail);
//                        Toast.makeText(LoginActivity.this, R.string.err_invalidpassoremail, Toast.LENGTH_SHORT).show(); // TODO. FirebaseAuthInvalidCredentialsException
                        Log.d("Firebase", String.valueOf(task.getException()));

                    }
                }
            });
        }
        else{ // TODO. puede que exista el usuario en la base de datos pero sea null el email !?
            hideProgressBar();
            btSignIn.setEnabled(true);
            CommonUtils.showMessage(LoginActivity.this, "No se ha podido iniciar sesión");
//            Toast.makeText(this, "No se ha podido iniciar sesión", Toast.LENGTH_SHORT).show();
        }


    }



    @Override
    public void showMessageError() {
        btSignIn.setEnabled(true);
        hideProgressBar();
        // Toast.makeText(this, getString(R.string.err_loginFailed), Toast.LENGTH_SHORT).show();
        CommonUtils.showMessage(this, R.string.err_loginFailed);
    }


    // Estaba guardado el usuario en las preferencias y lo cargamos y hacemos login directamente
    @Override
    public void onLoadCredentials(String user,String email, String password) {
        edUsername.setText(user);
        edPassword.setText(password);

//        presenter.getCurrentUser(LoginActivity.this);
//        logIn(null);
        logIn(email);
    }


    // No estaba guardado el usuario ni la contraseña en las preferencias
    @Override
    public void onNotObtainedCredentials() {
    }

    // Se guardan las credenciales. Por aquí solo pasa si se ha validado ya las credenciales y va a redirigir al navigation drawer
    @Override
    public void onSavedCredentials() {
//        Toast.makeText(this, "Guardado usuario con éxito", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServerError() {
        btSignIn.setEnabled(true);
        hideProgressBar();
        CommonUtils.showMessage(this, R.string.err_general_register);
    }


    public void hideProgressBar() {
        if (progressDialog != null)
            progressDialog.dismiss();
        progressDialog = null;
    }

    public void showProgressBar(String titulo, String mensaje) {
        if (progressDialog != null)
            return;
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(mensaje); // R.string.msg_loading
        if (titulo != null)
            progressDialog.setTitle(titulo); // R.string.app_name
        progressDialog.show();
    }


    /**
     * Inicio de sesión de Google (se implementa en LoginsFirebaseActivity
     */
    @OnClick(R.id.ivGoogle) void iniciarSesionGoogle(){
        isTwitter = false;

        Intent intent = new Intent(LoginActivity.this, LoginsFirebaseActivity.class);
        intent.putExtra(LoginsFirebaseActivity.OPTION, LoginsFirebaseActivity.GOOGLE_OPTION);
        startActivity(intent);
    }



    /**
     * INICIO DE SESIÓN DE TWITTER
     */
    @OnClick(R.id.ivTwitter) void iniciarSesionTwitter(){

        isTwitter = true;
        twitterAuthClient = new TwitterAuthClient();
//        endAuthorizeInProgress();

        try {
//            twitterAuthClient.cancelAuthorize();
        }
        catch (Exception e){
            Log.d("twitter", e.getMessage());
        }

        twitterAuthClient.authorize(LoginActivity.this, new Callback<TwitterSession>() {
            @Override
            public void success(final Result<TwitterSession> result) {
                final TwitterSession sessionData = result.data;
                //  the returned TwitterSession contains the user token and secret

//                Toast.makeText(LoginActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
                LoginActivity.this.result = result;
                handleTwitterSession(sessionData);

            }

            @Override
            public void failure(final TwitterException e) {
//                Toast.makeText(LoginActivity.this,  R.string.err_nologintwitter, Toast.LENGTH_SHORT).show();
                CommonUtils.showMessage(LoginActivity.this, R.string.err_nologintwitter);
            }
        });
    }

    private void endAuthorizeInProgress() {
        try {
            Field authStateField = twitterAuthClient.getClass().getDeclaredField("authState");
            authStateField.setAccessible(true);
            Object authState = authStateField.get(twitterAuthClient);
            Method endAuthorize = authState.getClass().getDeclaredMethod("endAuthorize");
            endAuthorize.invoke(authState);
        } catch (NoSuchFieldException | SecurityException | InvocationTargetException |
                NoSuchMethodException | IllegalAccessException e) {
            Log.d("twitter_end" + "Couldn't end authorize in progress.", e.getMessage());
        }
    }

    private void handleTwitterSession(TwitterSession session) {
        showProgressBar(null, getString(R.string.msg_logingin));
        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgressBar();

                        if (task.isSuccessful()) {
                            Log.d("twitter_login", "signInWithCredential:success");
                            user = mAuth.getCurrentUser();

                            getUserEmail(result);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d("twitter_login", "signInWithCredential:failure", task.getException());

                            if(task.getException() instanceof FirebaseAuthUserCollisionException) { //  FirebaseAuthUserCollisionException. An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address
                                CommonUtils.showMessage(LoginActivity.this,  getString(R.string.err_logincollision));
                            }
                            else
                                CommonUtils.showMessage(LoginActivity.this,  getString(R.string.err_general_twitter));
                        }

                    }

                });
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        // twitter only
        if (isTwitter)
            twitterAuthClient.onActivityResult(requestCode, responseCode, intent);
    }


    // obtener email de twitter
    private void getUserEmail(Result<TwitterSession> result) {
        TwitterSession session = result.data;
        //get user email
        new TwitterAuthClient().requestEmail(session, new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                String twitterEmail = result.data;
                Log.d("twitter_login", "twitterEmail: " + twitterEmail); // Para poder obtenerlo  hay que habilitarlo desde twitter develop y añadir URL de privacy policty y terms of use

                Intent intent = new Intent(LoginActivity.this, LoginsFirebaseActivity.class);
                intent.putExtra(LoginsFirebaseActivity.OPTION, LoginsFirebaseActivity.TWITTER_OPTION);
                // paso el email
                intent.putExtra("email", twitterEmail);
                startActivity(intent);
            }

            @Override
            public void failure(TwitterException e) {
                Log.d("twitter_login", "Cannot get Twitter email");
                CommonUtils.showMessage(LoginActivity.this,  getString(R.string.err_nologintwitter));

            }
        });
    }




}
