package com.example.kasane.animens.ui.MessageBoard;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.ui.adapter.ChatsListAdapter;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.repository.ChatsListRepository;
import com.squareup.picasso.Picasso;

public class MessageBoardDetailFragment extends Fragment {
    public static String TAG = "MessageBoardDetailFragment";

    Bitmap bitmap;
    MessageBoard messageBoard;

    TextView tvNick;
    ImageView imgPerfil;
    TextView tvCategoria;
    ImageView imgComment;
    ImageView imgFav;
    ImageView imgReport;
    TextView tvNumFavs;
    TextView tvNumComments;
    TextView tvMensaje;
    TextView tvHora;
    ProgressBar progressBar;

    RecyclerView rcvComments;

    public static MessageBoardDetailFragment newInstance(Bundle bundle){
        MessageBoardDetailFragment messageBoardDetailFragment = new MessageBoardDetailFragment();
        if (bundle != null){
            messageBoardDetailFragment.setArguments(bundle);
        }
        return messageBoardDetailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_board_detail, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        messageBoard = (MessageBoard) getArguments().getSerializable("MessageBoard");

//        byte[] byteArray = messageBoard.getImageBytes();
//        if (byteArray != null)
//            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);


        binding();

        if (!TextUtils.isEmpty(messageBoard.getPicture())){
            Picasso.get()
                    .load(messageBoard.getPicture())
                    .fit()
                    .centerInside()
                    .into(imgPerfil,  new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }

                    });
        }

//        if (messageBoard.getPicture64() != null){
////            imgPerfil.setImageBitmap(CommonUtils.base64ToBitmap(messageBoard.getPicture64()));
//        }

        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            }

        // TODO. Ejemplos de comentarios:
        rcvComments.setLayoutManager(new LinearLayoutManager(getContext()));
        // TODO. ESTO AL MVP
        ChatsListAdapter chatsListAdapter = new ChatsListAdapter(getContext());
        chatsListAdapter.setChats(ChatsListRepository.getChatsListRepository().getChats());
        rcvComments.setAdapter(chatsListAdapter);


        // TODO. EN PRESENTER -> GETCOMMENTS


    }



    private void binding(){
        progressBar = getView().findViewById(R.id.progressBar);
        rcvComments = getView().findViewById(R.id.rcvComments);
        tvNick = getView().findViewById(R.id.tvNick);
        imgPerfil = getView().findViewById(R.id.imgPerfil);
        tvCategoria = getView().findViewById(R.id.tvCategoria);
        imgComment = getView().findViewById(R.id.imgComment);
        imgFav = getView().findViewById(R.id.imgFav);
        imgReport = getView().findViewById(R.id.imgReport);
        tvNumFavs = getView().findViewById(R.id.tvNumFavs);
        tvNumComments = getView().findViewById(R.id.tvNumComments);
        tvMensaje = getView().findViewById(R.id.tvMensaje);
        tvHora = getView().findViewById(R.id.tvHora);

//        imgPerfil.setImageBitmap(bitmap);
        tvNick.setText(messageBoard.getUser_id());
        tvCategoria.setText(messageBoard.getCategory());
        tvNumFavs.setText("" + 0); // Todo. Esto viene de la BD
        tvNumComments.setText("" + 0);
        tvMensaje.setText(messageBoard.getTitle() + "\n\n" + messageBoard.getMessage());

        // TODO. Listeners:
        //imgFav.setOnClickListener(listenerFav);
        //imgPerfil.setOnClickListener(listenerPerfilUser); // Todo. Llevará al perfil del usuario que lo escribió
        //tvNick.setOnClickListener(listenerPerfilUser); // Todo. Llevará al perfil del usuario que lo escribió
        //imgReport.setOnClickListener(listenerReport);

    }



}
