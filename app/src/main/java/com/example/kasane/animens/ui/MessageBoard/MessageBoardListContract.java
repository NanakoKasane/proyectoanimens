package com.example.kasane.animens.ui.MessageBoard;

import android.graphics.Bitmap;

import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.data.network.model.Profile;

import java.util.ArrayList;

public interface MessageBoardListContract {
    interface View{
        void showMessages(ArrayList<MessageBoard> messageBoards, ArrayList<Bitmap> bitmaps);

        void showProgressBar();
        void hideProgressBar();

        void onMessageDeleted();

        void onUserProfile(Profile profile);
    }

    interface Presenter{
        void getMessages();

        void deleteMessage(MessageBoard messageBoard);

        void getUserProfile(String nick);
    }
}
