package com.example.kasane.animens.ui.Pofile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
// import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.Chats.ConversationFirebaseActivity;
import com.example.kasane.animens.ui.User.UserListPresenter;
import com.example.kasane.animens.ui.prefs.PreferencesActivity;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Fragment que gestiona el perfil
 * @author Marina Espinosa
 */
public class ProfileFragment extends BaseFragment implements ProfileContact.View{
    public static final String TAG = "fragmentProfile";

    CircleImageView profile_image;
    TextView tvProfileNombre;
    TextView tvDireccion;
    TextView tvDescripcion;
    TextView tvFechaNac;
    TextView tvFechaRegistro;
    TextView tvGenero;
//    TabLayout tabLayout;

    FloatingActionMenu floatingActionMenu;
    FloatingActionButton btEditarPerfil;
    FloatingActionButton btAnadirAmigo;
    FloatingActionButton btIniciarChat;
    ProgressBar progressBar;
    Button btAmigos;
    Button btAnimes;

    ProfilePresenter presenter;
    Profile profile;

    Listener listener;
    Menu menu;
    Bitmap imageBitmap;

    public interface Listener{
//        void loadFriends(ArrayList<User> users);
//        void loadAnimes(ArrayList<Anime> animes);
        void getFriends(String nick);
        void getAnimes(String nick);

        void onProfileEdit(Profile profile); // TODO objeto profile
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if (getArguments() == null) {
//            try {
//                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Perfil");
//            } catch (Exception e) {
//                Log.d("Marina", e.getMessage());
//            }
//        }
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.friend_menu, menu);
        this.menu = menu;

        menu.findItem(R.id.itmenu_delete).setVisible(false);
        menu.findItem(R.id.itmenu_add).setVisible(false);
        menu.findItem(R.id.itmenu_Preferences).setVisible(false);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itmenu_Preferences:
                Intent intent = new Intent(getContext(), PreferencesActivity.class);
                startActivity(intent);
                break;

            case R.id.itmenu_add:
                Toast.makeText(AnimensApplication.getContext(), "Enviando petición de amistad...", Toast.LENGTH_SHORT).show();
                presenter.sendFriendRequest(CommonUtils.getCurrentUserNick(AnimensApplication.getContext()), profile.getNick());
                break;

            case R.id.itmenu_delete:
                Toast.makeText(AnimensApplication.getContext(), "Borrando amigo...", Toast.LENGTH_SHORT).show();
                presenter.deleteFriend(profile.getNick());
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFriendListObtained(ArrayList<User> friends) {
        boolean esAmigo = false;
        for (User u : friends){
            if (profile.getNick().equals(u.getNick())) {
                // TODO. Es tu amigo si entra por aquí
                esAmigo = true;
            }
        }

        if (esAmigo)
            menu.findItem(R.id.itmenu_delete).setVisible(true); // TODO. posibilidad de borrar amigo

    }

    @Override
    public void onFriendDeleted(String friendNick) {
        Toast.makeText(AnimensApplication.getContext(), "Se ha borrado al usuario " +friendNick + " de su lista de amigos", Toast.LENGTH_SHORT).show();
        menu.findItem(R.id.itmenu_delete).setVisible(false);
        menu.findItem(R.id.itmenu_add).setVisible(true);
        btAnadirAmigo.setVisibility(View.VISIBLE);

    }

    @Override
    public void onChatroomObtained(Chatroom chatroom) {
        Intent intent = new Intent(getContext(), ConversationFirebaseActivity.class);
        intent.putExtra("chatroom", chatroom);
        Log.d("Chatroom", chatroom.getNick1() + " " + chatroom.getNick2());
        startActivity(intent);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.listener = (Listener) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException( context + " must implements Listener");
        }
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        inicializar();

        presenter = new ProfilePresenter(this);

        setHasOptionsMenu(true);


        // TODO. Obtener el extra User. Si es null, obtenemos el perfil del presenter (del user actual), si no, cargamos el de ese user pasado (obtenemos el perfil del presenter de ese usere)
        // Si el extra es null:
        if (getArguments() == null){
            try {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Perfil");
            }
            catch (Exception e){
                Log.d("Marina", e.getMessage());
            }

            if (!CommonUtils.isNetworkOnline()){
                if (getActivity() != null)
                    CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
            }
            else {
                showProgressBar(null, getString(R.string.msg_loading_profile), false, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                presenter.getProfile(getContext());
            }
            btEditarPerfil.setVisibility(View.VISIBLE);
            btAnadirAmigo.setVisibility(View.GONE);
            btIniciarChat.setVisibility(View.GONE);

        }
//        else if (getArguments().getBoolean("editado", false)){
//            presenter.getProfile(getContext());
//        }
        else{        // Si el extra no es null:
            Profile profile = (Profile) getArguments().getSerializable("Profile");

            cargarPerfil(profile);


            // TODO. DESCOMENTAR ESTO:
            if (profile != null && !profile.getNick().equals(CommonUtils.getCurrentUserNick(AnimensApplication.getContext()))){ // TODO. Si no soy yo el usuario, entonces habilito los botones de añadir y deshabilito editar perfil
                btEditarPerfil.setVisibility(View.GONE);
                btAnadirAmigo.setVisibility(View.GONE);

    //            btAnadirAmigo.setVisibility(View.VISIBLE);

                presenter.getFriendList();

                btIniciarChat.setVisibility(View.VISIBLE);
                presenter.checkIfRequestCanBeSent(profile.getNick()); // TODO. Esto será si está en la tabla amigos o si existe en friendrequests el usuario actual con este nick

            }
            else{ // VISITANDO TU PERFIL DESDE UNO DE TUS AMIGOS
                floatingActionMenu.setVisibility(View.GONE);
                btEditarPerfil.setVisibility(View.GONE);
                btAnadirAmigo.setVisibility(View.GONE);
                btIniciarChat.setVisibility(View.GONE);
            }



            // Si es fragment de perfil es llamado desde user: (es decir, no es tu perfil):
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);


        }


    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }



    public static ProfileFragment newInstance(Bundle bundle){
        ProfileFragment profileFragment = new ProfileFragment();
        if (bundle != null){
            profileFragment.setArguments(bundle);
        }
        return profileFragment;
    }


    // ProfileContract.View
    @Override
    public void loadProfile(Profile profile) {
        hideProgressBar();

        if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(profile.getNick())){
            menu.findItem(R.id.itmenu_Preferences).setVisible(true); // TODO. es el usuario actual
        }

        cargarPerfil(profile);
    }

    @Override
    public void onServerError() {
        hideProgressBar();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_server);
    }

    @Override
    public void onFriendRequestSended() {
        Toast.makeText(AnimensApplication.getContext(), "Petición enviada", Toast.LENGTH_SHORT).show();
        btAnadirAmigo.setVisibility(View.GONE);

        menu.findItem(R.id.itmenu_add).setVisible(false);
    }

    @Override
    public void onRequestCanBeSent() {
        btAnadirAmigo.setVisibility(View.VISIBLE);
        menu.findItem(R.id.itmenu_add).setVisible(true);

    }

    @Override
    public void onNoRequestCanBeSent() {

    }



    public void cargarPerfil(Profile profile){
        this.profile = profile;
        btEditarPerfil.setEnabled(true);

        if (profile.getName() != null)
            tvProfileNombre.setText(profile.getName());
        else
            tvProfileNombre.setText(profile.getNick());

        if (profile.getDescription() != null)
            tvDescripcion.setText(profile.getDescription());
        else
            tvDescripcion.setText(R.string.msg_defaultdescription);


        tvGenero.setText(profile.getGender());

        if (profile.getRegisterDate() != null){
            String formatedDate = CommonUtils.formatDate(profile.getRegisterDate());
            tvFechaRegistro.setText(formatedDate);
        }

        if (profile.getBirthdate() != null){
            String formatedDate = CommonUtils.formatDate(profile.getBirthdate());
            tvFechaNac.setText(formatedDate); // ;profile.getBirthdate()); //CommonUtils.dateToString(profile.getBirthdate()));
        }

        if (profile.getPicture() != null){
            Picasso.get()
                    .load(profile.getPicture())
                    .fit()
                    .centerInside()
                    .into(profile_image,  new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onError(Exception e) {
                            if (progressBar != null) {
                                progressBar.setVisibility(View.GONE);
                            }
                        }

                    });

            Picasso.get()
                    .load(profile.getPicture())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            ProfileFragment.this.imageBitmap = bitmap;
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

//            if (edited){
//
//            }

//            profile_image.setImageBitmap(CommonUtils.base64ToBitmap(profile.getPicture64()));
        }
        else{
            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
        }


    }

//    @Override
//    public void loadAnimes(ArrayList<Anime> animes) {
//        // TODO. Cargo el fragment de AnimeList con estos datos en el Bundle
//
//
//      //  listener.loadAnimes(animes);
//
//
////                            Intent intentAnimes = new Intent(getContext(), AnimeActivity.class); // TODO. Solo mostrar los favoritos (consulta a BBDD)
////                    startActivity(intentAnimes);
//    }
//
//    @Override
//    public void loadFriends(ArrayList<User> users) {
//        // TODO. Cargo el fragment de UserList con estos datos en el Bundle
//
//   //     listener.loadFriends(users);
//
////                Intent intent = new Intent(getContext(), UserListActivity.class);
////                                startActivity(intent);
//    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    // INICIALIZAR TODO
    void inicializar(){
//        tabLayout = getView().findViewById(R.id.tabLayout);
        if (getView()!= null) {
            btEditarPerfil = getView().findViewById(R.id.btEditarPerfil);
            btAnadirAmigo = getView().findViewById(R.id.btAnadirAmigo);
            btIniciarChat = getView().findViewById(R.id.btIniciarChat);
            profile_image = getView().findViewById(R.id.profile_image);
            tvProfileNombre = getView().findViewById(R.id.tvProfileNombre);
            tvDireccion = getView().findViewById(R.id.tvTitleImagen);
            tvDescripcion = getView().findViewById(R.id.tvDescription);
            tvFechaNac = getView().findViewById(R.id.tvFechaNac);
            tvFechaRegistro = getView().findViewById(R.id.tvFechaRegistro);
            tvGenero = getView().findViewById(R.id.tvGenero);
            progressBar = getView().findViewById(R.id.progressBar);
            btAnimes = getView().findViewById(R.id.btIrAnimes);
            btAmigos = getView().findViewById(R.id.btIrAmigos);
            floatingActionMenu = getView().findViewById(R.id.material_design_android_floating_action_menu);
            btEditarPerfil.setEnabled(false);


            btAnimes.setOnClickListener(new OnClickIrAnimes());
            btAmigos.setOnClickListener(new OnClickIrAmigos());

//        tabLayout.addOnTabSelectedListener(new TabSelected());

            btEditarPerfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onProfileEdit(profile);
                }
            });

            btIniciarChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                Toast.makeText(getContext(), "Yendo al chat", Toast.LENGTH_SHORT).show();

                    if (!CommonUtils.isNetworkOnline()) {
                        if (getActivity() != null)
                            CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
                    } else
                        presenter.getChatroom(profile.getNick());
                }
            });

            btAnadirAmigo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(AnimensApplication.getContext(), getString(R.string.msg_sendingfriendrequest), Toast.LENGTH_SHORT).show();

                    String nickCurrentUser = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());
                    String nickUserFriend = profile.getNick();

                    // TODO. Enviar petición, si el otro usuario acepta, ya se añadirá como amigo
                    presenter.sendFriendRequest(nickCurrentUser, nickUserFriend);

                    // presenter.addFriend(nickCurrentUser, nickUserFriend);

                }
            });

            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getContext() != null && imageBitmap != null) { // solo si ya se ha cargado la imagen, la mostraré en grande
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
                        LayoutInflater factory = LayoutInflater.from(getContext());
                        View mView = factory.inflate(R.layout.dialog_profileimage, null);// ((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_profileimage, null);
                        ImageView photoView = mView.findViewById(R.id.dialog_imageview);
                        photoView.setImageBitmap(imageBitmap);

                        mBuilder.setView(mView);
                        AlertDialog mDialog = mBuilder.create();
                        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        mDialog.show();
//                    Window window = mDialog.getWindow();
//                    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    }
                }
            });
        }
    }

    public class OnClickIrAmigos implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Toast.makeText(getContext(), "Yendo a mis amigos...", Toast.LENGTH_SHORT).show();
            if (profile != null){
                listener.getFriends(profile.getNick());   // presenter.getFriends(profile);
            }

//                    Intent intent = new Intent(getContext(), UserListActivity.class); // TODO. Solo mostrar los agregados (consulta a BBDD)
//                    startActivity(intent);
        }
    }

    public class OnClickIrAnimes implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // Toast.makeText(getContext(), "Yendo a mis animes...", Toast.LENGTH_SHORT).show();
            if (profile != null){
                try {
                    listener.getAnimes(profile.getNick());   // presenter.getAnimes(profile);
                    SharedPreferences prefs = getContext().getSharedPreferences("com.example.kasane.animens_preferences", Context.MODE_PRIVATE);

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("lastprofile", profile.getNick()); // TODO. Obtendré el perfil en UserListFragment en su Interactor -> getUserProfile
                    editor.commit();
                }
                catch (Exception e){}
            }

//                    Intent intentAnimes = new Intent(getContext(), AnimeActivity.class); // TODO. Solo mostrar los favoritos (consulta a BBDD)
//                    startActivity(intentAnimes);
        }
    }


//    class TabSelected implements TabLayout.OnTabSelectedListener {
//        @Override
//        public void onTabSelected(TabLayout.Tab tab) {
//            switch (tab.getPosition()) {
//                case 1:
//                   // Toast.makeText(getContext(), "Yendo a mis amigos...", Toast.LENGTH_SHORT).show();
//                    if (profile != null)
//                        listener.getFriends(profile.getNick());   // presenter.getFriends(profile);
//
////                    Intent intent = new Intent(getContext(), UserListActivity.class); // TODO. Solo mostrar los agregados (consulta a BBDD)
////                    startActivity(intent);
//                    break;
//                case 2:
//                    // Toast.makeText(getContext(), "Yendo a mis animes...", Toast.LENGTH_SHORT).show();
//                    if (profile != null)
//                        listener.getAnimes(profile.getNick());   // presenter.getAnimes(profile);
//
////                    Intent intentAnimes = new Intent(getContext(), AnimeActivity.class); // TODO. Solo mostrar los favoritos (consulta a BBDD)
////                    startActivity(intentAnimes);
//                    break;
//            }
//        }
//        @Override
//        public void onTabUnselected(TabLayout.Tab tab) {
//
//        }
//        @Override
//        public void onTabReselected(TabLayout.Tab tab) {
//
//        }
//    }
}
