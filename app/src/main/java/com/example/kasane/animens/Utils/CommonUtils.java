package com.example.kasane.animens.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.MessageBoard;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Clase con utilidades comunes
 * @author Marina Espinosa
 */
public class CommonUtils {
    static public boolean validateEmail(String email){
        String pattern = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";
        if (email.matches(pattern))
            return true;
        return false;
    }

    static public boolean validatePhone(String phone){
        String pattern = "(\\+34|0034|34)?[ -]*(6|7)[ -]*([0-9][ -]*){8}";
        if (phone.matches(pattern))
            return true;
        return false;
    }

    static public boolean validatePassword(String password){
        int minCaracters = 6;
        int maxCaracters = 15;

        if (TextUtils.isEmpty(password))
            return false;
        if (password.length() < minCaracters || password.length() > maxCaracters)
            return false;
        return true;
    }

    /**
     * Convierte String a Date. Si no se puede convertir devuelve NULL
     * @param fecha
     * @return
     */
    static public Date stringToDate(String fecha){
        Date date;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = simpleDateFormat.parse(fecha);
        } catch (ParseException e) {
            e.printStackTrace();
            date = null;
        }
        return date;
    }

    // Convierte de fecha a String con el formato dd/MM/yyyy
    static public String dateToString(Date fecha){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return simpleDateFormat.format(fecha);
    }

    /**
     * Obtiene el nick del usuario que ha iniciado sesión. Devuelve vacío si falla al obtener el usuario
     * @param context
     * @return
     */
    static public String getCurrentUserNick(Context context){
        SharedPreferences prefs = context.getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);
        String user = prefs.getString("user", "");

        return user;
    }

    static public int getCurrentFriendRequestsCount(){
        SharedPreferences prefs = AnimensApplication.getContext().getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);
        int count = prefs.getInt("count", 0);
        return count;
    }

    static public void saveCountOfFriendRequests(int count){
        SharedPreferences prefs = AnimensApplication.getContext().getSharedPreferences("com.example.kasane.animens_preferences",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("count", count);
        editor.commit();
    }


    // Para encriptar
    public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = text.getBytes("iso-8859-1");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }


    // Pasar bitmap a ByteArray
    public static byte[] bitmapAByteArray(Bitmap bitmap){
        try {
            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);
            byte[] byteArray = bStream.toByteArray();
            return byteArray;

        }
        catch (Exception e){
            return null;
        }

    }


    /**
     * Bitmap a base64
     */
    public static String bitmapToBase64(Bitmap bmp){
        if (bmp != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 90, stream); //compress to which format you want.
            byte [] byte_arr = stream.toByteArray();
            String image_str = Base64.encodeToString(byte_arr, Base64.DEFAULT);
            return image_str;
        }
        return null;
    }

    /**
     * Base64 a bitmap
     */
    public static Bitmap base64ToBitmap(String encodedImage){
        if (encodedImage != null){
            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return decodedByte;
        }
        return null;
    }


    public static float getRatingPer10(float rating){
        return (10*rating)/5; // regla de 3
    }


    public static ArrayList<Bitmap> getImagesBitMap(ArrayList<Anime> animeList){
        ArrayList<Bitmap> bitmaps = new ArrayList<>();
        for(Anime animeTmp : animeList){
            Bitmap bitmapTmp = getBitmapFromURL(animeTmp.getPicture());
            bitmaps.add(bitmapTmp);
        }
        return bitmaps;
    }

    private static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Log.d("Marina", "Error al convertir a bitmap la imagen: " + src);
            return null;
        }
    }

//    public static Bitmap decodificarByteArray(byte[] byteArray){
//        return  BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
//    }


    public static boolean messageBoardIsFromThisUser(Context context, MessageBoard messageBoard){
        // TODO. Si user_id del messageBoard es la de este usuario (que tenemos su nick en preferencias)

        String nickUser = getCurrentUserNick(context);
        // String idMessageBoard = messageBoard.getMessageBoard_id();
        String user_nick = messageBoard.getUser_nick();

        // TODO. Consulta a la base de datos:
        // TODO. select count(*) from (select user_id from messageboard where messageBoard_id = 'idMessageBoard' and user_id = (select user_id from user where nick = 'nickUser') ) as cuantos;
        // Si la anterior consulta devuelve count(*) > 0, devolveré true. Si no, false

        // CONSULTA usando nick en vez de ID. TODO. LA QUE USARÉ
        // TODO. select count(*) from (select user_nick from messageboard where messageBoard_id = 'idMessageBoard' and user_nick = 'marina' ) as cuantos;
        // Si la anterior consulta devuelve count(*) > 0, devolveré true. Si no, false



        if (user_nick.equals(nickUser))
            return true;
        return false;

    }


    public static void showMessage(Activity activity, String message){
        Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    public static void showMessage(Activity activity, int resource){
        Snackbar.make(activity.findViewById(android.R.id.content), resource, Snackbar.LENGTH_LONG).show();
    }

    public static void hideKeyboard(Activity activity, View view){
        try {
            final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        catch (Exception e){

        }
    }

    private static void logout(Context context){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        Toast.makeText(context, "Cerrando sesión...", Toast.LENGTH_SHORT).show();

        // Cierro sesión normal
        SharedPreferences prefs = context.getSharedPreferences("com.example.kasane.animens_preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user", "");
        editor.putString("password", "");
        editor.putString("email", "");
        editor.commit();

        // TODO. Cierro la sesión de GOOGLE
        if (mAuth.getCurrentUser() != null) // Sesión Google
            mAuth.signOut();

        // Vuelvo al login
        Intent intent = new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);        // Elimino la pila ?
        context.startActivity(intent);
        ((Activity)context).finish();

    }

    public static void showDialogLogOut(final Context context){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(R.string.logout);
        alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout(context);
            }
        });
        alertDialog.setNegativeButton(R.string.no, null);
        alertDialog.show();
    }



    static public String imagetoString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] imageType = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imageType, Base64.DEFAULT);
    }


    /**
     * Cambia el formato de la fecha (de yyy-MM-dd a dd/MM/yyyy)
     */
    static public String formatDate(String date){
        if (date != null){
            try {
                final String OLD_FORMAT = "yyyy-MM-dd";
                final String NEW_FORMAT = "dd/MM/yyyy";

                String oldDateString = date;
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                Date d = null;
                try {
                    d = sdf.parse(oldDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sdf.applyPattern(NEW_FORMAT);
                newDateString = sdf.format(d);

                return newDateString;
            }
            catch (Exception e){
                return null;
            }
        }
        else
            return null;

    }

    static public String formatDateReverse(String date){
        if (date != null){
            try {
                final String NEW_FORMAT = "yyyy-MM-dd";
                final String OLD_FORMAT = "dd/MM/yyyy";

                String oldDateString = date;
                String newDateString;

                SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
                Date d = null;
                try {
                    d = sdf.parse(oldDateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sdf.applyPattern(NEW_FORMAT);
                newDateString = sdf.format(d);

                return newDateString;
            }
            catch (Exception e){
                return null;
            }
        }
        else
            return null;

    }

    /**
     * Muestro un diálogo para introducir un nick de usuario y darle a Aceptar
     */
    static public void showEditTextDialog(Context context, DialogInterface.OnClickListener listener){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Nick");
        alertDialog.setMessage("Introduzca un nick (único)");

        final EditText input = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setPositiveButton("Aceptar", listener);
        alertDialog.setView(input);
        alertDialog.show();
    }


    /**
     * Obtengo la hora y minutos actuales (HH:mm)
     * @return
     */
    public static String getFormattedCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm"); // :ss
        String strDate = mdformat.format(calendar.getTime());
        return strDate;
    }


    /**
     * Obtengo la fecha actual en formato dd/MM/yyyy
     * @return
     */
    public static String getFormattedCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = mdformat.format(calendar.getTime());
        return strDate;
    }

    /**
     * Compruebo si hay conexión a internet, forma 1
     * @return
     */
    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e){
            e.printStackTrace();
            Log.d("isOnline", e.getMessage());
        }
        catch (InterruptedException e) {
            e.printStackTrace();
            Log.d("isOnline", e.getMessage());
        }
        catch (Exception e){
            Log.d("isOnline", e.getMessage());
        }

        return false;
    }


    /**
     * Compruebo si hay conexión a internet, forma 2
     * @return
     */
    public static boolean isNetworkOnline() {
        try {
            ConnectivityManager cm =
                    (ConnectivityManager) AnimensApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected(); // isConnectedOrConnecting
        }
        catch (Exception e){
            Log.d("isOnline", "(network): " + e.getMessage());
            return false;
        }
    }


}
