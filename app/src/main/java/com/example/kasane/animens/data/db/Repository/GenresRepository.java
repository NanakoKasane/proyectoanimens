package com.example.kasane.animens.data.db.Repository;

import com.example.kasane.animens.data.db.DAO.GenreDAO;
import com.example.kasane.animens.data.db.model.Genre;

import java.util.ArrayList;
/**
 * Repository para sqlite
 * @author Marina Espinosa
 */
public class GenresRepository {
    private static GenresRepository genresRepository;
    private GenreDAO genreDAO;

    private GenresRepository(){
        genreDAO = new GenreDAO();
    }

    public static GenresRepository getInstance(){
        if (genresRepository == null)
            genresRepository  = new GenresRepository();
        return genresRepository;
    }

    public int getGenresCount(){
        return genreDAO.getGenresCount();
    }

    public boolean insertAllGenres(ArrayList<Genre> genres){
        return genreDAO.insertAllGenres(genres);
    }

    public ArrayList<String> getGenreString(){
        return genreDAO.getGenreString();
    }
}
