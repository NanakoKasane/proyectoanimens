package com.example.kasane.animens.ui.adapter;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.firebase.model.PublicMessages;
import com.example.kasane.animens.data.network.model.Chatroom;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
/**
 * Adapter para mostrar los mensajes del chat público
 * @author Marina Espinosa
 */
public class GeneralChatFirebaseAdapter extends FirebaseRecyclerAdapter<PublicMessages, GeneralChatFirebaseAdapter.MessageViewHolder> {
    private ProgressBar progressBar;
    private Chatroom chatroom;
    private static final int TYPE_USERACTUAL = 0;
    private static final int TYPE_OTROUSUARIO = 1;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query.
     *
     * @param options
     */
    public GeneralChatFirebaseAdapter(ProgressBar progressBar, @NonNull FirebaseRecyclerOptions<PublicMessages> options) {
        super(options);
        this.progressBar = progressBar;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        boolean isFromOtherUser;
        if (viewType == TYPE_USERACTUAL){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_me, parent, false);
            isFromOtherUser = false;
        }
        else{
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_otheruser, parent, false);
            isFromOtherUser = true;
        }

        return new MessageViewHolder(view, isFromOtherUser);
    }

    @Override
    public int getItemViewType(int position) {
        PublicMessages messages = this.getItem(position);

        String senderNick = messages.getSenderNick();
        if (messages.getSenderNick() == null)
            senderNick = "";

        if(senderNick.equals(CommonUtils.getCurrentUserNick(AnimensApplication.getContext()))){
            return TYPE_USERACTUAL;
        }
        else{
            return TYPE_OTROUSUARIO;
        }
    }

    @NonNull
    @Override
    public PublicMessages getItem(int position) {
        return super.getItem(position); // return super.getItem(getCount() - 1 - pos);

    }

    @Override
    protected void onBindViewHolder(final MessageViewHolder viewHolder, int position, PublicMessages friendlyMessage) {
        progressBar.setVisibility(ProgressBar.INVISIBLE);

        if (friendlyMessage.getMessage() != null ) {
            if (viewHolder.isFromOtherUser){
                viewHolder.name.setText(friendlyMessage.getSenderNick());
                viewHolder.name.setVisibility(View.VISIBLE);
                viewHolder.progressBarImage.setVisibility(View.VISIBLE);
                viewHolder.avatar.setVisibility(View.VISIBLE);

                Picasso.get()
                        .load(friendlyMessage.getSenderPicture())
                        .fit()
                        .centerInside()
                        .error(R.drawable.imagenotfound)
                        .into(viewHolder.avatar, new com.squareup.picasso.Callback()
                        {
                            @Override
                            public void onSuccess() {
                                if (viewHolder.progressBarImage != null) {
                                    viewHolder.progressBarImage.setVisibility(View.GONE);
                                }
                            }
                            @Override
                            public void onError(Exception e) {
                                if (viewHolder.progressBarImage != null) {
                                    viewHolder.progressBarImage.setVisibility(View.GONE);
                                }
                            }

                        });
            }
            else{
                viewHolder.name.setVisibility(View.GONE);
                viewHolder.avatar.setVisibility(View.GONE);
                viewHolder.progressBarImage.setVisibility(View.GONE);
            }

            // Común sea de un usuario u otro
            viewHolder.message_body.setText(friendlyMessage.getMessage());
            viewHolder.message_body.setVisibility(TextView.VISIBLE);

            // Añado la fecha formateada
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            Date dateMessage = null;
            String horaFormated = null;
            try {
                dateMessage = sdfDate.parse(friendlyMessage.getDaySended());
            } catch (Exception e) {
                e.printStackTrace();
            }
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM",new Locale("es", "ES")); // new Locale("es_ES")); // dd 'de' MMMM
            if (dateMessage != null) {
                horaFormated = sdf2.format(dateMessage);

                viewHolder.tvHora.setText(horaFormated + " · " + friendlyMessage.getHourSended()); // TODO
            }
            else
                viewHolder.tvHora.setText(friendlyMessage.getHourSended());
            viewHolder.tvHora.setTypeface(null, Typeface.ITALIC);

            viewHolder.tvHora.setVisibility(View.VISIBLE);

        }

        // No es este de este chat:
        else{
            viewHolder.avatar.setVisibility(View.GONE);
            viewHolder.message_body.setVisibility(View.GONE);
            viewHolder.tvHora.setVisibility(View.GONE);
            viewHolder.name.setVisibility(View.GONE);
            viewHolder.progressBarImage.setVisibility(View.GONE);
        }


    }



    public static class MessageViewHolder extends RecyclerView.ViewHolder{
        boolean isFromOtherUser;

        // De ambos
        TextView tvHora;
        TextView message_body;

        // Solo enviado por otro:
        TextView name;
        CircleImageView avatar;
        ProgressBar progressBarImage;

        public MessageViewHolder(View itemView, boolean isFromOtherUser) {
            super(itemView);
            tvHora = itemView.findViewById(R.id.tvHora);
            message_body = itemView.findViewById(R.id.message_body);

            name = itemView.findViewById(R.id.name);
            avatar = itemView.findViewById(R.id.avatar);
            progressBarImage = itemView.findViewById(R.id.progressBarImage);

            this.isFromOtherUser = isFromOtherUser;
        }
    }
}
