package com.example.kasane.animens.data.network.model;

import android.support.annotation.NonNull;

import java.util.Comparator;

public class MessageBoardLikes implements Comparable<MessageBoardLikes> {
    private String messageBoard_id;
    private String userWhoLikes;

    public MessageBoardLikes(String messageBoard_id, String userWhoLikes) {
        this.messageBoard_id = messageBoard_id;
        this.userWhoLikes = userWhoLikes;
    }

    public String getMessageBoard_id() {
        return messageBoard_id;
    }

    public void setMessageBoard_id(String messageBoard_id) {
        this.messageBoard_id = messageBoard_id;
    }

    public String getUserWhoLikes() {
        return userWhoLikes;
    }

    public void setUserWhoLikes(String userWhoLikes) {
        this.userWhoLikes = userWhoLikes;
    }

    // Comparando por id del mensaje
    @Override
    public int compareTo(@NonNull MessageBoardLikes o) {
        return this.getMessageBoard_id().compareTo(o.getMessageBoard_id());
    }

    // Comparando por id del user
    public static class OrderByUserId implements Comparator<MessageBoardLikes>{
        @Override
        public int compare(MessageBoardLikes o1, MessageBoardLikes o2) {
            return o1.getUserWhoLikes().compareTo(o2.getUserWhoLikes());
        }
    }

}
