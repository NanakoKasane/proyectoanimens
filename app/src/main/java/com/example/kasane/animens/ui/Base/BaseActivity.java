package com.example.kasane.animens.ui.Base;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.data.network.model.User;
import com.example.kasane.animens.ui.Anime.AnimeActivity;
import com.example.kasane.animens.ui.Chats.ChatsActivity;
import com.example.kasane.animens.ui.GeneralChat.GeneralChatFirebaseActivity;
import com.example.kasane.animens.ui.Genres.GenresListActivity;
import com.example.kasane.animens.ui.MessageBoard.MessageBoardActivity;
import com.example.kasane.animens.ui.Notifications.NotificationsActivity;
import com.example.kasane.animens.ui.Pofile.ProfileActivity;
import com.example.kasane.animens.ui.Pofile.ProfileInteractor;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.example.kasane.animens.ui.prefs.PreferencesActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


// Todas las Activity heredarán de esta activity. Y no tendrán que establecer el layout (setContentView) ni nada

/**
 * Base activity que contiene el Navigation Drawer y lo gestiona (clicks). Las Activity heredan de esta porque deben tener el mismo NavigationDrawer
 * @author Marina Espinosa
 */
public class BaseActivity extends AppCompatActivity {
    public ActionBarDrawerToggle actionBarDrawerToggle;
    public User user;

    // @BindView(R.id.aboutView) AboutView aboutView;

    // TODO. Para el Navigation Drawer
    @BindView(R.id.drawer_layout) public DrawerLayout drawerLayout;
    @BindView(R.id.navigationview) public NavigationView navigationView;


    public Toolbar toolbar;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.

//        switch (item.getItemId()) {
//            case android.R.id.home:
//                drawerLayout.openDrawer(GravityCompat.START);
//                return true;
//        }

        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onBackPressed() {
//        if (drawerLayout.isDrawerOpen(GravityCompat.START))
//            drawerLayout.closeDrawer(GravityCompat.START);
//        else
//            super.onBackPressed();
//    }

    private void selectDrawerItem(MenuItem menuItem) {
        Intent intent = null;

        switch (menuItem.getItemId()){
            // Perfil
            case R.id.nav_profile:
                intent = new Intent(BaseActivity.this, ProfileActivity.class);
//                actionBar.setTitle(R.string.nav_profile);
                break;

            // Gente
            case R.id.nav_users:
                intent = new Intent(BaseActivity.this, UserListActivity.class);
//                actionBar.setTitle(R.string.nav_users);
                break;

            // Animes
            case R.id.nav_anime:
                intent = new Intent(BaseActivity.this, AnimeActivity.class);
//                actionBar.setTitle(R.string.nav_anime);
                break;

            // Géneros
            case R.id.nav_genres:
                intent = new Intent(BaseActivity.this, GenresListActivity.class);
//                actionBar.setTitle(R.string.nav_genres);
                break;

            // Chats
            case R.id.nav_chats:
                intent = new Intent(BaseActivity.this, ChatsActivity.class);
//                actionBar.setTitle(R.string.nav_chats);
                break;

            // Tablón
            case R.id.nav_messageboard:
                intent = new Intent(BaseActivity.this, MessageBoardActivity.class);
//                actionBar.setTitle(R.string.nav_messageboard);
                break;

            // Configuración
            case R.id.nav_settings:
                intent = new Intent(BaseActivity.this, PreferencesActivity.class);
//                actionBar.setTitle(R.string.nav_settings);
                break;

             // Chat público / general
            case R.id.nav_general_chat:
                intent = new Intent(BaseActivity.this, GeneralChatFirebaseActivity.class);
                break;

            case R.id.nav_peticiones:
                intent = new Intent(BaseActivity.this, NotificationsActivity.class);
                break;

            case R.id.nav_logout:
                CommonUtils.showDialogLogOut(BaseActivity.this);
                break;

        }


        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        drawerLayout.closeDrawers();

        if (intent != null)
            startActivity(intent);
//        actionBar.setTitle(menuItem.getTitle());

    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);

        toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);




        // TODO. LA FOTO Y NOMBRE DE USUARIO:
        View hView =  navigationView.getHeaderView(0);
        final CircleImageView nav_user = hView.findViewById(R.id.profile_image);
        final TextView tvNick = hView.findViewById(R.id.tvNick);
        final ProgressBar progressBar = hView.findViewById(R.id.progressBar);


        // TODO. Obtener user actual SOLO SI ES LA PRIMERA VEZ QUE SE CREA
//        if ((User) getIntent().getSerializableExtra("User") != null){
//            user = (User) getIntent().getSerializableExtra("User");
//        }

//        user = ((AnimensApplication)getApplication()).getCurrentUser();
        ProfileInteractor profileInteractor = new ProfileInteractor();
        if (!CommonUtils.isNetworkOnline()){
            CommonUtils.showMessage(BaseActivity.this, R.string.msg_nointernet);
        }
        else {
            profileInteractor.getProfile(AnimensApplication.getContext(), new ProfileInteractor.GetProfile() {
                @Override
                public void profileObtained(Profile profile) {
                    tvNick.setText(profile.getNick());

                    Picasso.get()
                            .load(profile.getPicture())
                            .fit()
                            .centerInside()
                            .into(nav_user, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    if (progressBar != null)
                                        progressBar.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError(Exception e) {
                                    if (progressBar != null)
                                        progressBar.setVisibility(View.GONE);
                                }
                            });

                }

                @Override
                public void onServerError() {
                    CommonUtils.showMessage(BaseActivity.this, "Error en el servidor");
                }
            });
        }

        if (user != null){
            // Lo borro
//            getIntent().removeExtra("User"); // NO LO BORRO PORQUE SE VUELVE A CREAR EN CADA ACTIVITY


            // Cuando obtengo resultado:
//            tvNick.setText(user.getNick());
//
//            Picasso.get()
//                    .load(user.getPicture())
//                    .fit()
//                    .centerInside()
//                    .into(nav_user,  new com.squareup.picasso.Callback() {
//                        @Override
//                        public void onSuccess() {
//                        }
//
//                        @Override
//                        public void onError(Exception e) {
//                        }
//                    });



            // TODO
        }
        else{
            // user = null
        }


        // getSupportActionBar().setTitle("Animens");

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });



        drawerLayout.setDrawerShadow(android.R.color.darker_gray, GravityCompat.START);

        // TODO. Hamburguesa
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open,  R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    public void goToNotifications(View view) {
        Intent intent = new Intent(BaseActivity.this, NotificationsActivity.class);
        startActivity(intent);

    }

    /*
    @Override
    public Resources.Theme getTheme() {
        Resources.Theme theme = super.getTheme();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String temaElegido = pref.getString("tema", "Azul");
        if (temaElegido.equals("Azul")){
            setTheme(R.style.AppThemeNoActionBar);
            getWindow().setNavigationBarColor(getResources().getColor(R.color.primary));
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }
        else if (temaElegido.equals("Rojo")){
            setTheme(R.style.AppThemeNoActionBarRed);
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorRedPrimary));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorRedDark));
        }
        return theme;
    }
    */


}
