package com.example.kasane.animens.ui.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Chats;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * (Actualmente en desuso)
 * Adapter para mostrar el listado de Chats
 * @author Marina Espinosa
 */
public class ChatsListAdapter extends RecyclerView.Adapter<ChatsListAdapter.ChatsHolder> {
    private Context context;
    private ArrayList<Chats> chats;
    private View.OnClickListener onClickListener;

    public ChatsListAdapter(Context context){
        this.context = context;
        this.chats = new ArrayList<>();
    }

    public void setChats(ArrayList<Chats> chats) {
        this.chats = chats;
    }


    @NonNull
    @Override
    public ChatsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.chatslist_item, parent, false);

        view.setOnClickListener(onClickListener);

        ChatsHolder chatsHolder = new ChatsHolder(view);
        return chatsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsHolder holder, int position) {
        Chats chat = chats.get(position);

        holder.tvNick.setText(chat.getNick());
        // TODO. holder.tvHora.setText();
        holder.tvLastMessage.setText(chat.getLast_message());
        holder.imgUser.setImageResource(chat.getPictureUser());
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public class ChatsHolder extends RecyclerView.ViewHolder{
        TextView tvNick;
        TextView tvHora;
        TextView tvLastMessage;
        CircleImageView imgUser;

        public ChatsHolder(View itemView) {
            super(itemView);
            tvNick = itemView.findViewById(R.id.tvNick);
            tvHora = itemView.findViewById(R.id.tvHora);
            tvLastMessage = itemView.findViewById(R.id.tvLastMessage);
            imgUser = itemView.findViewById(R.id.imgUser);
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public Chats getItem(int position){
        return chats.get(position);
    }
}
