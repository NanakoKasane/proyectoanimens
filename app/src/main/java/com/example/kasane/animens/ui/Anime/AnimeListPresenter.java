package com.example.kasane.animens.ui.Anime;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Genres.GenresListActivity;
import com.example.kasane.animens.ui.User.UserListActivity;
import com.example.kasane.animens.ui.User.UserListFragment;
import com.example.kasane.animens.ui.User.UserListInteractor;

import java.util.ArrayList;

// Presenter
public class AnimeListPresenter implements AnimeListContact.Presenter, AnimeListInteractor.GetAnimeList, AnimeListInteractor.EditRating, AnimeListInteractor.DeleteAnime, UserListInteractor.GetUserProfile {
    private AnimeListContact.View view;
    private AnimeListInteractor interactor;
    private AnimeListContact.ViewEditRating viewEditRating;

    public AnimeListPresenter(AnimeListContact.View view){
        this.view = view;
        interactor = new AnimeListInteractor();
    }

    public AnimeListPresenter(AnimeListContact.ViewEditRating viewEditRating){
        this.viewEditRating = viewEditRating;
        interactor = new AnimeListInteractor();
    }


    // Presenter
    @Override
    public void getAnimeList(ArrayList<Anime> animes) { // Si animes no es null solo se descargará las fotos, la lista de Animes ya la tiene.
        view.showProgressBar(null, AnimensApplication.getContext().getString(R.string.msg_loading_animes), false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent intent = new Intent(AnimensApplication.getContext(), UserListActivity.class);
                AnimensApplication.getContext().startActivity(intent);
            }
        });
        interactor.getAnimeList(animes, this);
    }

    @Override
    public void getAnimeListOfThisGenre(String genre) {
        view.showProgressBar(null, AnimensApplication.getContext().getString(R.string.msg_loading_animeofgenre) + " " + genre + "...", false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent intent = new Intent(AnimensApplication.getContext(), GenresListActivity.class);
                AnimensApplication.getContext().startActivity(intent);
            }
        });
        interactor.getAnimeListOfThisGenre(genre, this);
    }

    @Override
    public void getAnimeListOfThisUser(String nick) {
        view.showProgressBar(null, AnimensApplication.getContext().getString(R.string.msg_loading_favoritsanimes), false, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Intent intent = new Intent(AnimensApplication.getContext(), UserListActivity.class);
                AnimensApplication.getContext().startActivity(intent);
            }
        });
        interactor.getAnimeListOfThisUser(nick, this);
    }


    @Override
    public void editRating(Anime anime, float rating) {
//        view.showProgressBar();
        interactor.editRating(anime, rating, this);
    }

    @Override
    public void deleteAnime(Anime anime) {
        interactor.deleteAnime(anime, this);
    }

    @Override
    public void getUserProfile(String nick) {
        UserListInteractor userListInteractor = new UserListInteractor();
        userListInteractor.getUserProfile(nick, this);
    }

    // Interactor
    @Override
    public void onAnimeListObtained(ArrayList<Anime> anime, ArrayList<Bitmap> images) {
        view.hideProgressBar();
        view.loadAnimes(anime, images);
    }

    @Override
    public void onServerError() {
        view.hideProgressBar();
        view.onServerError();
    }

    @Override
    public void onNoResults() {
        view.hideProgressBar();
        view.onNoResults();

        // view.showNoAnimeMessage();
        // TODO. Mensaje. "No tiene añadidos animes"
    }

    // Interactor EditRating
    @Override
    public void onRatingEdited(Anime anime, float rating) {
//        view.hideProgressBar();
        viewEditRating.onRatingEdited(anime, rating);
    }

    // Interactor DeleteAnime
    @Override
    public void onDeleted() {
        view.onDeleted();
    }

    @Override
    public void onUserProfile(Profile profile) {
        view.onUserProfile(profile);
    }
}
