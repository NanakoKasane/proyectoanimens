package com.example.kasane.animens.ui.Chats;

public class ConversationFirebasePresenter implements ConversationFirebaseContract.Presenter, ConversationFirebaseInteractor.UpdateLastMessage {
    private ConversationFirebaseContract.View view;
    private ConversationFirebaseInteractor interactor;

    public ConversationFirebasePresenter(ConversationFirebaseContract.View view){
        this.view = view;
        interactor = new ConversationFirebaseInteractor();
    }

    @Override
    public void setLastMessageReaded(String chat_id) {
        interactor.setLastMessageReaded(chat_id, this);
    }

    @Override
    public void updateLastChatMessage(String lastMessage, String timeLastMessage, String dateLastMessage, String senderLastMessage, String chat_id) {
        interactor.updateLastChatMessage(lastMessage, timeLastMessage, dateLastMessage, senderLastMessage, chat_id, this);
    }


    // Interactor
    @Override
    public void onUpdatedReaded() {
        view.onUpdatedReaded();
    }

    @Override
    public void onUpdatedLastChatMessage() {
        view.onUpdatedLastChatMessage();
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

}
