package com.example.kasane.animens.ui.Anime;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.R;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Base.BaseFragment;
import com.example.kasane.animens.ui.Login.LoginActivity;
import com.example.kasane.animens.ui.Pofile.ProfilePresenter;
import com.example.kasane.animens.ui.adapter.AnimeListAdapter;

import java.util.ArrayList;
import java.util.Collections;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Fragment que muestra y gestiona el listado de Animes
 * @author Marina Espinosa
 */
public class AnimeListFragment extends BaseFragment implements AnimeListContact.View, AnimeListContact.ViewEditRating{
    public static String TAG = "AnimeListFragment";
    RecyclerView rvAnimeList;
    FloatingActionButton btnAdd;
    ArrayList<Bitmap> bitmaps;
    AnimeListAdapter animeListAdapter;

    static AnimeListPresenter presenter;
    AnimeListPresenter presenterEditRating;
    private ProgressDialog progressDialog;

    OnAnime listener;
    String genre;
    String nick;

    interface OnAnime{
        void onEdit(Anime anime);
        void onDetail(Anime anime);

        void onProfile(Profile profile);
    }

    public static AnimeListFragment newInstance(Bundle bundle){
        AnimeListFragment animeListFragment = new AnimeListFragment();
        if (bundle != null)
            animeListFragment.setArguments(bundle);
        return animeListFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            listener = (OnAnime) context;
        }
        catch (ClassCastException e){
            throw new ClassCastException(context.toString() + " must implements OnAnime");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_anime_list, container, false);
        if (getActivity() != null)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvAnimeList= view.findViewById(R.id.rvAnimeList);
        btnAdd = view.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), AnimeAddFragment.class);
//                intent.putExtra("editar", false);
//                startActivity(intent);

                listener.onEdit(null);
            }
        });
        // TODO. Quito posibilidad de añadir:
        btnAdd.setVisibility(View.GONE);

        rvAnimeList.setLayoutManager(new GridLayoutManager(getContext(), 2));

        //  new DescargarImagenes(this).execute(AnimeListRepository.getAnimeListRepository());

        presenter = new AnimeListPresenter((AnimeListContact.View) this);

        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
        }
        else {
            if (getArguments() != null) {
                genre = getArguments().getString("Genre");
                nick = getArguments().getString("Nick");

                if (genre != null)
                    presenter.getAnimeListOfThisGenre(genre);
                if (nick != null)
                    presenter.getAnimeListOfThisUser(nick);
            } else
                presenter.getAnimeList(null);
        }

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setRetainInstance(true);

        setHasOptionsMenu(true); // TODO. Carga su propio menú
    }

    @Override
    public void onResume() {
        super.onResume();
//        recargarLista();
    }

    private void recargarLista(){
        if (!CommonUtils.isNetworkOnline()){
            if (getActivity() != null)
                CommonUtils.showMessage(getActivity(), R.string.msg_nointernet);
            else
                Toast.makeText(AnimensApplication.getContext(), R.string.msg_nointernet, Toast.LENGTH_SHORT).show();
        }
        else {
            showProgressBar(null, "Actualizando Animes...", false, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {

                }
            });

            if (genre != null)
                presenter.getAnimeListOfThisGenre(genre);
            if (nick != null)
                presenter.getAnimeListOfThisUser(nick);
            else if (genre == null && nick == null) {
                presenter.getAnimeList(null);
            }
        }
    }

    // AnimeListContact.View
    @Override
    public void loadAnimes(ArrayList<Anime> animes, ArrayList<Bitmap> images) {
//        hideProgressBar();

        // TODO. Importante el context siguiente:
        animeListAdapter = new AnimeListAdapter(getContext(), animes, images); // La instancia es única, y la instancia está ordenada, así que no pasa nada por volverla a pasar

        animeListAdapter.setOnRatingBarChangeListener(new myOnRatingBarChangesListener());
        animeListAdapter.setOnClickListener(new myOnClickListener());

        // TODO. Quito posibilidad de borrar:
//        animeListAdapter.setOnLongClickListener(new ListenerLongClickDelete());

        rvAnimeList.setAdapter(animeListAdapter);

        // animeListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onRatingEdited(Anime anime, float rating) {
//        Toast.makeText(AnimensApplication.getContext(), "Voto registrado: " + rating + " para el Anime: " + (anime).getName(), Toast.LENGTH_SHORT).show();
    }


    class myOnRatingBarChangesListener implements MaterialRatingBar.OnRatingBarChangeListener {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            if (fromUser){
                 Toast.makeText(AnimensApplication.getContext(), "Voto registrado: " + CommonUtils.getRatingPer10(rating) + " para el Anime: " + ((Anime)(ratingBar.getTag())).getName(), Toast.LENGTH_SHORT).show();

                presenterEditRating = new AnimeListPresenter((AnimeListContact.ViewEditRating) AnimeListFragment.this);
                presenterEditRating.editRating((Anime)(ratingBar.getTag()), CommonUtils.getRatingPer10(rating));



            }
        }
        // después de esto, se irá a la base de datos y se calculará el nuevo voto teniendo en cuenta este voto y los anteriores
    }


    class myOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            // Toast.makeText(AnimeActivity.this, "Anime a mostrar el detalle: " + ((Anime)v.getTag()).getName(), Toast.LENGTH_SHORT).show();
//            Intent intent = new Intent(getContext(), AnimeDetailFragment.class);
//
//            intent.putExtra("Anime", (Anime)(v.getTag()) ); // Serializable extra
//
//            startActivity(intent);


            // TODO
            listener.onDetail((Anime) v.getTag());

        }
    }

    class myOnTouchListener implements MaterialRatingBar.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return true;
        }
    }

    private class ListenerLongClickDelete implements View.OnLongClickListener{
        @Override
        public boolean onLongClick(final View v) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setMessage(R.string.dialog_delete_anime);
            alertDialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    presenter.deleteAnime((Anime) v.getTag());

                }
            });
            alertDialog.setNegativeButton(R.string.no, null);
            alertDialog.show();


            return true; // Cancelo el click normal
        }
    }

    @Override
    public void onDeleted() {
        Intent intent = new Intent(getActivity(), AnimeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intent, 0);
        buildNotification(getString(R.string.app_name), "Anime borrado correctamente", pendingIntent, getContext());

        Toast.makeText(AnimensApplication.getContext(), "Anime borrado correctamente", Toast.LENGTH_SHORT).show();
        recargarLista();
    }

    @Override
    public void onServerError() {
//        hideProgressBar();
        if (getActivity() != null)
            CommonUtils.showMessage(getActivity(), R.string.err_server);
    }

    @Override
    public void onNoResults() {
        // TODO. Maybe un dialog

//        hideProgressBar();
//        Toast.makeText(getContext(), "No tiene animes añadidos como favoritos", Toast.LENGTH_SHORT).show();

        if (getArguments() != null){
            String nick = getArguments().getString("Nick");
            DialogInterface.OnClickListener listenerOk =  new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    presenter.getUserProfile(nick);
                }
            };

            if (CommonUtils.getCurrentUserNick(AnimensApplication.getContext()).equals(nick)){
                // ESTO SOLO SI SON TUYOS
                showInformationDialog(getString(R.string.msg_noanime_title), getString(R.string.msg_user_noanime), listenerOk);
            }
            else{
                // SI NO
                showInformationDialog(getString(R.string.msg_noanime_title), getString(R.string.msg_otheruser_noanime), listenerOk);
            }

        }
    }

    @Override
    public void onUserProfile(Profile profile) {
        listener.onProfile(profile);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){

//            inflater.inflate(R.menu.order_menu, menu);

//            super.onCreateOptionsMenu(menu,inflater);
//        }

        menu.clear();
        inflater.inflate(R.menu.search_order_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (animeListAdapter != null)
                    animeListAdapter.filter(query);
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if (animeListAdapter != null)
                    animeListAdapter.filter(newText);
                return false;
            }
        });
    }

    // Ordenaciones
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (animeListAdapter != null) {
            ArrayList<Anime> animes = animeListAdapter.getAnimeList();

            // Ordenar por nombre
            if (item.getItemId() == R.id.orderNombre) {
                try {
                    Collections.sort(animes, new Anime.OrderByName());
                    presenter.onAnimeListObtained(animes, null);
                }
                catch (Exception e){}
            }

            // Ordenar por ID
            if (item.getItemId() == R.id.orderId) {
                Collections.sort(animes);
                presenter.onAnimeListObtained(animes, null);
            }

            // Ordenar por valoración ascendente (el por defecto):
            if (item.getItemId() == R.id.orderRating) {
                // presenter.getAnimeList(null);
                try {
                    Collections.sort(animes, new Anime.OrderByRating());
                    presenter.onAnimeListObtained(animes, null);
                }
                catch (Exception e){}
            }
        }


//        presenter.getAnimeList(animes);
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }




}

