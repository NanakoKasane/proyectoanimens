package com.example.kasane.animens.ui.GeneralChat;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.data.network.model.Profile;
import com.example.kasane.animens.ui.Pofile.ProfileInteractor;

public class GeneralChatFirebasePresenter implements GeneralChatFirebaseContract.Presenter, ProfileInteractor.GetProfile{
    private GeneralChatFirebaseContract.View view;
    private ProfileInteractor profileInteractor;

    public GeneralChatFirebasePresenter(GeneralChatFirebaseContract.View view){
        this.view = view;
        profileInteractor = new ProfileInteractor();
    }

    @Override
    public void getCurrentUserProfile() {
        profileInteractor.getProfile(AnimensApplication.getContext(), this);
    }

    // interactor GetProfile
    @Override
    public void profileObtained(Profile profile) {
        view.onProfileObtained(profile);
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }
}
