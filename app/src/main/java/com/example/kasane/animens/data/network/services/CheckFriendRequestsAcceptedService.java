package com.example.kasane.animens.data.network.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.kasane.animens.AnimensApplication;
import com.example.kasane.animens.Utils.CommonUtils;
import com.example.kasane.animens.data.network.model.FriendRequests;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * (Actualmente sustituido por CheckFriendRequestsAcceptedFirebaseJobService)
 * Consulto si las peticiones de amistad enviadas a otros usuarios han sido aceptada
 */
public class CheckFriendRequestsAcceptedService extends IntentService {
    private static final int MAX_TIMEOUT = 2000;
    private static final int RETRIES = 2;
    public static final String ACTION_SUCCESS = "com.example.kasane.animens.accepted.success";
    public static final String ACTION_FAILURE = "com.example.kasane.animens.accepted.failure";
    public static final String MOTIVO = "MOTIVO";


    public CheckFriendRequestsAcceptedService() {
        super("CheckFriendRequestsAcceptedService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String nick = CommonUtils.getCurrentUserNick(AnimensApplication.getContext());

        SyncHttpClient client = new SyncHttpClient();
        client.setMaxRetriesAndTimeout(RETRIES, MAX_TIMEOUT);
        client.get(AnimensApplication.BASE_URL+"animens/checkIfMyFriendRequestsAreAccepted.php?userWhoSendsRequest_nick=" + nick , new JsonHttpResponseHandler() {

            // TODO. On success -> obtengo la lista de peticiones aceptadas y mando con sus datos notificación
            // Mediante Broadcast notifico -> Le paso en el bundle serializable las notificaciones
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Gson gson = new Gson();
                    ArrayList<FriendRequests> friendRequests = gson.fromJson(response.getJSONArray("friendrequests").toString(), new TypeToken<ArrayList<FriendRequests>>(){}.getType());

                    Bundle bundle = new Bundle();
                    int contador = 0;
                    for (FriendRequests f : friendRequests ){
                        bundle.putSerializable("friendrequest" + contador, f);
                        contador++;
                    }
                    bundle.putInt("peticioneaceptadas", contador);
                    Intent intentSuccess = new Intent();
                    intentSuccess.setAction(CheckFriendRequestsAcceptedService.ACTION_SUCCESS);
                    intentSuccess.putExtras(bundle);
                    LocalBroadcastManager.getInstance(AnimensApplication.getContext()).sendBroadcast(intentSuccess);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("AnimensPHP", e.getMessage());
                    Log.d("AnimensPHP", response.toString());
//                        listener.onServerError();
                }
            }

            // TODO. On failure -> será que no hay ninguna petición
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("AnimensPHP", "OnFailure" + responseString + "\n" + throwable.getMessage());
//                    listener.onServerError();

                Intent intentFailure = new Intent();
                intentFailure.setAction(CheckFriendRequestsAcceptedService.ACTION_FAILURE);
                intentFailure.putExtra(CheckFriendRequestsAcceptedService.MOTIVO, "No hay ninguna petición");
                LocalBroadcastManager.getInstance(AnimensApplication.getContext()).sendBroadcast(intentFailure);

            }





        });

    }
}
