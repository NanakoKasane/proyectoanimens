
package com.example.kasane.animens.data.firebase.model;

/**
 * Clase modelo para los mensajes de Firebase
 * @author Marina Espinosa
 */
public class Messages {
    private String id; // id del mensaje
    private String message;
    private String senderNick;
    private String receiverNick;
    private String time_sended;
    private String chat_id; // id del chat único de un usuario con otro usuario
    private String date_sended;

    public Messages() {
    }

    public Messages(String id, String message, String senderNick, String receiverNick, String time_sended, String chat_id, String date_sended) {
        this.id = id;
        this.message = message;
        this.senderNick = senderNick;
        this.receiverNick = receiverNick;
        this.time_sended = time_sended;
        this.chat_id = chat_id;
        this.date_sended = date_sended;
    }

    public Messages(String message, String senderNick, String receiverNick, String time_sended, String chat_id, String date_sended) {
        this.message = message;
        this.senderNick = senderNick;
        this.receiverNick = receiverNick;
        this.time_sended = time_sended;
        this.chat_id = chat_id;
        this.date_sended = date_sended;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderNick() {
        return senderNick;
    }

    public void setSenderNick(String senderNick) {
        this.senderNick = senderNick;
    }

    public String getMessage() {
        return message;
    }

    public String getTime_sended() {
        return time_sended;
    }

    public void setTime_sended(String time_sended) {
        this.time_sended = time_sended;
    }

    public String getReceiverNick() {
        return receiverNick;
    }

    public void setReceiverNick(String receiverNick) {
        this.receiverNick = receiverNick;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getDate_sended() {
        return date_sended;
    }

    public void setDate_sended(String date_sended) {
        this.date_sended = date_sended;
    }

}
