package com.example.kasane.animens.ui.Anime;

import android.text.TextUtils;

import com.example.kasane.animens.data.db.Repository.AnimeRepository;
import com.example.kasane.animens.data.db.Repository.GenresRepository;
import com.example.kasane.animens.data.network.model.Anime;
import com.example.kasane.animens.data.network.model.Genres;
import com.example.kasane.animens.data.network.repository.AnimeListRepository;
import com.example.kasane.animens.data.network.repository.GenresListRepository;

import java.util.ArrayList;
import java.util.List;

public class AnimeAddInteractor {

    public void validateAnime(String nombre, String url, String tipo, String numCaps, AnimeAddInteractor.ValidateAnime listener) {
        // Todo. No puede estar vacío ni el título ni los capítulos ni la foto (y la foto debe ser válida, verse en un navegador). name maximo 100, genre 125, type 7, episodes 7, picture (60)
        if (TextUtils.isEmpty(nombre)){
            listener.onEmptyNameError();
            return;
        }

        if (TextUtils.isEmpty(numCaps)){
            listener.onEmptyEpisodesError();
            return;
        }

        if (TextUtils.isEmpty(url)){
            listener.onEmptyPictureError();
            return;
        }

        // TODO. Comprobar que la imagen sea válida (que se vea en un navegador):



        try {
            int caps = Integer.parseInt(numCaps);
        }
        catch (Exception e){
            listener.onInvalidEpisodesNumber();
            return;
        }


        // Si no ha salido por ningún return es que es válido:
        listener.onSuccessfulValidation();

    }


    public void editAnime(Anime animeAEditar, Anime animeEditado, AnimeAddInteractor.EditAnime listener) {
//        ArrayList<Anime> animes = AnimeListRepository.getAnimeListRepository().getAnimeList();

        // TODO: Si animeAEditar es null, es que es añadir
        if (animeAEditar == null){
//            animes.add(animeEditado);
            // TODO. INSERT A LA BD
            AnimeRepository.getInstance().insertAnime(animeEditado);
        }
        else{ // Editar
//            int indice = animes.indexOf(animeAEditar);
//            if (indice != -1){
//                animes.set(indice, animeEditado);
//                // TODO. Update A LA BD
//            }

            AnimeRepository.getInstance().updateAnime(animeEditado);
        }

        listener.onEdited(animeEditado);

    }

    public void getGenresStringList(AnimeAddInteractor.GetGenresStringList listener) {
        // TODO. Consulta a la BD:
        // TODO.  select genre from genres;

        ArrayList<String> generosString = GenresRepository.getInstance().getGenreString();

//        ArrayList<String> generosString = new ArrayList<>();
//        List<Genres> genres = GenresListRepository.getGenresListRepository().getGeneros();
//        for(Genres g : genres){
//            generosString.add(g.getGenre());
//        }
        listener.onGenresListObtained(generosString);

    }

    interface ValidateAnime{
        void onSuccessfulValidation();

        void onEmptyNameError();
        void onEmptyEpisodesError();
        void onEmptyPictureError();
        void onInvalidEpisodesNumber();
    }

    interface EditAnime{
        void onEdited(Anime animeEdited);
    }

    interface GetGenresStringList{
        void onGenresListObtained(ArrayList<String> genres);
    }
}
