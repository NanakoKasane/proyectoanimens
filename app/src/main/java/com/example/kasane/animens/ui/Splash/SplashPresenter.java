package com.example.kasane.animens.ui.Splash;

import com.example.kasane.animens.data.db.model.Anime_genres;
import com.example.kasane.animens.data.db.model.Genre;
import com.example.kasane.animens.data.network.model.Anime;

import java.util.ArrayList;

public class SplashPresenter implements SplashContract.Presenter, SplashInteractor.Download, SplashInteractor.Insert {
    private SplashContract.View view;
    private SplashInteractor interactor;

    public SplashPresenter(SplashContract.View view){
        this.view = view;
        interactor = new SplashInteractor();
    }

    @Override
    public void downloadAnimeData() {
        interactor.downloadAnimeData(this);
    }

    @Override
    public void downloadAnime_genresData() {
        interactor.downloadAnime_genresData(this);
    }

    @Override
    public void downloadGenres() {
        interactor.downloadGenresData(this);
    }

    @Override
    public void insertAnimes(ArrayList<Anime> animes) {
        interactor.insertAnimes(animes, this);
    }

    @Override
    public void insertAnime_genres(ArrayList<Anime_genres> anime_genres) {
        interactor.insertAnime_genres(anime_genres, this);
    }

    @Override
    public void insertGenres(ArrayList<Genre> genres) {
        interactor.insertGenres(genres, this);
    }

    // Interactor

    @Override
    public void onAnimeDownloaded(ArrayList<Anime> animes) {
        view.onDownloadedAnimeData(animes);
    }

    @Override
    public void onAnime_genres_Downloaded(ArrayList<Anime_genres> anime_genres) {
        view.onDownloadedAnime_genresData(anime_genres);
    }

    @Override
    public void onGenresDownloaded(ArrayList<Genre> genres) {
        view.onGenresDownloaded(genres);
    }

    @Override
    public void onError(String errorMessage) {
        view.onError(errorMessage);
    }

    @Override
    public void itWasAlreadyDownloaded() {
        view.itWasAlreadyDownloaded();
    }

    @Override
    public void onInsertedAnimes() {
        view.onInsertedAnimes();
    }

    @Override
    public void onInsertedAnime_genres() {
        view.onInsertedAnime_genres();
    }

    @Override
    public void onInsertedGenres() {
        // TODO. No es necesario
    }


}
