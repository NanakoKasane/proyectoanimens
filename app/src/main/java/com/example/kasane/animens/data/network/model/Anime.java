package com.example.kasane.animens.data.network.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Clase modelo que define la entidad Anime
 * @author Marina Espinosa
 */
public class Anime implements Serializable, Comparable<Anime> {
    /**
     * Fields
     */
    private String anime_id; // VARCHAR(100), no vacío
    private int anime_idIncrement; // único, no vacío
    private String name; // VARCHAR(100), no vacío
    private String genre; // varchar(125)
    private String type; // varchar(7)
    private String episodes; //varchar(7), no vacío
    private float rating; // decimal(5.2)
    private String picture; // ruta de la imagen. varchar(50), no vacío

    private byte[] image;

    /**
     * Constructors
     */
    public Anime(){}

    // TODO. Sqlite -> en orden
    public Anime(String anime_id, String name, String genre, String type, String episodes, float rating, String picture, int anime_idIncrement) {
        this.anime_id = anime_id;
        this.name = name;
        this.genre = genre;
        this.type = type;
        this.episodes = episodes;
        this.rating = rating;
        this.picture = picture;
        this.anime_idIncrement = anime_idIncrement;
    }


    public Anime(String anime_id, int anime_idIncrement, String name, String episodes, String picture){
        this.anime_id = anime_id;
        this.anime_idIncrement = anime_idIncrement;
        this.name = name;
        this.episodes = episodes;
        this.picture = picture;
    }

    public Anime(String anime_id, int anime_idIncrement, String name, String episodes, String picture, float rating){
        this.anime_id = anime_id;
        this.anime_idIncrement = anime_idIncrement;
        this.name = name;
        this.episodes = episodes;
        this.picture = picture;
        this.rating = rating;
    }

    public Anime(String anime_id, int anime_idIncrement, String name, String genre, String type, String episodes, float rating, String picture) {
        this.anime_id = anime_id;
        this.anime_idIncrement = anime_idIncrement;
        this.name = name;
        this.genre = genre;
        this.type = type;
        this.episodes = episodes;
        this.rating = rating;
        this.picture = picture;
    }


    /**
     * getter and setter
     */
    public String getAnime_id() {
        return anime_id;
    }

    public void setAnime_id(String anime_id) {
        this.anime_id = anime_id;
    }

    public int getAnime_idIncrement() {
        return anime_idIncrement;
    }

    public void setAnime_idIncrement(int anime_idIncrement) {
        this.anime_idIncrement = anime_idIncrement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEpisodes() {
        return episodes;
    }

    public void setEpisodes(String episodes) {
        this.episodes = episodes;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public float getRatingPer5(){
        return (5*this.rating)/10; // regla de 3
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    public ArrayList<String> getGenresList() {
        ArrayList<String> generosArrayList = new ArrayList<>();
        if (!TextUtils.isEmpty(this.genre)) {
            String[] nombresGeneros = this.genre.split(", ");
            Collections.addAll(generosArrayList, nombresGeneros);
        }
//        for(String s : nombresGeneros)
//            generosArrayList.add(s);
        return generosArrayList;
    }

    /**
     * Sobreescribiendo equals
     */
    @Override
    public boolean equals(Object obj) {
        boolean isEqual = obj instanceof Anime;
        return isEqual && (((Anime)obj).getAnime_idIncrement() == this.getAnime_idIncrement());
    }

    /**
     * Comparando por id
     */
    @Override
    public int compareTo(@NonNull Anime o) {
        return Integer.compare(this.getAnime_idIncrement(), o.getAnime_idIncrement());
    }


    /**
     * Otros criterios de comparación
     */
    // Comparo por nombre
    public static class OrderByName implements Comparator {

        @Override
        public int compare(Object o1, Object o2) {
            return ((Anime)o1).getName().compareTo(((Anime)o2).getName());
        }
    }

    public static class OrderByRating implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            return Float.compare(((Anime)o2).getRating(),  ((Anime)o1).getRating()) ;
        }
    }


}
