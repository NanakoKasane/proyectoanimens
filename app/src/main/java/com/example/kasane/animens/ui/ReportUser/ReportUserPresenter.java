package com.example.kasane.animens.ui.ReportUser;

import android.graphics.Bitmap;

public class ReportUserPresenter implements ReportUserContract.Presenter, ReportUserInteractor.ReportUser {
    private ReportUserContract.View view;
    private ReportUserInteractor interactor;

    public ReportUserPresenter(ReportUserContract.View view){
        this.view = view;
        interactor = new ReportUserInteractor();
    }

    @Override
    public void sendReport(String nick, Bitmap imagen, String descripcion, String motivos) {
        interactor.reportUser(nick, imagen, descripcion, motivos, this);
    }

    @Override
    public void onPhotoTooBig() {
        view.onPhotoTooBig();
    }

    @Override
    public void onServerError() {
        view.onServerError();
    }

    @Override
    public void onReported() {
        view.onReported();
    }
}
